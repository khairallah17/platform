import { Router } from "express";
import userController from "../controllers/user.controller";

const router = Router()

router.route("/")
        .get(userController.getAllUsers)

router.route("/:id")
        .patch(userController.updateUser)
        .delete(userController.deleteUser)
        .post(userController.activateUser)
        .get(userController.getUserById)

const userRoutes = router

export default userRoutes