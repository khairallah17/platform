import categoriesController from "../controllers/categories.controller";
import { Router } from "express";

const router = Router()

router.route("/")
            .get(categoriesController.getAllCategories)
            .post(categoriesController.createNewCategory)

router.route("/:name")
            .patch(categoriesController.updateCategory)
            .delete(categoriesController.deleteCategorie)

const categoriesRoutes = router

export default categoriesRoutes