import express from "express"
import userService from "../services/user.service";

const router = express.Router()

router.route("/users")
    .get(userService.getAllUsers)

router.route("/users/:id")
    .get(userService.getUserById)
    .patch(userService.activateUser)

export const adminRoute = router