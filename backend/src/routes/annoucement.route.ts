import { Router } from "express";
import AnnoucementController from "../controllers/annoucement.controller";
import { upload } from "../config/multer.config";
import { checkFileValidMW } from "../middleware/checkFileValidMW";

const router = Router()

router.route("/")
            .get(AnnoucementController.getAllAnnoucement)
            .patch(upload.single('file'), checkFileValidMW, AnnoucementController.updateAnnoucement)
            .post(upload.single('file'), checkFileValidMW, AnnoucementController.createAnnouncement)

router.route("/:id")
            .get(AnnoucementController.getAnnoucementById)
            .delete(AnnoucementController.deleteAnnoucement)
            .patch(AnnoucementController.approveAnnoucement)

const annoucemementRoute = router

export default annoucemementRoute