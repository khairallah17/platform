import passport from "passport";
import express, { Request, Response } from "express"
import authController from "../controllers/auth.controller";
import jwtController from "../controllers/jwt.controller";
import googleAuthController from "../controllers/googleAuth.controller";
import { upload } from "../config/multer.config";

const router = express.Router()

router.route("/signup")
    .post(upload.fields([{
        name: 'WorkshopImage', maxCount: 1
    },{
        name: 'certificationImage', maxCount: 1
    }]), authController.authRegisterController)

router.route("/login")
    .post(authController.authLoginController)

router.route("/refresh")
    .get(jwtController.refreshTokenController)

router.route("/logout")
    .get(authController.authLogoutController)

router.get("/facebook", passport.authenticate("facebook"))

router.get("/", passport.authenticate("google", {
    scope: ["email", "profile"]
}, () => {
    console.log("AUTH ROUTE")
}))

router.get("/facebook/callback", passport.authenticate("facebook", { failureRedirect: "/" }), (req: Request, res: Response) => {
    return res.redirect("/auth/profile")
})

router.get("/profile", (req: Request, res: Response) => {
    if (!req.user) {
        return res.send({ message: 'No user' })
    }

    return res.json(req.user)
})

router.get("/callback/redirect", 
                passport.authenticate("google"),
                googleAuthController.googleAuthLogin)

export const authRouter = router;