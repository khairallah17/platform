import { Router } from "express";
import reservationController from "../controllers/reservation.controller";

const router = Router()

router.route("/")
            .post(reservationController.createNewReservation)

router.route("/user/:id")
            .get(reservationController.getReservationsByUser)

router.route("/annoucement/:id")
            .get(reservationController.getReservationsByAnnoucement)

const reservationRoute = router

export default reservationRoute