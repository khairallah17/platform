import { Router } from "express";
import blogController from "../controllers/blog.controller";

const router = Router()

router.route("/")
        .get(blogController.getAllArticles)
        .post(blogController.createNewArticle)

router.route("/:id")
        .delete(blogController.deleteArticle)
        .patch(blogController.updateArticle)
        .get(blogController.getArticleById)

const blogRoute = router

export default blogRoute
