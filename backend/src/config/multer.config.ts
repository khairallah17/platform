import multer from "multer";
import path from "path";
import fs from "fs"
import parseFileName from "../utils/parseFileName";

export const multerStorage = multer.diskStorage({
    destination(_, file, callback) {
        const { base } = parseFileName(file.originalname)

        fs.promises.mkdir(path.join('public/uploads', base), { recursive: true })
            .then(() => {
                callback(null, `public/uploads/${base}`)
            })
    },
    filename: (_, file, cb) => cb(null, `full${parseFileName(file.originalname).ext}`)
})

export const upload = multer({storage: multerStorage})