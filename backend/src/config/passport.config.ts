import passport from "passport"
import googleAuthService from "../services/googleAuth.service"
import { User as PrismaUser } from "@prisma/client"
import { faceBookStrategy } from "../services/facebookAuth.service"

passport.serializeUser((user: Express.User, done) => {
    done(null, user)
})

passport.use(googleAuthService.googleAuthStrategy)
// passport.use(faceBookStrategy)

passport.deserializeUser(async (id: PrismaUser, done) => {

    done(await googleAuthService.googleAuthDeserialize(id), null)

})
