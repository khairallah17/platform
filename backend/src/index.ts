import express, { NextFunction, Request, Response } from "express"
import dotenv from "dotenv"
import "./config/passport.config"
import session from "express-session"
import { authRouter } from "./routes/auth.route"
import { eventsLogger } from "./middleware/logEvents.middleware"
import verifyJWT from "./middleware/verifyJWT.middleware"
import errorHandler from "./middleware/error.middleware"
import cookieParser from "cookie-parser"
import passport from "passport"
import verifyRoles from "./middleware/verifyRole.middleware"
import cors from "cors"

// ROUTES
import annoucemementRoute from "./routes/annoucement.route"
import userRoutes from "./routes/user.route"
import blogRoute from "./routes/blog.route"
import reservationRoute from "./routes/reservation.route"
import categoriesRoutes from "./routes/categories.route"

dotenv.config()

const app = express()
const port = Number(process.env.PORT) || 8080

// LOGS MIDDLEWARE
app.use(eventsLogger)

const option: cors.CorsOptions = {
    origin: ["http://localhost:3000"],
    credentials: true,
    methods: ["GET","POST","DELETE","PATCH","OPTIONS"]
}

app.use(cors(option))

// HANDLING FORM DATA
app.use(express.urlencoded({ extended: true }))

// HANDLING JSON
app.use(express.json())


// COOKIE PARSER MIDDLEWARE
app.use(cookieParser())

// ERROR HANDLING MIDDLEWARE
app.use(errorHandler)

// LOADING STATIC FILES
app.use(express.static('public'))

// SESSION MIDDLEWARE
app.use(session({
    secret: String(process.env.COOKIE_SECRET),
    resave: false,
    saveUninitialized: true
}))

app.use(passport.initialize())
app.use(passport.session())

app.use("/auth", authRouter)
app.use("/user", verifyJWT, verifyRoles(["ADMIN"]), userRoutes)
app.use("/annoucements", verifyJWT, verifyRoles(["CREATOR", "ADMIN"]), annoucemementRoute)
app.use("/blog", verifyJWT, verifyRoles(["ADMIN"]), blogRoute)
app.use("/reservation", reservationRoute)
app.use("/categories", categoriesRoutes)

app.listen(port, "0.0.0.0", () => console.log(`app listening on http://localhost:${port}`))