import { FACEBOOK_APP_ID, FACEBOOK_APP_SECRET } from "../utils/secrets";
import passportFacebook from "passport-facebook"

const FacebookStrategy = passportFacebook.Strategy

export const faceBookStrategy = new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "/auth/facebook/callback",
    },
    function (accessToken, refreshToken, profile, done) {
        console.log(profile)
        return done(null, profile)
    }
)