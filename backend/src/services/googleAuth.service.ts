import { User as PrismaUser } from "@prisma/client"
import passportGoogle from "passport-google-oauth20"
import { GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET } from "../utils/secrets"
import prismaClient from "../config/prismaClient.config"
import tokenService from "./jwtToken.service"
import UserPreferencesModel from "../models/userPreferences.model"
import bcrypt from "bcrypt"

const GoogleStrategy = passportGoogle.Strategy

const googleAuthStrategy = new GoogleStrategy(

    {
        clientID: GOOGLE_CLIENT_ID,
        clientSecret: GOOGLE_CLIENT_SECRET,
        callbackURL: "http://localhost:3000"
    },
    async (accessToken ,refreshToken, profile, done) => {

        try {
            
            const foundUser = await prismaClient.user.findUnique({
                where: {
                    googleId: profile.id
                },
                include: {
                    userPreferences: true
                }
            })
    
            if (foundUser)
            {
                const user = {
                    ...foundUser,
                    refreshToken: await tokenService.generateRefreshToken(foundUser.email, String(foundUser.userPreferences?.type), foundUser.id, Boolean(foundUser?.userPreferences?.status), foundUser.name, String(foundUser.phoneNumber), new Date(String(foundUser?.birthDate)), String(foundUser.profileImage)),
                    accessToken: await tokenService.generateAccessToken(foundUser.email, String(foundUser.userPreferences?.type), foundUser.id, Boolean(foundUser?.userPreferences?.status), foundUser.name, String(foundUser.phoneNumber), new Date(String(foundUser?.birthDate)), String(foundUser.profileImage)),
                }
                return done(null, user)
            }

            const newPassword = await bcrypt.hash(String("12345678"), 10)
    
            const userPreferencesData: UserPreferencesModel = {
                status: true,
                type: "CLIENT"
            }

            const newUser = await prismaClient.user.create({
                data: {
                    email: String(profile.emails?.[0].value),
                    name: String(profile.displayName),
                    googleId: String(profile.id),
                    password: newPassword,
                    userPreferences: {
                        create: {
                            ...userPreferencesData
                        }
                    }
                }
            })
                
            return done(null, newUser)


        } catch (error: any) {
            
            done(error)

        }

    }
)

const googleAuthDeserialize = async (prismaUser: PrismaUser) => {

    try {

        const user = await prismaClient.user.findUnique({
            where: {
                id: prismaUser.id
            }
        })

        if (user)
            return user
        else
            throw new Error("user not found")

    } catch (error: any) {

        throw new Error(error)
    
    }

}

const googleAuthService = {
    googleAuthStrategy,
    googleAuthDeserialize
}

export default googleAuthService