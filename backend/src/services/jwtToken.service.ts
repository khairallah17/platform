import jwt from "jsonwebtoken"
import dotenv from "dotenv"
import prismaClient from "../config/prismaClient.config"

dotenv.config()

const ACCESS_TOKEN_SECRET = String(process.env.ACCESS_TOKEN_SECRET)
const REFRESH_TOKEN_SECRET = String(process.env.REFRESH_TOKEN_SECRET)

const generateAccessToken = async (userEmail: string, roles: string, userId: string, status: boolean, name: string, phoneNumber: string, birthDate: Date, profileImage: string) => {

    return jwt.sign(
        { 
            email: userEmail,
            roles: roles,
            id: userId,
            status,
            name,
            phoneNumber,
            birthDate,
            profileImage
        },
        ACCESS_TOKEN_SECRET,
        { expiresIn: '1d' }
    )

}

const generateRefreshToken = async (userEmail: string, roles: string, userId: string, status: boolean, name: string, phoneNumber: string, birthDate: Date, profileImage: string) => {

    return jwt.sign(
        { 
            email: userEmail,
            roles: roles,
            id: userId,
            status,
            name,
            phoneNumber,
            birthDate,
            profileImage
        },
        REFRESH_TOKEN_SECRET,
        { expiresIn: '2d' }
    )

}

const refreshTokenService = async (refreshToken: string) => {

    try {

        const foundUser = await prismaClient.user.findFirst({
            where: {
                refreshToken: refreshToken
            }
        })

        const accessToken = jwt.verify(
            String(refreshToken),
            REFRESH_TOKEN_SECRET,
            async (err, decoded: any) => {
                if (err || foundUser?.email !== decoded.email)
                    throw new Error("Invalid Token")
                return await generateAccessToken(decoded.email, decoded.roles, decoded.id, decoded.status, decoded.name, decoded.phoneNumber, decoded.birthDate, decoded.profileImage)
            }
        )

        return accessToken

    } catch (error: any) {

        throw new Error(error)

    }

}

const tokenService = {
    generateAccessToken,
    generateRefreshToken,
    refreshTokenService
}

export default tokenService