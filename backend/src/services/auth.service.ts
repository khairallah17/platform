import bcrypt from "bcrypt"
import prismaClient from "../config/prismaClient.config"
import tokenService from "./jwtToken.service"
import { User } from "@prisma/client"
import UserModel from "../models/user.model"

const login = async (userEmail: string, userPwd: string) => {

    try {

        const foundUser = await prismaClient.user.findUnique({
            where: {
                email: String(userEmail)
            },
            include: {
                userPreferences: true
            }
        })

        if (!foundUser)
            throw new Error("user does not exist")
        
        const pwdMatch = await bcrypt.compare(userPwd, foundUser.password)

        if (!pwdMatch)
            throw new Error("incorect password")

        
        const accessToken = await tokenService.generateAccessToken(foundUser.email, String(foundUser.userPreferences?.type), foundUser.id, Boolean(foundUser?.userPreferences?.status), foundUser.name, String(foundUser.phoneNumber), new Date(String(foundUser?.birthDate)), String(foundUser.profileImage))
        const refreshToken = await tokenService.generateRefreshToken(foundUser.email, String(foundUser.userPreferences?.type), foundUser.id, Boolean(foundUser?.userPreferences?.status), foundUser.name, String(foundUser.phoneNumber), new Date(String(foundUser?.birthDate)), String(foundUser.profileImage))
        // console.log(accessToken)

        const updatedUser = await prismaClient.user.update({
            where: {
                email: userEmail,
            },
            data: {
                refreshToken: refreshToken
            }
        })

        if (!updatedUser)
            throw new Error("user not updated")

        return [accessToken, refreshToken]

    } catch(err: any) {

        throw new Error(err)

    }

}

const logout = async (refreshToken: string) => {

    try {

        const foundUser = await prismaClient.user.findFirst({
            where: {
                refreshToken: refreshToken
            }
        })

        if (!foundUser)
            return null

        const deletedRefreshToken = await prismaClient.user.update({
            where: {
                email: foundUser?.email
            },
            data: {
                refreshToken: null
            }
        })

        return deletedRefreshToken

    } catch (error: any) {

        throw new Error(error)
    
    }

}

const authService = {
    login,
    logout
}

export default authService