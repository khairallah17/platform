import { Annonce, Prisma } from "@prisma/client";
import prismaClient from "../config/prismaClient.config";

const createAnnouncement = async (anouncementData: Annonce, userId: string, categoryName: string) => {

    try {
    
        const createdAnnoucement = await prismaClient.annonce.create({
            data: {
                title: anouncementData.title,
                description: anouncementData.description,
                address: anouncementData.address,
                city: anouncementData.city,
                seats: anouncementData.seats,
                announcedBy: {
                    connect: {
                        id: userId
                    }
                },
                details: anouncementData.details as Prisma.JsonArray,
                categoryRef: {
                    connect: {
                        name: categoryName
                    }
                }
            },
        })

        if (!createdAnnoucement)
            throw new Error("annoucement not created")
    
        return createAnnouncement

    } catch (error: any) {

        throw new Error(error)
    
    }

}

const updateAnnouncement = async (anouncementData: Annonce, annoucementId: string) => {

    try {

        const updatedAnnouncemenet = await prismaClient.annonce.update({
            where: {
                id: annoucementId
            },
            data: {
                ...anouncementData,
                details: anouncementData.details as Prisma.JsonArray
            }
        })

        if (!updatedAnnouncemenet)
            throw new Error("annoucement update Failed")
        
        return updatedAnnouncemenet

    } catch (error: any) {
        throw new Error(error)
    }

}

const deleteAnouncement = async (annoucementId: string) => {

    try {
        
        const deletedAnnoucement = await prismaClient.annonce.delete({
            where: {
                id: annoucementId
            }
        })

        if (!deletedAnnoucement)
            throw new Error("failed to delete annoucement")

        return deletedAnnoucement

    } catch (error: any) {
        throw new Error(error)
    }

}

const getAllAnnoucement = async () => {

    try {

        const annoucements = await prismaClient.annonce.findMany()

        if (!annoucements)
            throw new Error("error while fetching annoucements")

        return annoucements

    } catch (error: any) {
        throw new Error(error)
    }

}

const getAnnoucementById = async (annoucementId: string) => {

    try {
        
        const annoucement = await prismaClient.annonce.findUnique({
            where: {
                id: annoucementId
            }
        })

        if (!annoucementId)
            throw new Error("annoucemement does not exists")

        return annoucement

    } catch (error: any) {

        throw new Error(error)

    }

}

const approveAnnoucement = async (announcemementId: string) => {

    try {

        const announcement = await prismaClient.annonce.update({
            where: {
                id: announcemementId
            },
            data: {
                approved : true
            }
        })

        return announcement

    } catch (error: any) {
        throw new Error(error)
    }

}

const AnnouncementService = {
    createAnnouncement,
    updateAnnouncement,
    deleteAnouncement,
    getAllAnnoucement,
    getAnnoucementById,
    approveAnnoucement
}

export default AnnouncementService