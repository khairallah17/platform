import { Articles } from "@prisma/client";
import prismaClient from "../config/prismaClient.config";

const createNewArticle = async (articleData: Articles) => {

    try {
        
        const createdArticle = await prismaClient.articles.create({
            data: {
                ...articleData
            }
        })

        return createdArticle

    } catch (error: any) {
        throw new Error(error)
    }

}

const updateArticle = async (articleId: string, articleData: Articles) => {

    try {

        const updatedArticle = await prismaClient.articles.update({
            where: {
                id: articleId
            },
            data: {
                ...articleData
            }
        })

        return updatedArticle

    } catch (error: any) {
        throw new Error(error)
    }

}

const deleteArticle = async (articleId: string) => {

    try {

        const deletedArticle = await prismaClient.articles.delete({
            where: {
                id: articleId
            }
        })

        return deletedArticle

    } catch (error: any) {
        throw new Error(error)
    }

}

const getArticles = async () => {

    try {
        
        const articles = await prismaClient.articles.findMany()

        return articles

    } catch (error: any) {
        throw new Error(error)
    }

}

const getArticleById = async (articleId: string) => {

    try {
        
        const article = await prismaClient.articles.findUnique({
            where: {
                id: articleId
            }
        })
        
        return article

    } catch (error: any) {
        throw new Error(error)
    }

}

const blogService = {
    createNewArticle,
    updateArticle,
    deleteArticle,
    getArticles,
    getArticleById
}

export default blogService