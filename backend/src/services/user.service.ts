import prismaClient from "../config/prismaClient.config";
import UserModel from "../models/user.model";
import UserPreferencesModel from "../models/userPreferences.model";
import bcrypt from "bcrypt"
import { User as PrismaUser, Category } from "@prisma/client";
import { UserCategory } from "../models/user.model";
import { File } from "buffer";

const createNewUser = async (userData: UserModel) => {

    try {

        const foundUser = await prismaClient.user.findUnique({
            where: {
                email: String(userData.email)
            }
        })

        if (foundUser)
            throw new Error("user already Exists")


        const hashedPwd = await bcrypt.hash(String(userData.password), 10)

        const userPreferencesData: UserPreferencesModel = {
            type: String(userData.role),
            status: userData.role == "CREATOR" ? false : true,
            certification: userData.files?.certificationImage[0].destination ? true : false,
            certificationImage: String(userData.files?.certificationImage[0].destination),
            professionType: String(userData?.professionType),
            workshop: userData.files?.WorkshopImage[0].destination ? true : false,
            WorkshopImage: String(userData.files?.WorkshopImage[0].destination),
            professionLevel: String(userData?.professionLevel)
        }

        const createdUser = await prismaClient.user.create({
            data: {
                email: String(userData.email),
                password: hashedPwd,
                phoneNumber: String(userData.phoneNumber),
                name: String(userData.name),
            },
        })

        if (userData.role == "CREATOR") {
            const createdPreferences = await prismaClient.userPreference.create({
                data: {
                    ...userPreferencesData,
                    userId: createdUser.id,
                }
            })
    
            const createdUserCategory = await prismaClient.userCategory.create({
                data: {
                    userPreferenceId: createdPreferences.id,
                    category: {
                        connect: {
                            name: String(userData.category)
                        }
                    },
                }
            })
    
            const updateUser = await prismaClient.user.update({
                where: { id: createdUser.id },
                data: {
                    userPreferences: {
                        connect: {
                            id: createdPreferences.id
                        }
                    }
                }
            })
        }


        return createdUser

    } catch (err: any) {

        throw new Error(err)

    }

}

const getAllUsers = async () => {

    try {

        const users = await prismaClient.user.findMany({
            where: {
                userPreferences: {
                    type: {
                        in: ["CREATOR", "CLIENT"]
                    }
                }
            },
            select: {
                googleId: false,
                password: false,
                refreshToken: false,
                birthDate: true,
                announcements: true,
                createdAt: true,
                email: true,
                name: true,
                phoneNumber: true,
                profileImage: true,
                id: true,
                userPreferences: {
                    select: {
                        id: false,
                        user: false,
                        userId: false,
                        certification: true,
                        certificationImage: true,
                        professionCategory: true,
                        professionType: true,
                        status: true,
                        type: true,
                        workshop: true,
                        WorkshopImage: true,
                        categoryName: true
                    }
                }
            }
        })
    
        return users

    } catch (error: any) {

        throw new Error(error)
    
    }

}

const getUserById = async (userId: String) => {

    try {

        const foundUser = await prismaClient.user.findUnique({
            where:{
                id: String(userId)
            }
        })
    
        return foundUser

    } catch (error:any) {

        throw new Error(error)

    }

}

const activateUser = async (userId: String) => {

    try {

        const updatedUser = await prismaClient.user.update({
            where: {
                id: String(userId)
            },
            data: {
                userPreferences: {
                    update: {
                        status: true
                    }
                }
            }
        })

        return updatedUser

    } catch (error:any) {

        throw new Error(error)

    }

}

const updateUser = async (userId: string, userData: PrismaUser) => {

    try {

        const updatedUser = await prismaClient.user.update({
            where: {
                id: userId
            },
            data: {
                ...userData
            }
        })

        return updatedUser

    } catch (error: any) {

        throw new Error(error)

    } 

}

const deleteUser = async (userId: string) => {

    try {

        const deletedUser = await prismaClient.user.delete({
            where: {
                id: userId
            }
        })

        return deletedUser

    } catch (error: any) {

        throw new Error(error)

    }

}

const getUserByRefreshToken = async (refreshToken: string) => {

    try {

        const user = await prismaClient.user.findFirst({
            where: {
                refreshToken: refreshToken
            },
            include: {
                userPreferences: {
                    select: {
                        type: true
                    }
                }
            }
        })

        if (!user)
            throw new Error("no user found")

        return user?.userPreferences?.type

    } catch (error: any) {
        throw new Error(error)
    }

}

const approveUser = async (userId: string) => {

    try {

        const foundUser = await prismaClient.user.update({
            where: {
                id: userId
            },
            data: {
                userPreferences: {
                    update: {
                        data: {
                            status: true
                        }
                    }
                }
            }
        })

        return approveUser

    } catch (error: any) {
        throw new Error(error)
    }

}

const userService = {
    getUserById,
    createNewUser,
    getAllUsers,
    activateUser,
    updateUser,
    deleteUser,
    getUserByRefreshToken,
    approveUser
}

export default userService 