import prismaClient from "../config/prismaClient.config";

const getAllCAtegories = async () => {

    try {
        
        const categories = await prismaClient.category.findMany({
            select: {
                name: true
            }
        })

        return categories

    } catch (error: any) {
        
        throw new Error(error)

    }

}

const createNewCategory = async (categoryName: string) => {

    try {

        const createdCategorie = await prismaClient.category.create({
            data: {
                name: categoryName
            }
        })

        return createdCategorie

    } catch (error: any) {
        throw new Error(error)
    }

}

const updateCategorie = async (categorieName: string) => {

    try {
        
        const updatedCategorie = await prismaClient.category.update({
            where: {
                name: categorieName
            },
            data: {
                name: categorieName
            }
        })

        return updatedCategorie

    } catch (error: any) {
        throw new Error(error)
    }

}

const deleteCategorie = async (categorieName: string) => {

    try {

        const deletedCategorie = await prismaClient.category.delete({
            where: {
                name: categorieName
            }
        })

        return deletedCategorie

    } catch (error: any) {
        throw new Error(error)
    }

}

const CategoriesService = {
    createNewCategory,
    updateCategorie,
    deleteCategorie,
    getAllCAtegories
}

export default CategoriesService
