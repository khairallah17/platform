import prismaClient from "../config/prismaClient.config";
import { Reservation } from "@prisma/client";

const checkReservationAvailability = async (annoucementId: string) => {

    try {
        
        const reservation = await prismaClient.annonce.findUnique({
            where: {
                id: annoucementId
            },
            include: {
                reservations: {
                    select: {
                        seats: true
                    }
                }
            }
        })

        if (!reservation)
            return null

        const totalCapacity = reservation?.seats
        const reservedPlaces = reservation?.reservations.reduce(
            (acc, reservation) => acc + reservation.seats,
            0
        )

        const availableSeats = totalCapacity - reservedPlaces

        if (availableSeats <= 0)
            return null
        else
            return availableSeats

    } catch (error: any) {
        
        throw new Error(error)

    }

}

const createNewReservation = async (reservationData: Reservation) => {

    try {

        const availableSeats = await checkReservationAvailability(reservationData.annoucementId)

        if (!availableSeats)
            throw new Error("no available seats left")

        if (availableSeats <= reservationData.seats)
            throw new Error("no available seats left")

        const createdReservation = await prismaClient.reservation.create({
            data: {
                ...reservationData
            }
        })

        return createdReservation

    } catch (error: any) {

        throw new Error(error)

    }

}

const getReservationsByAnnoucement = async (annoucementId: string) => {

    try {
    
        const reservations = await prismaClient.reservation.findMany({
            where: {
                annoucementId: annoucementId
            }
        })

        return reservations

    } catch (error: any) {

        throw new Error(error)
    
    }

}

const getReservationsByUser = async (userId: string) => {

    try {
        
        const reservations = await prismaClient.reservation.findMany({
            where: {
                userId: userId
            }
        })

        return reservations

    } catch (error: any) {
        
        throw new Error(error)

    }

}

const updateRservation = async (reservationId: string, ReservationData: Reservation) => {
    
    try {
        
        const reservations = await prismaClient.reservation.update({
            where: {
                id: reservationId
            },
            data: {
                ...ReservationData
            }
        })

    } catch (error: any) {
        
        throw new Error(error)

    }

}

const reservationService = {
    createNewReservation,
    getReservationsByAnnoucement,
    getReservationsByUser,
    updateRservation
}

export default reservationService