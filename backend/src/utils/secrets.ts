import dotenv from "dotenv"
import fs from "fs"

if (fs.existsSync(".env"))
{
    dotenv.config()
}
else
    console.error("env file not found")

export const ENVIRONMENT = process.env.NODE_ENV
const prod = ENVIRONMENT === "PROD"
const db = ENVIRONMENT == "MYSQL"

export const PORT  = (process.env.PORT || 5000) as Number

export const DATABASE_URL = prod 
                                ? (process.env.MYSQL_PROD)
                                : (process.env.MYSQL_DEV)

if (!DATABASE_URL) {
    if (prod) {
        console.error("No mysql connection string set MYSQL_PROD environment variable")
    } else {
        console.error("No mysql connection string set MYSQL_DEV environment variable")
    }
    process.exit(1)
}

export const GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID as string;
export const GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET as string;
export const FACEBOOK_APP_ID = process.env.FACEBOOK_APP_ID as string;
export const FACEBOOK_APP_SECRET = process.env.FACEBOOK_APP_SECRET as string;

if (!GOOGLE_CLIENT_ID)
{
    console.error("No google client id string ,set GOOGLE_CLIENT_ID in environment variables")
    process.exit(1);
}
if (!GOOGLE_CLIENT_SECRET)
{
    console.error("No google client id string ,set GOOGLE_CLIENT_SECRET in environment variables")
    process.exit(1)
}
if (!FACEBOOK_APP_ID)
{
    console.error("No facebook app id string ,set FACEBOOK_APP_ID in environment variables")
    process.exit(1)
}
if (!FACEBOOK_APP_SECRET)
{
    console.error("No facebook app secret string ,set FACEBOOK_APP_SECRET in environment variables")
    process.exit(1)
}
