import { request } from "express";
import UserModel from "../../models/user.model";
import { User as PrismaUser } from "@prisma/client";

declare global {
    namespace Express {
        interface User extends PrismaUser {}
    }
}