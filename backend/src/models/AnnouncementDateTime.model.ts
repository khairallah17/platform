export interface AnnouncementDateTime {
    startTime:          Date,
    endTime:            Date,
    WorkShopDate:       Date
}