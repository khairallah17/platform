import { File } from "buffer"

interface MyFile {
    WorkshopImage: FileDetails[],
    certificationImage: FileDetails[]
}

interface FileDetails {
    fieldname: string,
    originalname: string,
    encoding: string,
    mimetype: string,
    destination: string,
    filename: string,
    path: string,
    size: number

}

interface UserModel {
    id?: String,
    name: string,
    email: string,
    password: string,
    phoneNumber?: string,
    birthDate?: Date,
    googleId?: string,
    role?: string,
    accessToken?: String,
    refreshToken?: String,
    category?:   String,
    certification?: boolean
    certificationImage?: String,
    professionType?: String,
    workshop?: boolean,
    WorkshopImage?: String
    professionLevel?: String,
    files?: MyFile
}

export interface UserCategory {
    name: string
}

export default UserModel