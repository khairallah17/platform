interface UserPreferencesModel {
    type: string,
    status: boolean,
    category?: string
    certification?: boolean 
    certificationImage?: string 
    professionType?: string 
    workshop?: boolean 
    WorkshopImage?: string 
    professionLevel?:   string
}

export default UserPreferencesModel