import { Response, Request, NextFunction } from "express";
import tokenService from "../services/jwtToken.service";

const refreshTokenController = async (req: Request, res: Response, next: NextFunction) => {

    const cookie = req.cookies

    if (!cookie?.jwt)
        return res.sendStatus(401)

    const refreshToken = cookie.jwt

    try {

        const accessToken = await tokenService.refreshTokenService(refreshToken)

        return res.status(200).json({accessToken: accessToken})

    } catch (err: any) {

        next(err)
    
    }

}

const jwtController = {
    refreshTokenController
}

export default jwtController