import { NextFunction, Request, Response } from "express"
import { User as PrismaUser } from "@prisma/client"
import tokenService from "../services/jwtToken.service"
import prismaClient from "../config/prismaClient.config"

const googleAuthLogin = async (req: Request, res: Response, next: NextFunction) => {

    if (req.user == undefined)
        return res.sendStatus(404)

    const user = req.user as PrismaUser

    try {

        const foundUser = await prismaClient.user.findUnique({
            where: {
                id: user.id
            },
            include: {
                userPreferences: true
            }
        })

        if (!foundUser)
            return res.sendStatus(404)

        const refreshToken = await tokenService.generateRefreshToken(String(foundUser?.email), String(foundUser?.userPreferences?.type), String(foundUser?.id), Boolean(foundUser?.userPreferences?.status), String(foundUser?.name), String(foundUser.phoneNumber), new Date(String(foundUser?.birthDate)), String(foundUser.profileImage))
        const accessToken = await tokenService.generateAccessToken(String(foundUser?.email), String(foundUser?.userPreferences?.type), String(foundUser?.id), Boolean(foundUser?.userPreferences?.status), String(foundUser?.name), String(foundUser.phoneNumber), new Date(String(foundUser?.birthDate)), String(foundUser.profileImage))

        if (!refreshToken || !accessToken)
            return res.sendStatus(403)

        const updatedUser = await prismaClient.user.update({
            where: { 
                email: user.email
            },
            data: {
                refreshToken: refreshToken
            }
        })

        if (!updatedUser)
            return res.sendStatus(409)

        return res.status(200).json({ accessToken: `Bearer ${accessToken}` }).cookie("jwt", refreshToken, { httpOnly: true, maxAge: 24 * 60 * 60 * 1000 }).redirect("http://localhost:3000/")


    } catch(error) {
        next(error)
    }

}

const googleAuthController = {
    googleAuthLogin
}

export default googleAuthController