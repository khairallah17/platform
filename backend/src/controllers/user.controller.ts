import userService from "../services/user.service";
import { NextFunction, Request, Response } from "express";

const getAllUsers = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const users = await userService.getAllUsers()

        return res.status(200).json(users)

    } catch (error) {
        next(error)
    }

}

const getUserById = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.status(403).json({ error: "empty fields required" })

    try {

        const user = await userService.getUserById(id)

        return res.status(200).json({ user })
        
    } catch (error) {
        next(error)
    }

}

const updateUser = async (req: Request, res: Response, next: NextFunction) => {

    const { id, details } = req.body

    if (!id)
        return res.sendStatus(400)

    if (!details)
        return res.status(400).json({message: "no data to update"})

    try {

        const updateUser = await userService.updateUser(id, details)

        return res.sendStatus(200)

    } catch (error: any) {

        next(error)

    }

}

const activateUser = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    try {
        
        const activatedUser = await userService.activateUser(id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const deleteUser = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.body

    try {

        const deletedUser = await userService.deleteUser(id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const approveUser = async (req: Request, res: Response, next: NextFunction) => {
    
    const { id } = req.params

    try {

        const approvedUser = await userService.approveUser(id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }
}

const userController = {
    getAllUsers,
    getUserById,
    updateUser,
    activateUser,
    deleteUser
}

export default userController