import { NextFunction, Request, Response } from "express"
import dotenv from "dotenv"
import UserModel from "../models/user.model"
import userService from "../services/user.service"
import authService from "../services/auth.service"
import { User as PrismaUser } from "@prisma/client"

dotenv.config()


const authRegisterController = async (req:Request, res:Response, next: NextFunction) => {

    const { email, password,  name, role, phoneNumber } = req.body

    // console.log(req.body)
    // console.log(req.files)

    if (!email || !password || !name || !role || !phoneNumber)
        return res.status(400).json({error: "empty fields required"})

    const userData: UserModel = {
        ...req.body,
        files: req.files
    }

    try {

        await userService.createNewUser(userData)

        return res.status(200).json({message: "user created successfully"})

    } catch (error:any) {

        next(error)

    }

}

const authLoginController = async (req:Request, res: Response, next: NextFunction) => {
    
    const { email, password } = req.body

    // CHECK FOR EMPTY FIELDS
    if (!email || !password)
        return res.status(401).json({error: "empty fields required"})

    try {

        const [accessToken, refreshToken] = await authService.login(email, password)

        // SEND THE REFRESH TOKEN TO THE FRONTEND USING HTTPONLY COOKIE
        return res.status(200).cookie('jwt', refreshToken, { 
            httpOnly: true,
            maxAge: 24 * 60 *60 * 1000,
            path: "/",
            sameSite: true
        }).json({ accessToken: `Bearer ${accessToken}` })

    } catch (error:any) {
        
        next(error)

    }
}

const authLogoutController = async (req: Request, res: Response, next: NextFunction) => {

    const cookie = req.cookies

    if (!cookie?.jwt)
        return res.sendStatus(204)

    const refreshToken = cookie.jwt

    try {

        const deletedUser = await authService.logout(refreshToken)

        return res.sendStatus(200)

    } catch (error: any) {
        
        next(error)

    }

}


const authController = {
    authRegisterController,
    authLoginController,
    authLogoutController
}

export default authController