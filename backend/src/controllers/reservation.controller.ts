import { NextFunction, Request, Response } from "express";
import reservationService from "../services/reservation.service";
import { Reservation } from "@prisma/client";

const createNewReservation = async (req: Request, res: Response, next: NextFunction) => {

    const { annoucementId, userId, fullName, phoneNumber, seats } = req.body

    if (!annoucementId)
        return res.status(400).json({ error: "annoucementId required" })

    if (!userId || !fullName || !phoneNumber || !seats)
        return res.status(400).json({ error: "empty fields required" })    

    const reservationData: Reservation = {
        ...req.body
    }

    try {
        
        const createdReservation = await reservationService.createNewReservation(reservationData)

        return res.sendStatus(200)

    } catch (error: any) {
        
        throw new Error(error)

    }

}

const getReservationsByAnnoucement = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.status(400).json({ error: "annoucement id required" })

    try {

        const reservations = await reservationService.getReservationsByAnnoucement(id)

        return res.status(200).json({reservations})

    } catch (error: any) {

        next(error)

    }

}

const getReservationsByUser = async (req: Request, res:Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.status(400).json({error: "user id required"})

    try {
        
        const reservations = await reservationService.getReservationsByUser(id)

        return reservations

    } catch (error: any) {
        
        next(error)

    }

}

const updateReservation = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params
    const data = req.body

    if (!id || !data)
        return res.status(400).json({error: "missing data"})

    try {

        const updatedReservation = reservationService.updateRservation(id, data)

        return updateReservation

    } catch (error) {
        next(error)
    }

}

const reservationController = {
    createNewReservation,
    getReservationsByAnnoucement,
    getReservationsByUser
}

export default reservationController