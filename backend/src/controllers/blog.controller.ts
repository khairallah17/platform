import { NextFunction, Request, Response } from "express";
import blogService from "../services/blog.service";
import { Articles } from "@prisma/client";

const createNewArticle = async (req: Request, res: Response, next: NextFunction) => {

    const { title, content, image } = req.body

    if (!title || !content || !image)
        return res.sendStatus(400)

    try {
        
        const articleData: Articles = {
            ...req.body
        }

        const createdArticle = await blogService.createNewArticle(articleData)

        return res.sendStatus(201)

    } catch (error) {
        next(error)
    }

}

const updateArticle = async (req: Request, res: Response, next: NextFunction) => {

    const { id, details } = req.body

    if (!id)
        return res.sendStatus(400)

    if (!details)
        return res.status(400).json({message: "no details to update"})

    try {

        const updatedArticle = await blogService.updateArticle(id, details)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const deleteArticle = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.sendStatus(400)

    try {
        
        const deletedArticle = await blogService.deleteArticle(id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const getAllArticles = async (req: Request, res: Response, next: NextFunction) => {

    try {
        
        const articles = await blogService.getArticles()

        return res.status(200).json({articles})

    } catch (error) {
        next(error)
    }

}

const getArticleById = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.sendStatus(400)

    try {


    } catch (error: any) {
        next(error)
    }

}

const blogController = {
    createNewArticle,
    updateArticle,
    deleteArticle,
    getAllArticles,
    getArticleById
}

export default blogController