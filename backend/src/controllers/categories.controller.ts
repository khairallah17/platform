import { NextFunction, Request, Response } from "express";
import CategoriesService from "../services/categories.service";

const createNewCategory = async (req: Request, res: Response, next: NextFunction) => {

    const { name } = req.body

    if (!name)
        return res.sendStatus(400)

    try {
        
        const createdCategory = await CategoriesService.createNewCategory(name)

        return res.sendStatus(201)

    } catch (error: any) {
        next(error)
    }
}

const updateCategory = async (req: Request, res: Response, next: NextFunction) => {

    const { name } = req.params

    if (!name)
        return res.sendStatus(400)

    try {

        const updatedCategorie = await CategoriesService.updateCategorie(name)

        return res.sendStatus(200)

    } catch (error: any) { 
        next(error)
    }

}

const deleteCategorie = async (req: Request, res: Response, next: NextFunction) => {

    const { name } = req.params

    if (!name)
        return res.sendStatus(400)

    try {

        const deletedCategorie = await CategoriesService.deleteCategorie(name)

        return res.sendStatus(200)

    } catch (error: any) {
        next(error)
    }

}

const getAllCategories = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const categories = await CategoriesService.getAllCAtegories()

        let array = Object.values(categories).map(vl => Object.values(vl))

        const set = [...new Set(array.flat())]

        return res.status(200).json({categories: set})

    } catch (error) {
        next(error)
    }

}

const categoriesController = {
    createNewCategory,
    updateCategory,
    deleteCategorie,
    getAllCategories
}

export default categoriesController