import { NextFunction, Request, Response } from "express";
import AnnouncementService from "../services/announcement.service";
import { Annonce, User as PrismaUser } from "@prisma/client";
import { AnnouncementDateTime } from "../models/AnnouncementDateTime.model";
import userService from "../services/user.service";

const createAnnouncement = async (req: Request, res: Response, next: NextFunction) => {

    const { title, description, duration, details, city, address, categoryName, seats, dateTime } = req.body
    const { file } = req

    if (!req.user)
        return res.sendStatus(400)

    const { id } = req.user as PrismaUser

    if (!title || !description || !duration || !details || !city || !address || !categoryName || !seats || !dateTime)
        return res.status(400).json({error: "empty fields required"})

    const foundUser = await userService.getUserById(id)

    if (!foundUser)
        return res.status(400).json({error: "no user found"})

    const anouncementData = {
        title: title as string,
        description: description as string,
        details: JSON.stringify(details),
        city: city as string,
        address: address as string,
        seats: seats as number,
        announcerId: foundUser.id as string,
        categoryName: categoryName as string
    }

    try {
        
        const createdAnnoucement = await AnnouncementService.createAnnouncement(anouncementData as Annonce, id, categoryName)

        return res.sendStatus(200)

    } catch (error: any) {
        
        next(error)

    }

}

const updateAnnoucement = async (req: Request, res: Response, next: NextFunction) => {

    const { id, details } = req.body

    if (!id)
        return res.sendStatus(400)

    if (!details)
        return res.sendStatus(400).json({message: "no data to update"})

    try {

        const annoucementData: Annonce = {
            ...details
        }
        
        const updatedAnnoucement = await AnnouncementService.updateAnnouncement(annoucementData, id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const deleteAnnoucement = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.sendStatus(400)

    try {

        const deletedAnnoucement = await AnnouncementService.deleteAnouncement(id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const getAllAnnoucement = async (req: Request, res: Response, next: NextFunction) => {

    try {

        const annoucements = await AnnouncementService.getAllAnnoucement()

        return res.status(200).json({annoucements})

    } catch (error) {
        next(error)
    }

}

const getAnnoucementById = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        return res.sendStatus(200)

    try {

        const annoucements = await AnnouncementService.getAnnoucementById(id)

        return res.status(200).json({annoucements})

    } catch (error) {
        next(error)
    }

} 

const approveAnnoucement = async (req: Request, res: Response, next: NextFunction) => {

    const { id } = req.params

    if (!id)
        res.sendStatus(400)

    try {

        const announcement = await AnnouncementService.approveAnnoucement(id)

        return res.sendStatus(200)

    } catch (error) {
        next(error)
    }

}

const AnnoucementController = {
    createAnnouncement,
    updateAnnoucement,
    deleteAnnoucement,
    getAllAnnoucement,
    getAnnoucementById,
    approveAnnoucement
}

export default AnnoucementController