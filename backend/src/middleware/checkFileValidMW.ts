import { Request, Response, NextFunction } from "express"

export const checkFileValidMW = (req: Request, res: Response, next: NextFunction) => {
    if (!req.file || !req.file.filename)
    {
        return res.status(400).json({...req.file})
    }
    next()
}