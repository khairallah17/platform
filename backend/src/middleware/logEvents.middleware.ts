import morgan from "morgan";
import fs from "fs"
import path from "path";
import express from "express";

const router = express.Router()
const accessLogStream = fs.createWriteStream(path.join(__dirname, '../logs/access.log'), { flags: "a" })

export const eventsLogger = morgan("combined", { stream: accessLogStream })