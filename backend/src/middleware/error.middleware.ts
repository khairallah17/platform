import { ErrorRequestHandler, NextFunction, Request, Response } from "express";
import { isHttpError } from "http-errors"

const errorHandler: ErrorRequestHandler = 
    (error: Error, req: Request, res: Response, next: NextFunction) => {

        // console.error(error)

        let statusCode = 500
        let errorMessage = "internal server error"

        if (isHttpError(error)) {
            console.log("HTTP ERROR DETECTED")
            statusCode = error.status
            errorMessage = error.message
        }

        res.status(statusCode).json({
            error: {
                message: errorMessage
            }
        })

    }

export default errorHandler