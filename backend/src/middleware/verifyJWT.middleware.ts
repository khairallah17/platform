import jwt from "jsonwebtoken";
import dotenv from "dotenv"
import { NextFunction, Request, Response } from "express";

dotenv.config()

const verifyJWT = (req: Request, res: Response, next: NextFunction) => {

    const token = req.headers['authorization']?.split(' ')[1]

    if (!token) 
        return res.sendStatus(401)
    
    jwt.verify(
        token,
        String(process.env.ACCESS_TOKEN_SECRET),
        (error, decoded: any) => {
            if (error)
                return res.sendStatus(403)
            req.user = decoded
            next()
        }
    )

}

export default verifyJWT