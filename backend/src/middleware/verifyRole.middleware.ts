import { NextFunction, Request, Response } from "express"
import userService from "../services/user.service"

const verifyRoles = (allowedRoles: string[]) => {

    return async (req: Request, res: Response, next: NextFunction) => {

        const { jwt } = req.cookies

        if (!jwt)
            return res.sendStatus(400)

        try {

            const role = await userService.getUserByRefreshToken(jwt)

            if (!role)
                return res.sendStatus(400)

            // check if the user roles is allowed then check for the proper roles for the client in the passed roles
            const resultRoles = allowedRoles.includes(role)
    
            if (!resultRoles)
                return res.sendStatus(401)
    
            next()

        } catch (error) {
            return res.sendStatus(400)
        }
    }

}

export default verifyRoles