import { MockContext, Context, createMockContext } from '../context'

let mockCtx: MockContext
let ctx: Context

beforeEach(() => {
    mockCtx = createMockContext()
    ctx = mockCtx as unknown as Context
})

interface createUserPreference {
    type: string,
    status: boolean
}

interface CreateUser {
    name: string
    email: string
    password: string,
    phoneNumber: string,
    birthDate: Date,
    userPreferences: createUserPreference
}

export async function createUser(user: CreateUser, ctx: Context) {

    return await ctx.prisma.user.create({
        data: user,
    })

}

createUser({
    name: "tester",
    email: "test@gmail.com",
    password: "123456",
    phoneNumber: "+2127011170313",
    birthDate: new Date(),
    userPreferences: {
        status: true,
        type: "ADMIN"
    }
}, ctx)
