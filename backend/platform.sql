-- MySQL dump 10.13  Distrib 8.0.35, for Linux (x86_64)
--
-- Host: localhost    Database: platform
-- ------------------------------------------------------
-- Server version	8.0.35-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AnnoucementDateAndTime`
--

DROP TABLE IF EXISTS `AnnoucementDateAndTime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AnnoucementDateAndTime` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `startDate` datetime(3) NOT NULL,
  `endDate` datetime(3) NOT NULL,
  `announcementId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `AnnoucementDateAndTime_announcementId_fkey` (`announcementId`),
  CONSTRAINT `AnnoucementDateAndTime_announcementId_fkey` FOREIGN KEY (`announcementId`) REFERENCES `Announcements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnoucementDateAndTime`
--

LOCK TABLES `AnnoucementDateAndTime` WRITE;
/*!40000 ALTER TABLE `AnnoucementDateAndTime` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnoucementDateAndTime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnnoucementInWishlist`
--

DROP TABLE IF EXISTS `AnnoucementInWishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AnnoucementInWishlist` (
  `annoucementId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`annoucementId`,`userId`),
  KEY `AnnoucementInWishlist_userId_fkey` (`userId`),
  CONSTRAINT `AnnoucementInWishlist_annoucementId_fkey` FOREIGN KEY (`annoucementId`) REFERENCES `Announcements` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT `AnnoucementInWishlist_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnoucementInWishlist`
--

LOCK TABLES `AnnoucementInWishlist` WRITE;
/*!40000 ALTER TABLE `AnnoucementInWishlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnoucementInWishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AnnouncementImages`
--

DROP TABLE IF EXISTS `AnnouncementImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `AnnouncementImages` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `annoucementId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `annoucementImage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `AnnouncementImages_annoucementId_fkey` (`annoucementId`),
  CONSTRAINT `AnnouncementImages_annoucementId_fkey` FOREIGN KEY (`annoucementId`) REFERENCES `Announcements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AnnouncementImages`
--

LOCK TABLES `AnnouncementImages` WRITE;
/*!40000 ALTER TABLE `AnnouncementImages` DISABLE KEYS */;
/*!40000 ALTER TABLE `AnnouncementImages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Announcements`
--

DROP TABLE IF EXISTS `Announcements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Announcements` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `announcerId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updatedAt` datetime(3) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` json DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seats` int NOT NULL,
  `filled` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Announcements_title_key` (`title`),
  KEY `Announcements_announcerId_fkey` (`announcerId`),
  KEY `Announcements_categoryName_fkey` (`categoryName`),
  CONSTRAINT `Announcements_announcerId_fkey` FOREIGN KEY (`announcerId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Announcements_categoryName_fkey` FOREIGN KEY (`categoryName`) REFERENCES `Categories` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Announcements`
--

LOCK TABLES `Announcements` WRITE;
/*!40000 ALTER TABLE `Announcements` DISABLE KEYS */;
/*!40000 ALTER TABLE `Announcements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Articles`
--

DROP TABLE IF EXISTS `Articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Articles` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updatedAt` datetime(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Articles`
--

LOCK TABLES `Articles` WRITE;
/*!40000 ALTER TABLE `Articles` DISABLE KEYS */;
/*!40000 ALTER TABLE `Articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Categories`
--

DROP TABLE IF EXISTS `Categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Categories` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updatedAt` datetime(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Categories_name_key` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categories`
--

LOCK TABLES `Categories` WRITE;
/*!40000 ALTER TABLE `Categories` DISABLE KEYS */;
INSERT INTO `Categories` VALUES ('07bab155-305a-40cc-9ffc-1cdc2ca6d09c','poterie','2023-09-19 12:17:38.298','2023-09-19 12:17:38.298'),('3e39469d-844e-11ee-acf6-00163e000000','Manuel','2023-11-16 08:03:28.000','2023-11-16 08:03:28.000'),('4eaa32c0-844e-11ee-acf6-00163e000000','artistique','2023-11-16 08:03:55.000','2023-11-16 08:03:55.000'),('534f05dc-844e-11ee-acf6-00163e000000','Culinaire','2023-11-16 08:04:03.000','2023-11-16 08:04:03.000'),('58bd236f-844e-11ee-acf6-00163e000000','multimedia','2023-11-16 08:04:12.000','2023-11-16 08:04:12.000'),('63a4e38a-844e-11ee-acf6-00163e000000','nature','2023-11-16 08:04:30.000','2023-11-16 08:04:30.000'),('a948ac9c-f1e4-4dc8-887b-22d8bd1b21e0','minuisier','2023-09-19 12:19:45.981','2023-09-19 12:19:45.981');
/*!40000 ALTER TABLE `Categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Comments`
--

DROP TABLE IF EXISTS `Comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Comments` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `announcementId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` double NOT NULL,
  `createdAt` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updatedAt` datetime(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Comments_userId_key` (`userId`),
  UNIQUE KEY `Comments_announcementId_key` (`announcementId`),
  CONSTRAINT `Comments_announcementId_fkey` FOREIGN KEY (`announcementId`) REFERENCES `Announcements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Comments_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Comments`
--

LOCK TABLES `Comments` WRITE;
/*!40000 ALTER TABLE `Comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `Comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reservations`
--

DROP TABLE IF EXISTS `Reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Reservations` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fullName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `annoucementId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seats` int NOT NULL,
  `dateAndTime` datetime(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Reservations_annoucementId_key` (`annoucementId`),
  UNIQUE KEY `Reservations_userId_key` (`userId`),
  CONSTRAINT `Reservations_annoucementId_fkey` FOREIGN KEY (`annoucementId`) REFERENCES `Announcements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Reservations_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reservations`
--

LOCK TABLES `Reservations` WRITE;
/*!40000 ALTER TABLE `Reservations` DISABLE KEYS */;
/*!40000 ALTER TABLE `Reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Univers`
--

DROP TABLE IF EXISTS `Univers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Univers` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Univers_name_key` (`name`),
  KEY `Univers_categoryId_fkey` (`categoryId`),
  CONSTRAINT `Univers_categoryId_fkey` FOREIGN KEY (`categoryId`) REFERENCES `Categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Univers`
--

LOCK TABLES `Univers` WRITE;
/*!40000 ALTER TABLE `Univers` DISABLE KEYS */;
/*!40000 ALTER TABLE `Univers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserCategory`
--

DROP TABLE IF EXISTS `UserCategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UserCategory` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userPreferenceId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserCategory_categoryName_key` (`categoryName`),
  CONSTRAINT `UserCategory_categoryName_fkey` FOREIGN KEY (`categoryName`) REFERENCES `Categories` (`name`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserCategory`
--

LOCK TABLES `UserCategory` WRITE;
/*!40000 ALTER TABLE `UserCategory` DISABLE KEYS */;
INSERT INTO `UserCategory` VALUES ('006f98df-d88b-4984-a6a1-10ba3bd68017','artistique','ae18aa57-d69c-4c96-9e5f-0991de91b8f5'),('1efc60cd-eccf-4b83-8d10-d0f66a2fdc7b','nature','35f2b189-ad9a-4532-adb3-27f2643036fe');
/*!40000 ALTER TABLE `UserCategory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserPreferences`
--

DROP TABLE IF EXISTS `UserPreferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `UserPreferences` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `userId` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `WorkshopImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `certification` tinyint(1) DEFAULT NULL,
  `certificationImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `professionType` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `workshop` tinyint(1) DEFAULT NULL,
  `professionLevel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `categoryName` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UserPreferences_userId_key` (`userId`),
  UNIQUE KEY `UserPreferences_categoryName_key` (`categoryName`),
  CONSTRAINT `UserPreferences_categoryName_fkey` FOREIGN KEY (`categoryName`) REFERENCES `UserCategory` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `UserPreferences_userId_fkey` FOREIGN KEY (`userId`) REFERENCES `Users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserPreferences`
--

LOCK TABLES `UserPreferences` WRITE;
/*!40000 ALTER TABLE `UserPreferences` DISABLE KEYS */;
INSERT INTO `UserPreferences` VALUES ('32f1ee7e-8cdb-4091-a78e-c02b9c7efc3a','CLIENT','fae21077-5e2b-4cfe-a22d-2ca7bd0fb513',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('35f2b189-ad9a-4532-adb3-27f2643036fe','CREATOR','7b5ffd2d-74c2-4c8a-bd7f-704bf68da285',0,'public/uploads/workshop',1,'public/uploads/certification','undefined',1,'professionel',NULL),('71302c8a-5d15-4327-b04e-588ba485f7f9','CLIENT','085c7c04-51ab-41c5-9e07-3571df6b709e',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('8a1734e6-eac9-47ad-a832-1e6a89f4dc34','CLIENT','894b2642-942e-4619-81df-c39f1bfbe03d',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('a4fdcf83-1eca-4d37-9942-a96c691759d2','CLIENT','65dc8444-3da0-431e-b987-64ede58dc844',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('ae18aa57-d69c-4c96-9e5f-0991de91b8f5','CREATOR','27a2582d-bc46-4f97-a922-73f03248bfb8',0,'public/uploads/workshop',1,'public/uploads/certification','undefined',1,'professionel',NULL),('ea819c8a-1149-49c4-bf07-e02e832e18e6','ADMIN','380fd8fd-bbbc-47a7-a37c-ca2c43eb70ff',1,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `UserPreferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `Users` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdAt` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `updatedAt` datetime(3) NOT NULL,
  `birthDate` datetime(3) DEFAULT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `googleId` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `refreshToken` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `profileImage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Users_email_key` (`email`),
  UNIQUE KEY `Users_googleId_key` (`googleId`),
  KEY `Users_email_idx` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES ('085c7c04-51ab-41c5-9e07-3571df6b709e','s.haliba@gmail.com','haliba saad','$2b$10$KqGL0kkxG96IN7Huj4TpFuBrfPBZ4c7/tMF8.f3iL5blLSJp2Y8dS','2023-11-14 20:41:31.062','2023-11-16 08:13:10.439',NULL,'0701170313',NULL,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InMuaGFsaWJhQGdtYWlsLmNvbSIsInJvbGVzIjoiQ0xJRU5UIiwiaWF0IjoxNzAwMTIyMzkwLCJleHAiOjE3MDAyOTUxOTB9.We8F84ciIwfci1JnldqgLAMY0uomv7dp6w8cULl3to0',0,NULL),('27a2582d-bc46-4f97-a922-73f03248bfb8','createur@gmail.com','createur 1','$2b$10$VlX3gHvqpQL6MWuYa8zXQe4OQ9a7J9S59.c2HOhG54XNSsmygVdAW','2023-11-16 07:08:37.256','2023-11-27 16:42:44.222',NULL,'0701170313',NULL,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImNyZWF0ZXVyQGdtYWlsLmNvbSIsInJvbGVzIjoiQ1JFQVRPUiIsImlkIjoiMjdhMjU4MmQtYmM0Ni00Zjk3LWE5MjItNzNmMDMyNDhiZmI4Iiwic3RhdHVzIjpmYWxzZSwibmFtZSI6ImNyZWF0ZXVyIDEiLCJwaG9uZU51bWJlciI6IjA3MDExNzAzMTMiLCJiaXJ0aERhdGUiOm51bGwsInByb2ZpbGVJbWFnZSI6Im51bGwiLCJpYXQiOjE3MDExMDMzNjQsImV4cCI6MTcwMTI3NjE2NH0.vjhNPn9byoB3zfhUDjgOpOUtTHkY6N3n1eZ34kTob1w',0,NULL),('380fd8fd-bbbc-47a7-a37c-ca2c43eb70ff','admin@gmail.com','tester','$2b$10$4KZsaq3IPxR0353IEpYO/uqRTZixRNVfszmsZmpaCH50JR0iRYax2','2023-10-01 13:03:32.376','2023-11-27 16:38:56.233','2023-10-01 13:03:32.305','+212701170313',NULL,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImFkbWluQGdtYWlsLmNvbSIsInJvbGVzIjoiQURNSU4iLCJpZCI6IjM4MGZkOGZkLWJiYmMtNDdhNy1hMzdjLWNhMmM0M2ViNzBmZiIsInN0YXR1cyI6dHJ1ZSwibmFtZSI6InRlc3RlciIsInBob25lTnVtYmVyIjoiKzIxMjcwMTE3MDMxMyIsImJpcnRoRGF0ZSI6IjIwMjMtMTAtMDFUMTM6MDM6MzIuMDAwWiIsInByb2ZpbGVJbWFnZSI6Im51bGwiLCJpYXQiOjE3MDExMDMxMzYsImV4cCI6MTcwMTI3NTkzNn0.FeVOXypWREzP_fXO8j_TFUbWX6413MP1Hed5fGu3rHY',0,NULL),('65dc8444-3da0-431e-b987-64ede58dc844','med.khairallah3@gmail.com','mohammed khairallah','$2b$10$ZjH48ErhON5.VQkIsDUmKe4b6Kd1KtOKEJVtVKHSq92uHQd5tCQje','2023-11-13 21:00:32.630','2023-11-25 13:41:09.232',NULL,NULL,'102147858476339761255','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1lZC5raGFpcmFsbGFoM0BnbWFpbC5jb20iLCJyb2xlcyI6IkNMSUVOVCIsImlkIjoiNjVkYzg0NDQtM2RhMC00MzFlLWI5ODctNjRlZGU1OGRjODQ0Iiwic3RhdHVzIjp0cnVlLCJuYW1lIjoibW9oYW1tZWQga2hhaXJhbGxhaCIsInBob25lTnVtYmVyIjoibnVsbCIsImJpcnRoRGF0ZSI6bnVsbCwicHJvZmlsZUltYWdlIjoibnVsbCIsImlhdCI6MTcwMDkxOTY2OSwiZXhwIjoxNzAxMDkyNDY5fQ.fYdWKU8IB8Nwkcu74Vwr0ahhl06P-IbQn1fcL0STB6Q',0,NULL),('7b5ffd2d-74c2-4c8a-bd7f-704bf68da285','test123@gmail.com','test1231 test123','$2b$10$poWlgEBCpkV0o1uniuKVRuGSUMTKZfT4x9htfvB.9FEY2xBCiGon6','2023-11-16 18:26:39.333','2023-11-16 18:29:22.074',NULL,'01147852369',NULL,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3QxMjNAZ21haWwuY29tIiwicm9sZXMiOiJDUkVBVE9SIiwiaWQiOiI3YjVmZmQyZC03NGMyLTRjOGEtYmQ3Zi03MDRiZjY4ZGEyODUiLCJzdGF0dXMiOmZhbHNlLCJuYW1lIjoidGVzdDEyMzEgdGVzdDEyMyIsInBob25lTnVtYmVyIjoiMDExNDc4NTIzNjkiLCJiaXJ0aERhdGUiOm51bGwsInByb2ZpbGVJbWFnZSI6Im51bGwiLCJpYXQiOjE3MDAxNTkzNjIsImV4cCI6MTcwMDMzMjE2Mn0.zzQgNQ6dSXhg9rwvbU1ZfFObIE7eJnov2aIb7ix4D6Y',0,NULL),('894b2642-942e-4619-81df-c39f1bfbe03d','a.laabouss@gmail.com','laabouss ayoub','$2b$10$NBl.PV4S048QwYn3cNsZ6eoLdLLf1AJc0h6GmrI/t4suxABv.dprm','2023-11-15 08:46:57.501','2023-11-15 08:46:57.501',NULL,'0701170313',NULL,NULL,0,NULL),('fae21077-5e2b-4cfe-a22d-2ca7bd0fb513','mohamedkhairallah686@gmail.com','Mohamed Khairallah','$2b$10$9irUGKqsBxEwtYtWNLnzpOF8ZjQJOrmJGL0Vt1rU0WQo509UeUXwK','2023-11-14 11:09:28.464','2023-11-16 18:12:01.440',NULL,NULL,'111767191224044337924','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1vaGFtZWRraGFpcmFsbGFoNjg2QGdtYWlsLmNvbSIsInJvbGVzIjoiQ0xJRU5UIiwiaWQiOiJmYWUyMTA3Ny01ZTJiLTRjZmUtYTIyZC0yY2E3YmQwZmI1MTMiLCJzdGF0dXMiOnRydWUsIm5hbWUiOiJNb2hhbWVkIEtoYWlyYWxsYWgiLCJwaG9uZU51bWJlciI6Im51bGwiLCJiaXJ0aERhdGUiOm51bGwsInByb2ZpbGVJbWFnZSI6Im51bGwiLCJpYXQiOjE3MDAxNTgzMjEsImV4cCI6MTcwMDMzMTEyMX0.CnrqT5Opkp2LpVjktYGoJe40ss_x0pGw4E7H51rAwic',0,NULL);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `_prisma_migrations`
--

DROP TABLE IF EXISTS `_prisma_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `_prisma_migrations` (
  `id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `checksum` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `finished_at` datetime(3) DEFAULT NULL,
  `migration_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logs` text COLLATE utf8mb4_unicode_ci,
  `rolled_back_at` datetime(3) DEFAULT NULL,
  `started_at` datetime(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `applied_steps_count` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `_prisma_migrations`
--

LOCK TABLES `_prisma_migrations` WRITE;
/*!40000 ALTER TABLE `_prisma_migrations` DISABLE KEYS */;
INSERT INTO `_prisma_migrations` VALUES ('014cf2f4-2340-4174-82ab-a5da16b8322c','af85198773b167b7651093c7b8c93528a346683cbf7b336c76337763b4ed3181','2023-09-19 09:05:06.572','20230919090506_adding_approved_status_to_annoucement',NULL,NULL,'2023-09-19 09:05:06.549',1),('0d758918-fdf0-4a92-8127-07fdf6fe3b18','cec6c9d4b9d22d56bd773d03415ec77fc830713ab5b898ad1746e2ff37897966','2023-09-18 14:45:22.857','20230918144522_adding_some_fields_to_annonce_table',NULL,NULL,'2023-09-18 14:45:22.830',1),('14ea3ee6-e417-44bd-be49-80a6ca8f8570','0a54f8bb2afb0fca4ec5fb3686ff04c4371839dd31b20043f2f86d164c23935e','2023-09-14 14:06:36.282','20230914140636_test_relation',NULL,NULL,'2023-09-14 14:06:36.169',1),('36ade7ed-ed5e-4951-b376-ccb9cbc09944','c8c26b268adde65853162c10c6a7e878256dafa710387fed8e86fb12059926e4','2023-11-16 03:47:57.059','20231116034757_adding_profession_level_to_user_preferences',NULL,NULL,'2023-11-16 03:47:57.009',1),('49c407d4-c29b-422f-ae0a-99639df825d6','73f93b413bdcd8269876b0153cc9f6057f90dea1cd25ec65a71a969e3857ac42','2023-09-16 14:25:19.737','20230916142519_removing_role_enum',NULL,NULL,'2023-09-16 14:25:19.679',1),('49fbd573-cb60-44e0-adcd-55c1a6ca855d','f2f59427bad31aa6d4cf1a30ba20de39a79bdc55ff6b8e2097209f92027ad627','2023-09-19 08:43:39.044','20230919084338_adding_cascade_deletion_to_relations',NULL,NULL,'2023-09-19 08:43:38.696',1),('5f8d11fb-c393-4311-96b3-bee609d2b740','d29930d338539eff55e0d2c62a42638053e08701712d5da368176446fdcaa608','2023-11-25 15:53:37.251','20231125155337_adding_univers_announcement_date_and_time_announcement_images_tables_and_updating_their_relations',NULL,NULL,'2023-11-25 15:53:37.012',1),('63748fd1-dde6-4fca-964a-cceff2946e7a','937f26814092b823f6b74c5413570cfaaa0abaa22637a87bd1954911ec670425','2023-09-18 15:05:46.077','20230918150546_updating_details_field',NULL,NULL,'2023-09-18 15:05:46.023',1),('65ea2af7-68d5-4334-a1f9-905c4e135164','e621b4725b3dc11b44bc01ea72a63ad05f5d2442635227661414a3d4f7b30755','2023-09-18 16:49:33.056','20230918164933_adding_a_filled_status_to_annonce_table',NULL,NULL,'2023-09-18 16:49:33.038',1),('7750b764-0dd4-41a1-9546-b50cbc0a8998','eb914dfb4386fe4cb95a586e871565561c93d7811bd26af29f74076d04aa5baf','2023-10-01 13:11:14.465','20231001131114_set_refresh_token_length_to_1000',NULL,NULL,'2023-10-01 13:11:14.436',1),('7febe7dc-c10b-443a-bd99-dc794159eb49','f58fd6abc629583724a8c5b90a247a5532818718a32df21e7c5bfa2a26f48965','2023-09-17 21:27:30.724','20230917212730_adding_unique_attribute_to_google_id',NULL,NULL,'2023-09-17 21:27:30.228',1),('894007aa-1c95-42fe-a33e-c19a0372c40a','9fb4750c727a5c23c76882d2913993c8d9633c135916d025e5d563ef18c79156','2023-11-16 06:53:28.104','20231116065324_adding_user_c_ategory_join_tables_with_relation_with_category_and_user_preferences_table',NULL,NULL,'2023-11-16 06:53:24.755',1),('8e3a8496-c42d-4abb-aaaf-b4e97b2533bc','a0ec3614b54f8a3aa7189b7f7dd633ee65814f6cc85eee0b619e7be409df96c9','2023-11-03 20:57:22.556','20231103205722_adding_date_and_time_for_reservation_table_profile_image_and_annoucement_images',NULL,NULL,'2023-11-03 20:57:22.471',1),('9562e5b0-46b1-4386-801c-bf29ea66363a','05b7755580c4829144c5653f73f15dd734e7bd0de70b0f33a94edd95c5830a69','2023-09-18 14:56:14.305','20230918145614_adding_reservation_relationship_between_annonce_and_users',NULL,NULL,'2023-09-18 14:56:14.244',1),('99ca8677-7a69-41b6-960b-621431c80684','a9c5b76710e585076a501a748fcb8870e283f9a61483236175c19ae91e15396e','2023-11-16 04:49:16.866','20231116044916_changing_references_on_category_user_preference_relation',NULL,NULL,'2023-11-16 04:49:16.787',1),('9aabb3d9-9bb0-4971-bc27-c73537fb8dc4','2df503b48d5f7651f90d0aaa366efaa506dff78b358e02ed55f419eb6f50925d','2023-09-19 17:47:46.765','20230919174746_adding_default_value_to_filled_attribute',NULL,NULL,'2023-09-19 17:47:46.741',1),('a397b10e-2a06-4985-88a4-a26f4e0144ce','e9713301bbca96ded443c687874ee384ad257ee8d9497b72604fc0bfcef86918','2023-09-19 11:53:01.914','20230919115301_create_unique_key_name_on_categories_table',NULL,NULL,'2023-09-19 11:53:01.818',1),('a6bdbc16-070c-483c-88c2-be34313d5b54','d09b18dcaa7bfe4970214b722a8c8620c0ddfe36c7a0c13f747fb372813c1857','2023-09-18 16:48:09.332','20230918164809_adding_number_of_seats_for_reservation_table_and_annonce_table',NULL,NULL,'2023-09-18 16:48:09.291',1),('b09bdf12-4c4d-4f2e-8e86-afc986a661db','344857dd30e5844c60e3874900a462102de300a1219228c71c5e41ddb510a58c','2023-11-04 13:20:49.231','20231104132049_remiving_availability_table_as_it_is_details_json_object_in_annonce',NULL,NULL,'2023-11-04 13:20:49.210',1),('b16c0e05-814c-4358-a16d-33dd42cde1a7','aaf603df1eeaf00e2052440e59d192c8b7ce7220658a14a68b6c80e305ed1bad','2023-09-16 11:29:17.329','20230916112916_users_and_annoucment',NULL,NULL,'2023-09-16 11:29:16.940',1),('b7b9659f-cc3a-49bb-8625-880ace3ade33','a9de48743a96e840c414e3cb515c50b26905229b516333fde9aa57007575f05e','2023-09-16 21:04:25.286','20230916210425_adding_refresh_token_to_user_table',NULL,NULL,'2023-09-16 21:04:25.223',1),('d3230670-df83-4f11-bf7f-f682eef73aa0','a602b33041d0817f296796474511c23d4f1acd5bef90ab68ce5aa63b9ed1924d','2023-09-17 11:10:38.035','20230917111037_removing_status_field_from_users_table',NULL,NULL,'2023-09-17 11:10:38.002',1),('d57a0087-7b9c-4d9c-9780-3460e8e1570c','4bb0bd2662e76365ff775f332a4cadd1685a63473c85be8720c57533d4622bca','2023-11-25 15:58:49.032','20231125155849_reverting_default_announcement_approved_to_true',NULL,NULL,'2023-11-25 15:58:49.017',1),('dce90ebf-343b-49f6-b16c-c4b5f4491a1c','e97a1c57c39f44da3c230c343e4ca6a50f7da40a056b6b9f681aee701f196035','2023-09-18 09:29:17.036','20230918092917_adding_distinction_between_google_accounts_and_native_accounts',NULL,NULL,'2023-09-18 09:29:17.013',1),('dda85b24-dc9a-405a-8394-16b3e481b957','86273e132aacd39f44a2078f8f688d82863023d825b8ef5c45d7cdf5287bf898','2023-09-18 15:48:28.601','20230918154828_adding_description_field_to_the_annonce_table',NULL,NULL,'2023-09-18 15:48:28.543',1),('dfae5141-ba77-43b0-9992-44a83ce2d960','47081283be967c4aab04d7aea5e689c1495e36c537d8648bc7526e6fbcc76216','2023-11-13 03:28:50.050','20231113032849_adding_to_user_preferences_table_creator_related_information',NULL,NULL,'2023-11-13 03:28:49.976',1),('e3347988-3cc9-4d64-afc2-45ae028abd88','20453e9a0f758b2073f3676dffb04e1a5cc7de60fb745ae4775a2822f28b35fa','2023-09-11 12:33:50.547','20230911123350_init',NULL,NULL,'2023-09-11 12:33:50.515',1),('e53f2736-4b83-46e5-9cf0-6a970bd6661c','8dbf1fe7be2571d1812de5fa449e9d42847181ee1c93aad37d20eb93df3d12ea','2023-09-17 11:39:57.051','20230917113956_adding_missing_created_at_and_updated_at_fields',NULL,NULL,'2023-09-17 11:39:56.976',1),('efea09f3-82c8-4c02-8b05-6ecae69f1054','3e8ca88135d716cd7125b94ddb02d7662778f7b2fa0b5a445e0bbe6a7dd56fe4','2023-09-16 11:28:50.481','20230915115124_',NULL,NULL,'2023-09-16 11:28:50.220',1),('f2cdc257-4c92-43a7-82b7-33e0f01358b7','5c9192ff535c50000ad57cc335f686f8c85c414f946e63d697b83ac0a000181d','2023-11-03 20:46:13.797','20231103204613_adding_wishlist_and_verification_tag_for_users',NULL,NULL,'2023-11-03 20:46:13.651',1),('f6b55622-e7f6-4a60-b4cd-10e62c542ef8','67bd0d8b074e7624e904243fcd5be66c8c9e1c7d15fc7666e459fdc6a20eac05','2023-09-18 16:44:10.336','20230918164410_adding_a_join_table_for_reseravation_between_user_and_annoucement',NULL,NULL,'2023-09-18 16:44:10.080',1),('f86c2ef9-86ad-47e3-a9ce-ad3f6c6fbb95','f75fa665fc98eb30ece546bf5ae88d7578a84932570ef997183b9caef1ca0889','2023-09-19 06:11:12.356','20230919061112_adding_some_information_to_reservation_table',NULL,NULL,'2023-09-19 06:11:12.218',1);
/*!40000 ALTER TABLE `_prisma_migrations` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-03  8:28:27
