module.exports = {
    preset: "ts-jest",
    testEnvironment: "node",
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
    },
    // setupFilesAfterEnv: ['<rootDir>/context.ts']
}