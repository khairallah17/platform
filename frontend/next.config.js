const { withNextVideo } = require('next-video/process')

/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    domains: ["localhost:5000"]
  },
}

module.exports = withNextVideo(nextConfig)
