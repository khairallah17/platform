import { useContext } from 'react'
import { AnnoucementContext } from "@/context/AnnoucementContext"

const AnnoucementsHook = () => {
  return (
    useContext(AnnoucementContext)
  )
}

export default AnnoucementsHook