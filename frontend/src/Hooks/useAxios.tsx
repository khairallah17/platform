import axios, { AxiosError, AxiosProxyConfig, AxiosRequestConfig, AxiosResponse } from "axios";
import { useEffect, useState } from "react"

axios.defaults.baseURL = "http://localhost:5000/"

export const useAxios = (axiosParams: AxiosRequestConfig) => {

    const [response, setResponse] = useState<AxiosResponse>();
    const [error, setError] = useState<AxiosError>();
    const [loading, setLoading] = useState(true);

    const fetchData = async (params: AxiosRequestConfig) => {
    
        try {

            const result = await axios.request(params);
            setResponse(result);

        } catch( err: any ) {
            setError(err);
        } finally {
            setLoading(false);
        }
        };

        useEffect(() => {
            fetchData(axiosParams);
        },[]);

        return { response, error, loading };
}