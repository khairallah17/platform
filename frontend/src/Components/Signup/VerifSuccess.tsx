import Image from "next/image";

export const VerifSucc = () => {
  return (
    <div className="w-[420px] h-fit laptop:w-[380px] phone:w-[300px] phone:h-fit flex flex-col items-center justify-start py-3 gap-3">
      <Image
        width={500}
        height={500}
        src="/submit.svg"
        alt=""
        className="w-[60px]"
      ></Image>
      <h1 className="font-bold text-[25px] text-[#0E1A2A] text-center">
        Votre compte est créé avec succès
      </h1>
    </div>
  );
};
