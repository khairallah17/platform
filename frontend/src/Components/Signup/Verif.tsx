import Image from "../../../node_modules/next/image";
import { OTP } from "./OTP";

interface Prop {
  val: boolean;
  setVal: (val: boolean) => void;
}

export const Verif = ({ val, setVal }: Prop) => {
  return (
    <div className="w-[420px] h-fit laptop:w-[380px] phone:w-[300px] phone:h-fit flex flex-col items-center justify-start py-3 gap-3 z-[99]">
      <Image
        width={500}
        height={500}
        src="/phone.svg"
        alt=""
        className="w-[60px]"
      ></Image>
      <h1 className="font-bold text-[25px] text-[#0E1A2A]">
        Vérifier votre boite SMS
      </h1>
      <div className="w-full h-fit">
        <p className=" font-normal text-[19px] text-center text-[#0E1A2A] opacity-50">
          Veuillez entrer le code de sécurité dans votre numéro :{" "}
          <span className="font-bold text-[19px] text-center text-[#0E1A2A]">
            +212 661335643
          </span>
        </p>
      </div>
      <OTP val={val} setVal={setVal} />
    </div>
  );
};
