import React, { FC, useState } from "react";
import { useRef, useEffect } from "react";
import { KeyboardEvent } from "react";
import { useRouter } from "next/router";
interface ApiResponse {
  isValid: boolean;
}

interface Prop {
  val: boolean;
  setVal: (val: boolean) => void;
}
let currentOTPIndex: number = 0;
export const OTP = (props: Prop) => {

  const router = useRouter();
  const test = "The code you entered is incorrect";
  const [otp, setOtp] = useState<string[]>(new Array(6).fill(""));
  const [activeOTPIndex, setactiveOTPIndex] = useState<number>(0);
  const [isValid, setIsValid] = useState<boolean>(true);

  let b: boolean = false;
  let show: boolean = false;
  let num: string = "";

  if (activeOTPIndex === 6) {
    b = true;
    otp.forEach((value) => {
      num += value;
    });
  } else {
    b = false;
  }
  function handleKeyDown(event: KeyboardEvent<HTMLInputElement>): void {
    const { key } = event;
    if (
      (key === "-" || key === "+" || key === "e" || key === ".") &&
      event.currentTarget === document?.activeElement
    ) {
      event.preventDefault();
    }
  }
  const handleSubmit = () => {
    return console.log(num);
  };

  const inputRef = useRef<HTMLInputElement>(null);
  const handleOnChange = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    let { value } = target;
    const newOTP: string[] = [...otp];
    newOTP[currentOTPIndex] = value.substring(value.length - 1);

    if (!value) setactiveOTPIndex(currentOTPIndex - 1);
    else setactiveOTPIndex(currentOTPIndex + 1);
    setOtp(newOTP);
  };
  const handleOnkeyDown = (
    { key }: React.KeyboardEvent<HTMLInputElement>,
    index: number
  ) => {
    currentOTPIndex = index;
    if (key === "Backspace") setactiveOTPIndex(currentOTPIndex - 1);
  };

  useEffect(() => {
    inputRef.current?.focus();
  }, [activeOTPIndex]);

  useEffect(() => {
  
    if (activeOTPIndex !== 7) setIsValid(true);
  }, [currentOTPIndex]);
  return (
    <form onSubmit={handleSubmit}>
      <div className="w-[100%] h-[55px] flex  items-end justify-center">
        <div
          className="w-[305px] h-[40px] flex items-center justify-between
    desktop:w-[305px] desktop:h-[40px]
    laptop:w-[290px] laptop:h-[40px]
    Large-phone:w-[280px] Large-phone:h-[40px]
    phone:w-[260px] phone:h-[40px]"
        >
          {otp &&
            Array.isArray(otp) &&
            otp.map((_, index) => {
              return (
                <React.Fragment key={index}>
                  <input
                    ref={index === activeOTPIndex ? inputRef : null}
                    type="text"
                    className=" w-[40px] h-[40px] outline-none text-[16px] 
    font-semibold text-black leading-[20px] 
    text-center	border-[0.75px] rounded-[3px] border-gray-700
    focus:border-[#000000] focus:ring-[#000000] focus:ring-opacity-50
    focus:ring-2
    desktop:w-[40px] desktop:h-[40px] desktop:text-[16px] desktop:leading-[20px]
    laptop:w-[38px] laptop:h-[38px] laptop:text-[16px] laptop:leading-[20px]
    Large-phone:w-[34px] Large-phone:h-[34px] Large-phone:text-[16px] Large-phone:leading-[20px]
    phone:w-[32px] phone:h-[32px] phone:text-[16px] phone:leading-[20px]
                      focus:text-gray-700
                       transition"
                    onKeyPress={handleKeyDown}
                    onChange={handleOnChange}
                    value={otp[index]}
                    onKeyDown={(e) => {
                      handleOnkeyDown(e, index);
                    }}
                    style={{ borderColor: isValid ? "#414243" : "#FF0000" }}
                  />
                </React.Fragment>
              );
            })}
        </div>
      </div>
      <div className="flex items-center justify-center">
        {!isValid && (
          <h1 className="text-[#FF0000] mt-2 font-sora font-regular text-[8px] leading-[10px]">
            {test}
          </h1>
        )}
      </div>
      <div className="w-[100%] h-fit flex items-end justify-center mt-8">
        <button onClick={() =>
        {
          props.setVal(true);
        }} className="bg-components-bold w-[200px] h-[48px] flex items-center justify-center text-white font-trebuchet font-bold text-[20px] rounded-[5px] phone:text-[17px] cursor-pointer">
          Confirmer
        </button>
      </div>
      <div className="w-full h-fit mt-8">
        <h1 className=" font-normal text-[12px] text-[#0E1A2A] ">
          Vous n&apos;avez pas reçu l&apos;e-mail ?{" "}
          <button className="text-[#4E3265] font-medium cursor-pointer">
            Cliquez pour renvoyer
          </button>
        </h1>
      </div>
    </form>
  );
};
