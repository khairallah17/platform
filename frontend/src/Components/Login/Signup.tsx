import { AiFillEye, AiFillEyeInvisible } from "react-icons/ai";
import GetContext from "@/context/UserContext";
import { ChangeEvent, FormEvent } from "react";
import Router from "next/router";
import { MdErrorOutline } from "react-icons/md";

interface Prop {
  showCheck: boolean;
  showCheck2: boolean;
  handleShowCheck: () => void;
  handleShowCheck2: () => void;
  showPassword: boolean;
  showPassword2: boolean;
  handleShowPassword: () => void;
  handleShowPassword2: () => void;
  showAccepte: boolean;
  handleShowAccepte: () => void;
  formSubmit: (e: FormEvent<HTMLFormElement>) => void,
  error: boolean,
  errorMsg: string
}
export const SignUpComponent = ({
  showCheck,
  showCheck2,
  handleShowCheck,
  handleShowCheck2,
  showPassword,
  showPassword2,
  handleShowPassword,
  handleShowPassword2,
  showAccepte,
  handleShowAccepte,
  formSubmit,
  error,
  errorMsg
}: Prop) => {

  const {
    inputMail, setInputMail,
    inputPassword, setInputPassword,
    inputRetypePassword, setInputRetypePassword,
    inputName, setInputName,
    inputLastName, setInputLastName,
    inputPhone, setInputPhone,
  } = GetContext()

  const submitForm1 = () => {

  }

  return (
    <div className="w-full h-fit bg-white px-8 py-4 rounded-[20px] flex flex-col items-center justify-start">
      
      <div className={`error bg-red-300 w-full rounded-md p-2 ${error ? "flex" : "hidden"} items-center gap-3`}>
        <MdErrorOutline className="text-red-500" size={32} />
        {error && <p>{errorMsg}</p>}
      </div>
      
      <form onSubmit={formSubmit} className="w-full h-fit mt-5 phone:mt-3 flex flex-col phone:gap-3">
        <div>
          <h4 className="font-gotham font-normal text-[16px]">Nom</h4>
          <input
          
            value={inputName}
            onChange={(e: ChangeEvent<HTMLInputElement>) => setInputName(e.target.value)}
            type="text"
            placeholder="Saisissez votre nom"
            className=" pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 border-[#00000080] rounded-[10px] px-2 outline-none"
          />
        </div>
        <div>
          <h4 className="font-gotham font-normal text-[16px] mt-4 phone:mt-3">
            Prenom
          </h4>
          <input
          
            value={inputLastName}
            onChange={(e: ChangeEvent<HTMLInputElement>) => setInputLastName(e.target.value)}
            type="text"
            className=" pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 border-[#00000080] rounded-[10px] px-2 outline-none"
            placeholder="Saisissez votre prenom"
          />
        </div>
        <div>
          <h4 className="font-gotham font-normal text-[16px] mt-4 phone:mt-3">
            Email
          </h4>
          <input
          
            value={inputMail}
            onChange={(e: ChangeEvent<HTMLInputElement>) => setInputMail(e.target.value)}
            type="email"
            className=" pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 border-[#00000080] rounded-[10px] px-2 outline-none"
            placeholder="Saisissez votre email"
          />
        </div>
        <div>
          <h4 className="font-gotham font-normal text-[16px] mt-4 phone:mt-3">
            Numero de Telephone
          </h4>
          <input
          
            value={inputPhone}
            onChange={(e: ChangeEvent<HTMLInputElement>) => setInputPhone(e.target.value)}
            type="tel"
            className=" pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 border-[#00000080] rounded-[10px] px-2 outline-none"
            placeholder="Saisissez votre numero de telephone"
          />
        </div>
        <div>
          <h4 className="font-gotham font-normal text-[16px] mt-4 phone:mt-3">
            Mot De Passe
          </h4>
          <div className="w-full h-[45px] phone:h-[40px] relative">
            <input
              value={inputPassword}
              onChange={(e: ChangeEvent<HTMLInputElement>) => setInputPassword(e.target.value)}
            
              type={showPassword2 ? "text" : "password"}
              className=" pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 border-[#00000080] rounded-[10px] px-2 outline-none"
              placeholder="Saisissez votre mot de passe"
            />
            {showPassword2 ? (
              <AiFillEye
                onClick={handleShowPassword2}
                className="absolute right-3 top-[2.2rem] phone:top-[1.7rem] transform -translate-y-1/2 text-[20px] phone:text-[18px] text-[#9747FF] cursor-pointer"
              />
            ) : (
              <AiFillEyeInvisible
                onClick={handleShowPassword2}
                className="absolute right-3 top-[2.2rem] phone:top-[1.7rem] transform -translate-y-1/2 text-[20px] phone:text-[18px] text-[#9747FF] cursor-pointer"
              />
            )}
          </div>
        </div>
        <div>
          <h4 className="font-gotham font-normal text-[16px] mt-8 phone:mt-3">
            Retaper votre Mot De Passe
          </h4>
          <div className="w-full h-[45px] phone:h-[40px] relative">
            <input
            
              type={showPassword ? "text" : "password"}
              value={inputRetypePassword}
              onChange={(e: ChangeEvent<HTMLInputElement>) => setInputRetypePassword(e.target.value)}
              className=" pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 border-[#00000080] rounded-[10px] px-2 outline-none"
              placeholder="Retaper votre mot de passe"
            />
            {showPassword ? (
              <AiFillEye
                onClick={handleShowPassword}
                className="absolute right-3 top-[2.2rem] phone:top-[1.7rem] transform -translate-y-1/2 text-[20px] phone:text-[18px] text-[#9747FF] cursor-pointer"
              />
            ) : (
              <AiFillEyeInvisible
                onClick={handleShowPassword}
                className="absolute right-3 top-[2.2rem] phone:top-[1.7rem] transform -translate-y-1/2 text-[20px] phone:text-[18px] text-[#9747FF] cursor-pointer"
              />
            )}
          </div>
        </div>

        <div className="w-fit h-fit mt-10 phone:mt-3 flex items-start justify-start gap-2 ">
          <input type="checkbox" name="terms and conditions" id="" />
          <h4 className=" font-normal text-[12px] text-[#26313F] opacity-[0.6]">
            J&apos;accepte Terms & conditions (Confidentialité et partage).
          </h4>
        </div>
        <div className="w-full h-fit mt-8 phone-mt-2 flex items-center phone:justify-center justify-end mb-4">
          <button type="submit" className="bg-components-bold w-[220px] h-[48px] text-white font-trebuchet font-bold text-[20px] rounded-[5px] phone:text-[17px] cursor-pointer">
            M&apos;inscrire
          </button>
        </div>
      </form>
    </div>
  );
};
