import { useEffect } from 'react'
import useAuth from './useAuth'
import { useRouter } from 'next/router'

const AuthWrapper = <P extends Record<string, unknown>>(
  WrappedComponent: React.ComponentType<P>
) => {
  
  const Wrapper = (props: P) => {
    const { isLogged, loading, userData } = useAuth();
    const router = useRouter();

    useEffect(() => {
      if (!loading && !isLogged) {
        router.push('/Sign_in');
      }
    }, [loading, isLogged, router]);

    return isLogged ? <WrappedComponent {...props} user={userData} /> : null;
  };

  return Wrapper;
}

export default AuthWrapper