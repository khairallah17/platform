import { useState, useEffect } from 'react'
import { jwtDecoder } from '@/services/services'

interface UserData {
    email:  string,
    role:   string,
    id:     string
}

const useAuth = () => {

    const [ isLogged, setIsLogged ] = useState<boolean>(false)
    const [ loading ,setLoading ] = useState<boolean>(true)
    const [ userData, setUserData ] = useState<UserData | null>(null)

    useEffect(() => {

        const token = localStorage.getItem("accessToken")

        if (token) {

            const decoded = jwtDecoder(token)

            setUserData({
                email: String(decoded?.email),
                role: String(decoded?.roles),
                id: String(decoded?.id)
            })

            setIsLogged(true)
            setLoading(false)

        }

        else
            setLoading(false)

    },[])

  return ({ isLogged, loading, userData })
}

export default useAuth