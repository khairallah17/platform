import { BiSearch } from "react-icons/bi";
import { IoSettingsSharp } from "react-icons/io5";
import { AiOutlineClear } from "react-icons/ai";

export const Filter = () => {

  const categories = ["categorie","multimedia", "artistique", "culinaire", "nature", "manuel"]
  const cities = [ "ville", "Casablanca", "Rabat", "Marrakech", "Fes", "Tangier", "Agadir", "Meknes", "Oujda", "Kenitra", "Tetouan", "Essaouira", "Nador", "El Jadida", "Chefchaouen", "Fez", "Ouarzazate", "Taroudant", "Erfoud", "Taza", "Beni Mellal", "Azrou", "Ifrane", "Zagora",]
  const size = ["nombre de personnes","10 personnes", "50 personnes", "+50 personnes"]

  return (
    <div className="w-full flex items-center h-[100px] phone:h-[150px] bg-[#410053] px-42 justify-center laptop:px-12 tablet:px-1 phone:px-1">
      <div className="w-3/4 h-full  phone:h-1/2 flex items-center justify-center gap-4">
        <div className="w-full phone:w-[250px] bg-yellow-50 relative flex items-center justify-center rounded-[4px] overflow-hidden">
          <input
            type="text"
            className="w-full outline-none h-full font-gotham font-semibold text-[16px] phone:text-[13px] text-[#000] pl-12 p-4 "
            placeholder="Entrer un mots cles ..."
          />
          <BiSearch className="absolute left-2 text-[#000] text-[20px]" />
        </div>
        <select placeholder="catégories" className='custom-select w-full p-4 outline-none phone:w-[22%] phone:h-[65%] rounded-[5px] font-trebuchet font-medium text-[18px] phone:text-[12px]'>
          {
            categories.map((cat, key) => (
              <option key={key} value={cat}>{cat}</option>
            ))
          }
        </select>
        <select className='custom-select w-full p-4 outline-none phone:w-[22%] phone:h-[65%] rounded-[5px] font-trebuchet font-medium text-[18px] phone:text-[12px]'>
          {
            cities.map((city, index) => (
              <option key={index} value={city}>{city}</option>
            ))
          }
        </select>
        <select className='custom-select w-full p-4 outline-none phone:w-[22%] phone:h-[65%] rounded-[5px] font-trebuchet font-medium text-[18px] phone:text-[12px]'>
          {
            size.map((sz, index) => (
              <option key={index} value={sz}>{sz}</option>
            ))
          }
        </select>
        <IoSettingsSharp size={150} className="text-white phone:hidden" />
        <AiOutlineClear size={150} className="text-white phone:hidden" />
        <button className="h-[65%] w-full bg-components-bold flex items-center justify-center cursor-pointer font-bold text-[17px] text-white rounded-[4px] phone:hidden ">
          Recherche
        </button>
      </div>
      <div className=" w-3/12 hidden phone:flex items-center justify-end gap-4 ">
        <button className="h-[65%] w-full px-5 bg-components-bold  cursor-pointer font-bold text-[17px] text-white rounded-[4px] hidden phone:flex phone:items-center phone:justify-center ">
          Recherche
        </button>
      </div>
    </div>
  );
};
