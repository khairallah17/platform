interface StepsProps {
  id: number;
  header: string;
  par: string;
}

export const Steps = ({ id, header, par }: StepsProps) => {
  return (
    <div className="w-[30%] phone:w-full h-fit  flex items-start justify-start gap-3">
      <div className="w-[40px] mt-3 h-[40px] tablet:w-[50px] tablet:h-[50px] phone:w-[45px] phone:h-[45px] bg-components-bold rounded-full flex items-center justify-center">
        <h1 className="capitalize font-bold text-[20px] text-white laptop:text-[18px] phone:text-[15px]">{id}</h1>
      </div>
      <div className="w-[70%] h-fit py-2">
        <h1 className="capitalize text-normal font-gotham text-[22px] tablet:text-[18px] phone:text-[15px] font-bold leading-normal text-black">
          {header}
        </h1>
        <p className=" text-left font-gotham text-[15px] tablet:text-[14px] phone:text-[13px] font-normal text-black">
          {par}
        </p>
      </div>
    </div>
  );
};
