import Link from "next/link";

export const LoginSection = () => {
    return (
        <div className="w-full sec1 my-20">
            <div className="container mx-auto grid grid-cols-2 phone:grid-cols-1 tablet-grid-cols-1">
              <div className="w-full h-full pt-24 pb-24 flex flex-col items-center justify-center gap-4">
              <h1 className="capitalize font-trebuchet font-bold text-[30px] laptop:text-[26px] phone:text-[20px]">
                Artisans, artistes, créateurs & créatifs
              </h1>
              <div className="w-[80%]">
                <p className="font-trebuchet font-medium text-[20px] text-[#000000A8] text-center laptop:text-[15px] phone:text-[11px] ">
                  Vous souhaitez proposer un atelier et faire partie de notre
                  communauté ?
                </p>
              </div>
              <Link href="/Signup/creator/step1" className="flex items-center justify-center bg-components-bold px-12 py-2 text-white font-trebuchet font-bold text-[20px] hover:bg-transparent border-2 border-components-bold duration-200 hover:text-components-bold rounded phone:text-[17px] cursor-pointer">
                Rejoignez nous
              </Link>
              </div>
              <div className="w-full h-full pt-24 pb-24 flex flex-col items-center justify-center gap-4">
                <h1 className=" font-trebuchet font-bold text-[30px] laptop:text-[26px] phone:text-[20px]">
                  Curieux ou déjà Client ?
                </h1>
                <div className="w-[80%]">
                  <p className="font-trebuchet font-medium text-[20px] text-[#000000A8] text-center laptop:text-[15px] phone:text-[11px] ">
                    Connectez-vous pour retrouver vos ateliers favoris, bénéficier
                    d&apos;avantages et et d&apos;offres promotionnelles
                  </p>
                </div>
                <Link href="/Signup/client" className="flex items-center justify-center bg-components-bold px-12 py-2 text-white font-trebuchet font-bold text-[20px] hover:bg-transparent border-2 border-components-bold duration-200 hover:text-components-bold rounded phone:text-[17px] cursor-pointer">
                  M&apos;inscrire
                </Link>
              </div>
            </div>
        </div>
    )
    };