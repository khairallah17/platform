import { Steps } from "@/Components/LandingPage/Steps";
import Video from "next-video"
import Youtube from "react-youtube"

export const WhoWeAre = () => {
  return (
    <div className="container mx-auto my-20">
          <div className="w-full flex items-center h-fit justify-center laptop:flex-col laptop:justify-start phone:p-4 laptop:items-center laptop:gap-2 tablet:flex-col tablet:justify-start tablet:items-center tablet:gap-2 phone:flex-col phone:justify-start phone:items-center phone:gap-2">
            <div className="w-full h-fit flex flex-col items-center gap-4 laptop:w-full tablet:w-full phone:w-full">
              <h1 className="font-gotham w-full text-left text-[32px] font-bold leading-normal text-[#000]">
                Qui sommes nous ?
              </h1>
              <div className="w-full h-fit">
                <p className=" text-left w-full font-gotham text-[20px] font-medium text-black">
                  Des créatifs passionnés qui souhaitent vous offrir des expériences
                  inoubliables.
                </p>
              </div>
              <div className="w-full h-fit flex items-start justify-between gap-2  phone:flex-col phone:items-start phone:justify-around">
                <Steps
                  id={1}
                  header={"explorer"}
                  par={
                    "Découvrez nos univers créatifs et nos savoir-faires et explorez de nouvelles activités insolites"
                  }
                />
                <Steps
                  id={2}
                  header={"Expérimentez"}
                  par={
                    "Réservez votre place chez nos artisans et créateurs et mettez la main à la pâte en vivant l’expérience aux côtés des meilleurs"
                  }
                />
                <Steps
                  id={3}
                  header={"Créez"}
                  par={
                    "Repartez avec vos créations et inscrivez-vous si vous souhaitez faire partie de notre communauté et bénéficier d’avantages"
                  }
                />
              </div>
              <button className="font-gotham font-normal bg-components-bold capitalize border-2 border-components-bold self-start hover:bg-white hover:text-components-bold duration-300 text-white px-16 rounded py-2">
                en savoir plus
              </button>
            </div>
            <iframe src="https://www.youtube.com/embed/bH-pNsiylTc" className="w-full h-auto phone:mt-4 tablet:mt-4 aspect-[2/1] border-none" allow='autoplay; encrypted-media' title='video'></iframe>
          </div>
    </div>
  );
};
