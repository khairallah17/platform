import { useState } from "react";
import { VscSettings } from "react-icons/vsc";
import { BiSearch } from "react-icons/bi";
import { Swiper, SwiperSlide } from "swiper/react"
import { AutoplayEvents } from "swiper/types";
import Image from "next/image";

import "swiper/css"

interface MainProp {
    value: string;
    setValue : (color : string) => void;
  }

export default function MainSection(props : MainProp) {

    const categories = ["categorie","multimedia", "artistique", "culinaire", "nature", "manuel"]
    const cities = [ "ville", "Casablanca", "Rabat", "mohammedia", "marrakech", "fes", "tangier", "agadir", "esssaouira", "assilah", "tetouan"]
    const size = ["nombre de personnes","10 personnes", "50 personnes", "+50 personnes"]

    const [showfilter, setShowfilter] = useState(false);
    const onchangedatainput = (e : any) => {
        props.setValue(e.target.value);
      }
      function handleShowFilter(){
        setShowfilter(!showfilter);
      }
    return (
        <div className="relative">
            <div className='h-[68%] w-1/2 phone:w-full absolute top-[20%] phone:top-[20%] left-[10%] z-50 p-4 phone:left-0'>
                <div className='w-full h-[65%] phone:h-[50%] '>
                  <div className='w-full -full'>
                    <h1 className=' font-trebuchet font-bold text-[44px] laptop:text-[40px] tablet:text-[36px] phone:text-[30px] text-[#FFFBFC]'>
                    Vivez l&apos;experience avec nos maitres artisans, nos artistes, nos créateurs et créatifs
                    </h1>
                  </div>
                </div>
                <div className='w-full h-[35%] '>
                    <div className={`w-[650px] bg-[#D8D8D8] overflow-hidden  phone:w-[320px] ${showfilter ? 'h-[180px] phone:h-[140px]' : 'h-[90px] phone:h-[70px]'}  rounded-[7px]`}
                    style={ {
                      transition: 'height 0.5s ease-in-out'
                    }}
                    >
                      <div className='w-full h-[90px] phone:h-[70px] flex justify-center items-start '>
                        <div className='w-[15%] h-[90px] phone:h-[70px] phone:w-[22%] bg-white rounded-l-[7px] flex items-center justify-center' >
                            <VscSettings color="black" className="w-[33px] h-[33px] phone:w-[25px] phone:h-[25px] cursor-pointer" onClick={handleShowFilter}/>
                        </div>
                        <div className='w-[85%] h-[90px] phone:h-[70px]  bg-[#D0CFCF] rounded-r-[7px] phone:w-[78%] relative flex items-center'>
                          <input type="text" className='w-[100%] h-[100%] outline-none pl-4 bg-[#D0CFCF] font- font-semibold text-[17px] phone:text-[14px] tracking-[1.2px]' placeholder='Search' value={props.value} onChange={onchangedatainput} />
                            <BiSearch color="black" className=" absolute right-4 w-[30px] h-[30px] cursor-pointer phone:w-[22px] phone:h-[22px] "/>
                        </div>
                      </div>
                      <div className='w-full h-[90px] phone:h-[70px] flex items-center justify-around gap-4 px-4 '>
                          <select placeholder="catégories" className='custom-select w-full rounded py-2 font-trebuchet font-medium text-[18px] phone:text-[12px]'>
                            {
                              categories.map((cat, key) => (
                                <option key={key} value={cat}>{cat}</option>
                              ))
                            }
                          </select>
                          <select className='custom-select w-full rounded py-2 font-trebuchet font-medium text-[18px] phone:text-[12px]'>
                            {
                              cities.map((city, index) => (
                                <option key={index} value={city}>{city}</option>
                              ))
                            }
                          </select>
                          <select className='text-ellipsis custom-select w-full rounded py-2 font-trebuchet font-medium text-[18px] phone:text-[12px]'>
                            {
                              size.map((sz, index) => (
                                <option key={index} value={sz} className=" text-ellipsis">{sz}</option>
                              ))
                            }
                          </select>
                      </div>
                    </div>
                </div>
            </div>
            <div className="backdrop-filter absolute w-full h-full top-0 left-0 bg-black z-30 bg-opacity-25">

            </div>

            <Swiper className="mySwiper" autoplay={{ delay: 3000 }}>
                <SwiperSlide>
                  <div style={{backgroundImage: `url("/slide-1.jpg")`}} className="w-full h-[780px] bg-cover bg-center"></div>
                </SwiperSlide>
                <SwiperSlide>
                <div style={{backgroundImage: `url("/slide-2.jpg")`}} className="w-full h-[780px] bg-cover bg-center"></div>
                </SwiperSlide>
                <SwiperSlide>
                  <div style={{backgroundImage: `url("/slide-3.jpg")`}} className="w-full h-[780px] bg-cover bg-center"></div>
                </SwiperSlide>
            </Swiper>

        </div>
    );
}