import Card from '@/Components/LandingPage/Card'

export default function NSection()
{
    return (
      <div className='container mx-auto mb-10 phone:p-4 '>
        <h1 className=' font-trebuchet font-bold text-[28px] phone:text-[22px] mb-8'>Nos nouveautés</h1>
        <div className='grid gap-4 phone:grid-cols-1 tablet:grid-cols-2 laptop:grid-cols-3 grid-cols-5 justify-between xl:justify-start overflow-x-hidden'>
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
        </div>
      </div>
    )
}