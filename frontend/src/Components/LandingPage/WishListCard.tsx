import { GrLocation, GrCalendar } from "react-icons/gr";
import { BiCategoryAlt } from "react-icons/bi";
import Link from "../../../node_modules/next/link";
export const WishListCard = () => {
  return (
    <div className="w-full h-fit rounded-[10px] flex items-center tablet:w-[65%] phone:w-[80%] justify-start overflow-hidden tablet:flex-col phone:flex-col shadow-lg ">
      <div className="w-[35%] h-[300px] phone:h-[200px] tablet:px-4 tablet:w-full phone:w-full bg-orange-600 Article-back"></div>
      <div className="w-[65%] tablet:w-full phone:w-full h-full px-4 pt-2 flex flex-col items-start justify-start gap-3 ">
        <h1 className="font-gotham font-normal text-[26px] text-[#000]">
          Notre Offre
        </h1>
        <div className="w-fit h-fit flex items-center justify-start gap-3 flex-wrap">
          <div className="w-fit h-fit flex gap-2 items-center">
            <GrLocation className="text-[#000] text-[20px]" />
            <h1 className="font-gotham text-[18px] font-bold">Rabat</h1>
          </div>
          <div className="w-fit h-fit flex gap-2 items-center">
            <GrCalendar className="text-[#000] text-[20px]" />
            <h1 className="font-gotham text-[18px] font-bold">14 Octobre</h1>
          </div>
          <div className="w-fit h-fit flex gap-2 items-center">
            <BiCategoryAlt className="text-[#000] text-[20px]" />
            <h1 className="font-gotham text-[18px] font-bold">Artistique</h1>
          </div>
        </div>
        <div className="w-[95%]">
          <p className="font-gotham font-light text-[#5E5E5E] text-[12px] phone:hidden">
            Lorem ipsum dolor sit amet consectetur. Varius elementum neque
            feugiat vel diam pellentesque quis tempor commodo. Nullam proin quis
            nisi nibh amet pulvinar tristique. Aenean in commodo sed feugiat in
            nunc elementum adipiscing. Nulla tortor nisi eget feugiat ridiculus
            pharetra. Volutpat nec placerat non aliquet suspendisse quis amet
            velit. Purus in eu pellentesque malesuada nulla in nulla. Mattis
            magna justo parturient tempor donec bibendum fusce duis ut. Urna
            vivamus varius mattis ut sodales enim. Ipsum enim sed gravida
            suspendisse quam.
          </p>
        </div>
        <div className="w-full flex items-center justify-between self-end">
          <h1 className="font-gotham font-bold text-[20px] text-[#757575]">
            1/100
          </h1>
          <Link
            href="/Article"
            className="w-[100px] h-[40px]  bg-components-bold rounded-[5px] flex items-center justify-center cursor-pointer font-trebuchet font-bold text-[10px] text-white tablet:mb-3  phone:mb-3"
          >
            Reserver
          </Link>
        </div>
      </div>
    </div>
  );
};
