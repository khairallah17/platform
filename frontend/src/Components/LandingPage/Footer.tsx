import Image from "next/image";
import {
  BiLogoFacebook,
  BiLogoTwitter,
  BiLogoLinkedinSquare,
  BiLogoYoutube,
} from "react-icons/bi";
import Link from "next/link";

export const Footer = () => {
  return (
    <div className="w-full p-8 self-end h-fit bg-[#EFDCF9] flex items-start justify-between tablet:justify-between laptop:justify-between phone:h-fit  phone:flex-col phone:items-start px-8">
      <div className="w-full phone:w-fit h-full">
        <Image
          src="/Header/Logo.svg"
          alt=""
          width={70}
          height={70}
        />
        <div className="h-fit mt-2 ">
          <p className="font-gotham font-normal text-[12px]">
            Lorem Ipsum is simply dummy text of the printing and typesetting
          </p>
        </div>
        <h1 className="font-gotham font-bold text-[18px] text-[#4E3265] mt-4">
          Suivez nous:
        </h1>
        <div className="w-fit h-fit flex items-center justify-start gap-2 mb-[10px]">
          <Link href="#">
            <BiLogoFacebook className="text-black text-[20px]" />
          </Link>
          <BiLogoLinkedinSquare className="text-black text-[20px]" />
          <BiLogoTwitter className="text-black text-[20px]" />
          <BiLogoYoutube className="text-black text-[20px]" />
        </div>
      </div>
      <div className="w-full h-full laptop:hidden tablet:hidden phone:hidden">
        <h1 className="font-gotham font-bold text-[22px] text-[#000]">
          Notre Offre
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Nos ateliers
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Nos catégories
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Par ville
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Cartes Cadeaux
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Boxes cadeaux
        </h1>
      </div>
      <div className="w-full h-full laptop:hidden tablet:hidden phone:hidden">
        <h1 className="font-gotham font-bold text-[22px] text-[#000]">
          Nos sélections
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Groupe plus de 10
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Entreprises
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Expériences exclusives
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . En extérieur
        </h1>
      </div>
      <div className="w-full h-full laptop:hidden tablet:hidden phone:hidden">
        <h1 className="font-gotham font-bold text-[22px] text-[#000]">
          A propos
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Qui sommes nous
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Rejoignez notre équipe
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Parrainage
        </h1>
        <h1 className="font-gotham font-normal text-[15px] text-[#000] mt-1">
          . Affiliation
        </h1>
      </div>
      <div className="w-full h-full phone:self-start tablet:pl-4 phone:mt-4">
        <h1 className="font-gotham font-bold text-[22px] text-[#000]">
          Inscrivez-vous à notre Newsletter
        </h1>
        <div className="w-full h-fit mt-2">
          <p className="font-gotham font-normal text-[12px]">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been
          </p>
        </div>
        <div className="w-full h-fit flex mt-2 gap-2 flex-wrap">
          <input
            type="text"
            placeholder="Email"
            id=""
            className="phone:w-full font-medium text-[#000] outline-none py-2 pl-4 rounded-[3px]"
          />
          <button className="phone:w-full font-bold rounded-[3px] text-[15px] text-white bg-components-bold px-[25px] py-[10px]">
            Submit
          </button>
        </div>
      </div>
    </div>
  );
};
