import Card from '@/Components/LandingPage/Card'

export default function PSection()
{
    return (
      <div className='container mx-auto p-4'>
        <h1 className=' font-trebuchet font-bold text-[28px] phone:text-[22px] mb-8'>Les plus populaires</h1>
        <div className='grid gap-4 phone:grid-cols-1 tablet:grid-cols-3 laptop:grid-cols-3 grid-cols-5 justify-between xl:justify-start overflow-x-hidden'>
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
        </div>
      </div>
    )
}