import Image from "next/image";
import { AiOutlineHeart, AiOutlineMenu } from "react-icons/ai";
import { MdLanguage } from "react-icons/md";
import { BsPersonFill } from "react-icons/bs";
import Link from "next/link";
import { useEffect, useState } from "react";
import { RxCross1 } from "react-icons/rx"
import GetContext from "@/context/UserContext";
import { useRouter } from "next/router";
import { IoMdArrowDropdown } from "react-icons/io";

export const Header = () => {

  const router  = useRouter()

  const { logout, isAuthenticated, globalAuthChecker, userRole } = GetContext()

  const [ showSettings, setShowSettings ] = useState<boolean>(false)
  const [ showDropdown, setShowDropDown ] = useState<boolean>(false)
  const [ dropDownItem, setDropDownItems ] = useState<Array<dropDown>>([])
  const [ showSubList, setShowSubList ] = useState<number>(-1)
  const [ sublist, setSublist ] = useState<Array<string>>([])

  interface dropDown {
    title:  string,
    items:  Array<string>,
    link?:  string
  }

  const navbar = [  {name: "nos ateliers", link: "/search"},
                    {name: "nos selections", link: "/noselections"},
                    {name: "cadeaux", link: "/cadeaux"},
                    {name: "à propos", link: "/apropos"}];

  const mobileNav = [ [
                        {title: "Par catégorie", items: ["Artistique", "Manuel", "Culinaire", "Nature", "Multimedia"], link: "/savoir-plus"},
                        {title: "Par ville ", items: ["casabalanca", "rabat", "fes", "temara", "tanger"], link: "/savoir-plus"},
                        {title: "Par évènements", items: ["Anniversaires"], link: "/savoir-plus"}
                      ],
                      [
                        {title: "", items: ["group plus de 10", "Entreprises", "Expériences exclusives", "En extérieur"], link: "/savoir-plus"}
                      ],
                      [
                        {title: "", items: ["Cartes cadeaux", "Cartes cadeaux entreprise", "Boxe cadeaux"], link: "/savoir-plus"}
                      ]]

  const atelier = [ {title: "Par catégorie", items: ["Artistique", "Manuel", "Culinaire", "Nature", "Multimedia"], link: "/savoir-plus"},
                    {title: "Par ville ", items: ["casabalanca", "rabat", "fes", "temara", "tanger"], link: "/savoir-plus"},
                    {title: "Par évènements", items: ["Anniversaires"], link: "/savoir-plus"}]

  const selection = [ {title: "", items: ["group plus de 10", "Entreprises", "Expériences exclusives", "En extérieur"], link: "/savoir-plus"} ]

  const cadeaux = [ {title: "", items: ["Cartes cadeaux", "Cartes cadeaux entreprise", "Boxe cadeaux"], link: "/savoir-plus"} ]

  const settings = [{name: isAuthenticated ? "DECONNEXION" : "", link: "", onClick: isAuthenticated ? logout : null},
                    {name: userRole == "CLIENT" || userRole == "" ? "" : "DASHBOARD", link: userRole == "CREATOR" ? "/Dashboard" : userRole == "ADMIN" ? "/admin/home" : "", onclick: null}]

  const showDropdownFunc = (query: string) => {
    console.log(query)
    if (query.toUpperCase() == "NOS ATELIERS")
      setDropDownItems(atelier)
    else if (query.toUpperCase() == "NOS SELECTIONS")
      setDropDownItems(selection)
    else if (query.toUpperCase() == "CADEAUX")
      setDropDownItems(cadeaux)
    else 
      return setShowDropDown(false)
    setShowDropDown(true)
  }

  const hideDropDownFunc = () => {
      setShowDropDown(false)
  }

  const showSettingsSetter = () => setShowSettings(!showSettings)

  useEffect(() => {
    globalAuthChecker()
  }, [])

  const [ openMenu, setOpenMenu ] = useState<boolean>(false)

  return (
    <div className="w-full h-[135px] bg-[#EFDCF9] px-24 gap-12 laptop:px-11  tablet:px-8 phone:px-8 flex  items-center relative justify-between">
      <div className="w-[70px]  mr-4 flex items-center justify-start  h-[100px]">
        <Link href="/">
          <Image
            className="w-[70px] laptop:w-[60px] tablet:w-[50px] phone:w-[35px]"
            src="/Header/Logo.svg"
            alt=""
            width={100}
            height={100}
          />
        </Link>
      </div>
      <div className="header-list flex items-center justify-around w-full gap-4  h-[100px]  laptop:hidden tablet:hidden phone:hidden mr-2">
        {
          navbar.map(({name, link}, index) => (
            <Link
              href={link}
              key={index}
              onMouseEnter={() => showDropdownFunc(name)}
              className=" font-trebuchet font-bold text-[18px] laptop:text-[15px] capitalize"
            >
              {name}
            </Link>
          ))
        }
      </div>
      <div
        onMouseLeave={hideDropDownFunc} 
        className={`absolute bg-components px-64 py-5 w-full h-fit gap-12 top-full right-0 flex ${showDropdown ? "opacity-1 z-[99]" : "opacity-0 z-[-10]"} duration-200`}>
        {
          dropDownItem.map(({title, items, link}, key) => (
            <ul key={key} className="flex flex-col gap-3">
              {
                title && <li className="font-bold mb-2 capitalize text-lg">{title}</li>
              }
              {
                items.map((item, key) => (
                  <li key={key} className="cursor-pointer border-b-2 border-transparent hover:border-b-2 duration-200 hover:border-components-bold">
                    <Link href={`/search/${item}`}>
                      {item}
                    </Link>
                  </li>
                ))
              }
            </ul>
          ))
        }
      </div>
      <div className="w-1/3  h-[100px]  flex items-center justify-center laptop:hidden tablet:hidden phone:hidden">
        <Link
          href="/search"
          className="text-center w-[340px] laptop:w-[270px] h-[50%] bg-components-bold flex items-center rounded-[5px] justify-center text-white font-trebuchet font-bold text-[20px] laptop:text-[16px]"
        >
          COMMENCER L&apos;EXPERIENCE
        </Link>
      </div>

      {/* MOBILE MENU */}
      <div className={`fixed z-40 h-screen w-[80%] ${openMenu ? "right-0" : "right-[-2000px]"} z-[999] duration-700 p-10 justify-around ease-in-out top-0 flex-col bg-components hidden laptop:flex tablet:flex phone:flex`}>
          <div className="cursor-pointer bg-components-bold h-fit p-2 flex-shrink w-fit rounded-md" onClick={() => setOpenMenu(false)}>
            <RxCross1 size={20} color={"white"} />
          </div>

          <div className="menu flex-1 flex justify-start items-center max-h-[70%]">
            <ul className="flex flex-col justify-around h-full w-full">

              {
                // mobileNav.map((item: any, index) => {
                //   return item?.map(({title, link, items}: any, index: number) => (
                //     <li key={index} className="relative text-xl">
                //       <div className="flex w-full items-center justify-between">
                //         <Link href={link}>
                //           {title}
                //         </Link>
                //         {
                //           title !== "A PROPOS" && <IoMdArrowDropdown size={32} className={`duration-500`} /> 
                //         }
                //       </div>
                //       {
                //         title !== "A PROPOS" && 
                //           <div className={``}>
                //             {
                //               items?.map((title: string, index:number) => (
                //                 <div key={index}>
                //                   <h3 className="font-bold capitalize">{title}</h3>
                //                   <ul>
                //                     {
                //                       items.map((it: any, index: number) => (
                //                         <li key={index}>
                //                           {it}
                //                         </li>
                //                       ))
                //                     }
                //                   </ul>
                //                 </div>
                //               ))
                //             }
                //           </div>
                //       }
                //     </li>
                //   ))
                // })
              }

              {
                settings.map(({name, link, onClick}, key) => (
                  <li key={key} className=" text-xl">
                    <Link href={link}>
                      {name}
                    </Link>
                  </li>
                ))
              }

            </ul>

            
          </div>

          <div className="self-start h-[100px] flex items-center justify-start w-full">
            <Link
              href="/Sign_in"
              className="text-center w-[340px] laptop:w-[270px] h-[50%] bg-components-bold flex items-center rounded-[5px] justify-center text-white font-trebuchet font-bold text-md laptop:text-[16px]"
            >
              COMMENCER L&apos;EXPERIENCE
            </Link>
          </div>

      </div>
      <div className={`backdrop fixed h-screen w-screen z-30 ${openMenu ? "right-0" : "right-[-2000px]"} z-[998] duration-700 ease-in-out top-0 bg-black bg-opacity-30 hidden laptop:flex tablet:flex phone:flex`}></div>
      {/* END MOBILE MENU */}

      <div className="header-icons flex items-center justify-end w-1/5 h-[100px] gap-[10px] phone:w-[80%] tablet:w-[80%]">
        {
          isAuthenticated && 
          <div className="w-[50px] h-[50px] bg-components-bold rounded-[5px]  flex items-center justify-center tablet:w-[40px] cursor-pointer tablet:h-[40px] phone:w-[30px] phone:h-[30px]">
            <Link href="/Wishlist">
              <AiOutlineHeart
                color="white"
                className="w-[30px] h-[30px] tablet:w-[23px] tablet:h-[23px] phone:w-[18px] phone:h-[18px]"
              />
            </Link>
          </div>
        }
        <div className="w-[50px] h-[50px] bg-components-bold rounded-[5px] flex items-center justify-center tablet:w-[40px] tablet:h-[40px] phone:w-[30px] phone:h-[30px]">
          <MdLanguage
            color="white"
            className="w-[30px] h-[30px] tablet:w-[23px] tablet:h-[23px] phone:w-[18px] phone:h-[18px]"
          />
        </div>
        <div className="w-[50px] h-[50px] bg-components-bold rounded-[5px] relative flex   items-center justify-center tablet:w-[40px] tablet:h-[40px] phone:w-[30px] phone:h-[30px]">
          <button onClick={isAuthenticated ? showSettingsSetter : () => router.push("/Sign_in")} >
            <BsPersonFill
              color="white"
              className="w-[30px] h-[30px] tablet:w-[23px] tablet:h-[23px] phone:w-[18px] phone:h-[18px]"
            />
          </button>
          <div className={`absolute w-48 h-fit ${showSettings ? "flex" : "hidden"} duration-200 flex flex-col top-[120%] right-0 z-50 bg-white`}>
              {
                settings.map(({name, onClick, link}, key) => {
                  if (name)
                    return (
                        <Link onClick={onClick} className="hover:bg-components-bold duration-200 hover:text-white p-4 font-trebuchet" href={link} key={key}>
                          <p>{name}</p>
                        </Link>
                    )
                  else
                      return 
                })
              }
          </div>
        </div>
        <div onClick={() => setOpenMenu(true)} className="w-[50px] h-[50px] cursor-pointer bg-components-bold rounded-[5px]  hidden laptop:flex phone:flex tablet:flex  items-center justify-center tablet:w-[40px] tablet:h-[40px] phone:w-[30px] phone:h-[30px]">
          <AiOutlineMenu
            color="white"
            className="w-[30px] h-[30px] tablet:w-[23px] tablet:h-[23px] phone:w-[18px] phone:h-[18px]"
          />
        </div>
      </div>
    </div>
  );
};
