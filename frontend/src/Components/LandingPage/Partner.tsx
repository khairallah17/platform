import Image from "next/image";
import Link from "next/link";

export const Partner = () => {
  return (
    <div className="mb-20 w-full h-fit px-24 laptop:px-11  tablet:px-8 phone:px-6 flex flex-col items-center justify-start gap-2">
      <h1 className="font-gotham font-bold text-[30px] text-[#000] laptop:text-[25px] phone:text-[20px]">
        Nos partenaires
      </h1>
      <p className="font-gotham font-light capitalize text-md">
        intéressés par des collaborations créatives à fort impact ?
      </p>
      <div className="w-[70%] tablet:w-[100%] phone:w-[100%] h-fit p-5 mt-2 logos">
        <div className="logos-slide w-fit ">
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/credit.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/ibm.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/toyota.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/shell.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/ocp.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/meta.png" alt="" width={60} height={60} />
          </div>
        </div>
        <div className="logos-slide w-fit ">
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/credit.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/ibm.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/toyota.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/shell.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/ocp.png" alt="" width={60} height={60} />
          </div>
          <div className="w-[120px] h-[120px] bg-white flex items-center justify-center rounded-full shadow-md hover:shadow-2xl">
            <Image src="/meta.png" alt="" width={60} height={60} />
          </div>
        </div>
      </div>
      <div className="w-[80%] laptop:w-[70%] tablet:w-[80%] phone:w-[92%] mt-2">
        <p className="font-gotham font-light text-xl leading-relaxed text-center">
          Intéressés par des collaborations créatives à fort impact ? Contribuez
          à l&apos;expérience et explorez de nouvelles opportunités en faisant partie
          de notre réseau de partenaires privilégiés !
        </p>
      </div>
      <Link href="/partners" className="mt-4 hover:bg-white hover:text-components-bold border hover:border-components-bold duration-200 rounded bg-components-bold px-12 py-2 text-white font-trebuchet font-bold text-[25px] phone:text-[20px] cursor-pointer">
        DEVENIR PARTENAIRE
      </Link>
    </div>
  );
};
