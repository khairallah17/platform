import {
  AiOutlineHeart,
  AiOutlineStar,
  AiOutlineCheckCircle,
} from "react-icons/ai";
import { useState } from "react";
import Link from "next/link";
import Image from "next/image";


export default function Card() {

  const [showhover, setShowhover] = useState(false);
  const [changecolor, setChangeColor] = useState(false);

  function handleMouseEnter() {
    setShowhover(true);
  }

  function handleMouseLeave() {
    setShowhover(false);
  }
  function handleChange() {
    setChangeColor(!changecolor);
  }
  return (
    <div
      className="inline-block  bg-[#F9F4FF] rounded-[15px] shadow-lg"
      onMouseEnter={() => {
        handleMouseEnter();
      }}
      onMouseLeave={() => {
        handleMouseLeave();
      }}
    >
      <div className="rounded-[15px] overflow-hidden flex flex-col items-center justify-between relative">
        <Image src="/card_image.png" height={600} width={600} className="" sizes="100vw" loading="lazy" alt="" />
        <div
          className={`"${showhover ? "flex z-50" : "hidden z-[-99]"} flex-col items-center justify-between  absolute top-0 left-0 w-full h-full bg-[#0000004f] transition-all duration-150 ease-in-out"`}
        >
          <div className="flex flex-col items-center justify-between h-full">
            <div className="flex items-start justify-between self-start w-full p-2">
              <div className="flex gap-3">
                <div className="px-2 py-1 rounded-full bg-[#E54934BD] flex items-center justify-center">
                  <h1 className="font-trebuchet font-semibold text-[9px] text-white">
                    Example
                  </h1>
                </div>
                <div className="px-2 py-1 rounded-full bg-[#E54934BD] flex items-center justify-center">
                  <h1 className="font-trebuchet font-semibold text-[9px] text-white ">
                    Example
                  </h1>
                </div>
              </div>
              <AiOutlineHeart
                onClick={handleChange}
                className={`${
                  changecolor ? "text-[#E54934] " : "text-white"
                } text-[18px]  mr-2 cursor-pointer `}
              />
            </div>
            <div className="flex self-end items-center gap-1 p-2">
              <AiOutlineStar className=" text-white text-[18px] ml-2" />
              <h1 className="mt-1 font-trebuchet font-bold text-[16px] text-white  ">
                4.1
              </h1>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-col items-start px-4 pt-2 pb-6">
        <div className="flex items-end justify-start">
          <h1 className="font-trebuchet font-thin text-[19px] tracking-[0.7px]  ">
            Atelier Poterie
          </h1>
        </div>
        <div className="flex items-end gap-6">
          <div className="w-fit h-fit flex items-center justify-start gap-1">
            <AiOutlineCheckCircle className="mb-[0.2px] text-[#410053]" />
            <p className="text-sm font-trebuchet font-normal text-[#410053] ">
              Durée: 2h00
            </p>
          </div>
          <div className="w-fit h-fit flex items-center justify-start gap-1">
            <AiOutlineCheckCircle className="mb-[0.2px] text-[#410053]" />
            <p className="text-sm font-trebuchet font-normal text-[#410053] ">
              Personnes: 10
            </p>
          </div>
        </div>
        <div className="w-full h-fit flex items-end justify-start mt-4">
          <p className="font-trebuchet font-thin text-[12px] text-[#4d4d4d] ">
            Rabat
          </p>
        </div>
        <div className="w-full flex items-end justify-between">
          <h1 className="font-trebuchet font-bold text-[18px] ">140 DH</h1>
          <Link
            href="/article/1"
            className=" bg-components-bold flex items-center justify-center font-trebuchet font-bold px-6 text-xs rounded py-1 text-white "
          >
            Reserver
          </Link>
        </div>
      </div>
    </div>
  );
}
