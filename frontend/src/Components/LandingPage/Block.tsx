export const Block = () => {
  return (
    <div className="inline-block rounded-[7px] bgBlock ">
      <div className="w-full h-full flex py-10 px-5 flex-col items-start justify-end bgBlockOpa gap-4">
        <h1 className="font-gotham text-start w-full font-bold text-2xl text-white">
          Block Title
        </h1>
        <div className="w-full">
          <p className="font-gotham font-thin text-[12px] text-left text-[#F8F8F8]">
            Lorem ipsum dolor sit amet consectetur. Tellus in rhoncus adipiscing
            felis iaculis nisi leo tellus facilisi .....
          </p>
        </div>
      </div>
    </div>
  );
};
