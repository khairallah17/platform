import { MdForest } from 'react-icons/md';
import Link from 'next/link';
import Image from 'next/image';

export default function CategoriesSection() {
    const arr = [   {name: 'Artistique', icon: "/icons/artistic.png", link: "/search/artistique"},
                    {name: 'Manuel', icon: "/icons/manuel.png", link: "/search/manuel"},
                    {name: 'Culinaire', icon: "/icons/culinaire.png", link: "/search/culinaire"},
                    {name: 'Nature', icon: "/icons/nature.png", link: "/search/nature"},
                    {name: 'Multimedia', icon: "/icons/multimedia.png", link: "/search/multimedia"}];
  
    return (
        <div className='container mx-auto mt-10 mb-5 p-4'>
            <div className='flex items-end justify-start'>
                <h1 className=' font-trebuchet font-bold text-[28px] phone:text-[22px]'>Découvrez nos univers</h1>
            </div>
            <div className='w-[100%] pt-5 flex items-center justify-center'>
                <div className='flex items-center justify-center flex-wrap'>
                    {
                        arr.map(({name, icon, link}, index) => (
                            <Link href={link} key={index} className='phone:w-[100px] w-[150px] h-[90%] phone:h-[70%] flex flex-col items-center justify-around mr-4'>
                                <div className='w-[120px] h-[120px] bg-white rounded-full tablet:w-[80px] tablet:h-[80px] phone:w-[60px] phone:h-[60px]  shadow-lg flex items-center justify-center  hover:shadow-2xl transition-all ease-out duration-500'>
                                    {/* <MdForest className='text-[#9747FF] text-[50px] tablet:text-[32px] phone:text-[25px]' /> */}
                                    <Image
                                        src={icon}
                                        width={48}
                                        height={48}
                                        className=' phone:w-8 phone::w-8'
                                        alt={`icon ${name}`}
                                    />
                                </div>
                                <h1 className='font-trebuchet font-bold text-[22px] phone:text-[15px]'>
                                    {name}
                                </h1>
                            </Link>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}