import { Block } from "@/Components/LandingPage/Block";
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';

// import required modules
import { Pagination, FreeMode } from 'swiper/modules';

export const BlogArea = () => {
  return (
    <div className="w-full h-fit mt-12 container mx-auto flex flex-col items-center justify-start gap-2 my-20 phone:p-4">
      <h1 className="text-start w-full font-gotham font-bold text-[30px] text-[#000] laptop:text-[25px] phone:text-[20px]">
        Communauté, Créativité, Collaboration
      </h1>
      <p className="font-gotham font-light text-[20px] text-start w-full tablet:text-[18px] phone:text-[15px] text-[##000000A8]">
        Découvrez notre blog, nos astuces et inspirations ...
      </p>
      <Swiper
        slidesPerView={1}
        centeredSlides={true}
        spaceBetween={30}
        grabCursor={true}
        modules={[FreeMode]}
        className="mySwiper"
        loop={true}
        breakpoints={{
          640: {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 3,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 4,
            spaceBetween: 50,
          },
        }}
      >
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
        <SwiperSlide>
          <Block/>
        </SwiperSlide>
      </Swiper>
    </div>
  );
};
