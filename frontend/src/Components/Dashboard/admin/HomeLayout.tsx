import React from 'react'

const HomeLayout = () => {
    return (
        <div className="w-full h-full p-10">

            <div className="w-full h-fit">
                <h1 className="font-gotham font-bold text-[40px] uppercase text-[#9C9C9C]">
                    Acceuille
                </h1>
            </div>

        </div>
    )   
}

export default HomeLayout