import { Header } from "@/Components/Dashboard/Header";
import { BiHomeAlt2 } from "react-icons/bi";
import { FiUsers } from "react-icons/fi"
import { MdLogout } from "react-icons/md";
import { AiOutlineFileDone, AiOutlineSetting } from "react-icons/ai";
import { CiReceipt } from "react-icons/ci"
import { HomeSection } from "@/Components/Dashboard/HomeSection";
import Link from "next/link";
import GetContext from "@/context/UserContext";
import { useEffect } from "react";
import { useRouter } from "next/router";
import AuthWrapper from "@/Components/authentication/authWrapper";

function DashboardLayout({ children }: any) {

    const { logout, accessToken } = GetContext()

    // const router = useRouter()
    
    // useEffect(() => {
  
    //   if (accessToken !=" ADMIN")
    //     router.push("/")
  
    // }, [])

    return (

        <div className="w-full h-[100vh] ">
            <Header />
            <div className="w-full MainDash flex items-start justify-center">
                <div className="h-full flex flex-col justify-between w-[70px] bg-[#9747FF] pt-[30px]">

                    <div className="top">
                        <Link href="/admin/home"
                        className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
                        >
                            <BiHomeAlt2 className=" text-[30px]" />
                        </Link>

                        <Link href="/admin/users"
                        className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
                        >
                            <FiUsers className=" text-[30px]" />
                        </Link>

                        <Link href="/admin/announcements"
                        className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
                        >
                            <AiOutlineFileDone className=" text-[30px]" />
                        </Link>

                        <Link href="/admin/reservations"
                        className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
                        >
                            <CiReceipt className=" text-[30px]" />
                        </Link>


                        <Link href="/admin/settings"
                        className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
                        >
                            <AiOutlineSetting className=" text-[30px]" />
                        </Link>
                    </div>

                    <div className="bottom">
                        <button
                        onClick={logout}
                        className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
                        >
                            <MdLogout className=" text-[30px]" />
                        </button>
                    </div>
                    
                </div>
            {children ? children : <HomeSection />}
            </div>
        </div>

    );
}

export default AuthWrapper(DashboardLayout)
