"use client"

import React from 'react'
import { User } from '@/interaces'
import Image from 'next/image'
import Link from 'next/link'

const Row = (props: {users: User, classTheme: string}) => {
  
  return (
    <Link href={`/admin/user/${props.users.id}`} className={props.classTheme}>

      <div className="w-1/2 max-w-1/2 overflow-hidden">
        <Image src={props.users.profileImage ? props.users.profileImage : require("../../../../../public/user.png")} alt='profile Image' height={32} width={32} />
      </div>

      <div className='w-1/2 max-w-1/2 overflow-hidden'>
        <p className='truncate'>{props.users.name}</p>
      </div>

      <div className='w-1/2 max-w-1/2 overflow-hidden'>
        <p className='truncate'>{props.users.userPreferences.type.toLowerCase()}</p>
      </div>

      <div className='w-1/2 max-w-1/2 overflow-hidden'>
        <p className='truncate'>{props.users.email}</p>
      </div>

      <div className='w-1/2 max-w-1/2 overflow-hidden '>
        <p className='truncate bg-white text-purple-600 p-2 w-[150px] text-center'>{props.users.status ? "active" : "non active"}</p>
      </div>

    </Link>
  )
}

export default Row