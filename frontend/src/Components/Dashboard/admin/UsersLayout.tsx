import React, { ChangeEvent, useEffect } from 'react'
import Row from './users/row'
import { User, UserList } from '@/interaces'
import { useState } from 'react'
import { BsSearch } from "react-icons/bs"
import { useAxios } from '@/Hooks/useAxios'

const UsersLayout = ( { users }: UserList) => {

    interface activeFilterAction {
        value: string
    }

    const [searchValue, setSearchValue] = useState<string>("")
    const [activeFilter, setActiveFilter] = useState<string>("ALL")
    const [activityfilter, setActivityFilter] = useState<string>("ALL")
    const [usersData, setUsersdata] = useState<User[]>([])

    const updateActiveFilter = (action: activeFilterAction): void => {
        setActiveFilter(action.value)
        if(action.value === "ALL"){
            return setUsersdata(users)
        }
        return setUsersdata(users?.filter(ele => ele.userPreferences.type.toLowerCase().includes(action.value.toLowerCase())))
    }

    const updateActivityFilter = (action: activeFilterAction): void => {
    
        setActivityFilter(action.value)

        if (action.value == "ALL")
            return setUsersdata(users)
        else if (action.value == "ACTIVE")
            return setUsersdata(usersData?.filter(ele => ele.status == true))
        else if (action.value == "NON ACTIVE")
            return setUsersdata(usersData?.filter(ele => ele.status == false))

    }

    const filter = [{ value: "Toutes les utilisateur", filter: "ALL", number: users?.length }, 
                    { value: "createur", filter: "CREATEUR", number: users?.filter(ele => ele.userPreferences.type.toLowerCase().includes("createur")).length },
                    { value: "client", filter: "CLIENT", number: users?.filter(ele => ele.userPreferences.type.toLowerCase().includes("client")).length }]

    const activityFilter = [{ value: "Tous", filter: "ALL", number: usersData?.length }, 
                            { value: "active", filter: "ACTIVE", number: usersData?.filter(ele => ele.status == true).length },
                            { value: "non active", filter: "NON ACTIVE", number: usersData?.filter(ele => ele.status == false).length }]

    useEffect(() => {
        setUsersdata(users)
    },[users])

    const filterUsersData = (e: ChangeEvent<HTMLInputElement>) => {
        if(!e.target.value){
            return setUsersdata([...users])
        } else {
            setSearchValue(e.target.value)
            setUsersdata(users?.filter(ele => ele.name.toLowerCase().includes(searchValue)))
        } 
    }

    return (
    <div className="w-full h-full rounded-[10px] overflow-scroll flex items-center justify-start tablet:flex-col phone:flex-col shadow-lg ">
        <div className='h-full w-full p-10'>

            <div className="w-full h-fit">
                <h1 className="font-gotham font-bold text-[40px] uppercase text-[#9C9C9C]">
                    liste des utilisateurs
                </h1>
            </div>

            <div className="search-input flex items-center border w-fit relative mt-10 px-4">
                <BsSearch className="absolute left-4 text-gray-400" />
                <input type="text" onChange={filterUsersData} placeholder='saisissez le nom d&apos;utilisateur' className='pl-6 px-4 w-96 py-2 outline-none' />
            </div>

            <h4 className='mt-4 capitalize text-lg text-gray-400'>type d&apos;utilisateur</h4>

            <div className="filters flex w-full gap-2 mt-4">


            {
                filter.map(({value, filter, number}, key) => (
                    <button key={key} onClick={() => updateActiveFilter({value: filter})}  className={`filter-all duration-300 bg-dashboard-default ${activeFilter != filter && "bg-opacity-20"} flex w-fit items-center px-4 py-2 gap-2`}>
                        <span className={`uppercase font-normal ${activeFilter === filter && "!text-white"} text-dashboard-default`}>{ value }</span>
                        <div className="numbers-of-orders w-8 h-8 max-w-8 max-h-8 text-dashboard-default rounded-full flex items-center justify-center bg-white">
                            { number }
                        </div>
                    </button>
                ))
            }


            </div>

            <h4 className='mt-4 capitalize text-lg text-gray-400'>status du compte</h4>
            <div className="activity-filters flex w-full gap-2 mt-4">


                {
                    activityFilter.map(({value, filter, number}, key) => (
                        <button key={key} onClick={() => updateActivityFilter({value: filter})}  className={`filter-all duration-300 bg-dashboard-default ${activityfilter != filter && "bg-opacity-20"} flex w-fit items-center px-4 py-2 gap-2`}>
                            <span className={`uppercase font-normal ${activityfilter === filter && "!text-white"} text-dashboard-default`}>{ value }</span>
                            <div className="numbers-of-orders w-8 h-8 max-w-8 max-h-8 text-dashboard-default rounded-full flex items-center justify-center bg-white">
                                { number }
                            </div>
                        </button>
                    ))
                }

            </div>

            <div className="users flex flex-col gap-4 py-4 w-full">

                <div className="titles flex flex-1 p-4 gap-4 items-center border-violet-500 border  text-violet-500">

                    <div className='w-1/2 overflow-hidden truncate max-w-1/2'>
                        <p className='truncate'>profile</p>
                    </div>

                    <div className='w-1/2 overflow-hidden truncate max-w-1/2'>
                        <p className='truncate'>nom et prenom</p>
                    </div>

                    <div className='w-1/2 overflow-hidden truncate max-w-1/2'>
                        <p className='truncate'>type de profile</p>
                    </div>

                    <div className='w-1/2 overflow-hidden truncate max-w-1/2'>
                        <p className='truncate'>email</p>
                    </div>

                    <div className='w-1/2 overflow-hidden truncate max-w-1/2'>
                        <p className='truncate'>status</p>
                    </div>

                </div>

                {
                    usersData?.map((ele: User, key: number) => (
                        <Row classTheme="flex flex-1 p-4 gap-4 items-center bg-violet-500 text-white" key={key} users={ele} />
                    ))
                }

            </div>

        </div>
    </div>
  )
}

export default UsersLayout