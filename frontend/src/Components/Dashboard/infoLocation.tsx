import { FiMap } from "react-icons/fi";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import { ChangeEvent, useState } from "react";
import AnnoucementsHook from "@/Hooks/AnnoucementsHook";
import { Select, MenuItem, FormControl, InputLabel, SelectChangeEvent } from "@mui/material";

const CitiesData = [
  "Casablanca",
  "Rabat",
  "Marrakech",
  "Fes",
  "Tangier",
  "Agadir",
  "Meknes",
  "Oujda",
  "Kenitra",
  "Tetouan",
  "Essaouira",
  "Nador",
  "El Jadida",
  "Chefchaouen",
  "Fez",
  "Ouarzazate",
  "Taroudant",
  "Erfoud",
  "Taza",
  "Beni Mellal",
  "Azrou",
  "Ifrane",
  "Zagora",
];

export const InfoLocation = () => {
  
  const { workshopCity, setWorkshopCity,
          workshopAddress, setWorkshopAddress } = AnnoucementsHook()

  return (
    <div className="w-full h-fit flex items-start justify-start">
      <div className="w-fit h-full flex flex-col items-center justify-start pr-6">
        <FiMap className="text-[60px] text-[#9747FF]" />
      </div>
      <div className="w-full h-fit flex flex-col items-start justify-start gap-3 pt-4">
        <h1 className=" font-trebuchet font-bold text-[30px]">Localisation</h1>
        <div className="w-full ">
          <p className="font-trebuchet font-normal text-[16px] text-[#B8B8B8] pb-2">
            Aidez les gens de la région à découvrir votre atelier et faites
            savoir aux participants où se présenter.
          </p>
        </div>
        <div className="w-full h-fit flex items-center justify-start gap-2 ">
          <FormControl fullWidth>
            <InputLabel>ville</InputLabel>
            <Select
              label="ville"
              onChange={(e: SelectChangeEvent) => setWorkshopCity(e.target.value as string)}
            >
              {
                CitiesData.map((city, key) => (
                  <MenuItem value={city} key={key}>
                    {city}
                  </MenuItem>
                ))
              }
            </Select>
          </FormControl>
          <TextField
            className="w-full"
            label="Addresse"
            inputProps={{ maxLength: 50 }}
            onChange={(e: ChangeEvent<HTMLInputElement>) => setWorkshopAddress(e.target.value)}
            value={workshopAddress}
          />
        </div>
      </div>
    </div>
  );
};
