import { BsFillCaretDownFill } from "react-icons/bs";


export const Header = () => {
    return (
        <div className="w-full h-[75px] bg-[#000] flex items-center justify-between px-3">
            <h1 className="font-bold text-[26px] text-white">LOGO</h1>
            <div className="w-[60px] h-[37px] bg-white rounded-l-full rounded-r-full flex items-center justify-between pr-1 cursor-pointer">
                <div className="w-[37px] h-[37px] rounded-full bg-[#9747FF] flex items-center justify-center">
                <h1 className="font-bold text-[14px] text-white">MK</h1>
                </div>
                <BsFillCaretDownFill className="text-[#9747FF] text-[15px]"/>
            </div>
        </div>
    )
};