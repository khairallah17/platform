import { IoImageOutline } from "react-icons/io5";
import TextField from "@mui/material/TextField";
import { AiOutlineClose } from "react-icons/ai";
import { useState } from "react";
export const Details = () => {
  const [value, setValue] = useState("");
  const [data, setData] = useState<string[]>([]);
  return (
    <div className="w-full h-fit flex items-start justify-start">
      <div className="w-fit h-full flex flex-col items-center justify-start pr-6">
        <IoImageOutline className="text-[55px] text-[#fff]" />
      </div>
      <div className="w-full h-fit flex flex-col items-start justify-start gap-3">
        <h1 className=" font-trebuchet font-bold text-[27px]">
          Detail Atelier
        </h1>
        <div className="w-full ">
          <p className="font-trebuchet font-normal text-[14px] text-[#B8B8B8] pb-2">
            Ajoutez plus de détails à votre atelier, nombres de place, ou tout
            faciliter votre atelier propose.
          </p>
        </div>
        <TextField
          label="Nombre de places"
          size="medium"
          sx={{
            width: "100%",
            "& .MuiOutlinedInput-root": {
              "& fieldset": {
                borderColor: "#B8B8B8",
              },
              "&:hover fieldset": {
                borderColor: "#B8B8B8",
              },
              "&.Mui-focused fieldset": {
                borderColor: "#B8B8B8",
              },
              "& input": {
                color: "#000",
                fontWeight: "500",
                fontSize: "22px",
              },
            },
          }}
          type="number"
          inputProps={{ min: "1", max: "10", step: "1" }}
        />
        <TextField
          sx={{ width: "100%" }}
          label="Plus de details"
          value={value}
          onChange={(e: any) => setValue(e.target.value)}
          onKeyPress={(e: any) => {
            if (e.key === "Enter") {
              setData([...data, value]);
              setValue("");
            }
          }}
        />
        <div className="w-full h-fit flex items-center justify-start gap-2">
          {data.length > 0 &&
            data.map((item, index) => (
              <div key={index} className="w-fit h-fit px-3 p-1 bg-slate-200 rounded-2xl flex items-center justify-center">
                <h1 className="font-trebuchet font-normal text-[15px]">{item}</h1>
                <AiOutlineClose className="text-[#000] text-[15px] ml-2 cursor-pointer" onClick={() => {
                  const newData = data.filter((item, i) => i !== index);
                  setData(newData);
                  }} />
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};
