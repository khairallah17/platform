import { ChangeEvent, useState } from "react";
import { RiFileTextLine } from "react-icons/ri";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import AnnoucementsHook from "@/Hooks/AnnoucementsHook";
import { Select, MenuItem, SelectChangeEvent, FormControl, InputLabel } from "@mui/material";

const CategoriesData = ["Art", "Design", "Photography", "Music", "Film"];
const universData = ["univers1", "univers2", "univers3", "univers4"]

export const InfoArticle = () => {
  
  const { workshopTitle, setWorkShopTitle,
          workshopCategorie, setWorkshopCategorie,
          workshopUnivers, setWorkshopUnivers } = AnnoucementsHook()

  return (
    <div className="w-full h-fit flex items-start justify-start">
      <div className="w-fit h-full flex flex-col items-center justify-start pr-6">
        <RiFileTextLine className="text-[60px] text-[#9747FF]" />
      </div>
      <div className="w-full h-fit flex flex-col items-start justify-start gap-3">
        <h1 className=" font-trebuchet font-bold text-[30px]">
          Information de Base
        </h1>
        <div className="w-full ">
          <p className="font-trebuchet font-normal text-[16px] text-[#B8B8B8] pb-2">
            Nommez votre Atelier et dites aux participants pourquoi ils
            devraient venir. Ajoutez des détails qui mettent en évidence ce qui
            le rend unique.
          </p>
        </div>
        <TextField
          sx={{ width: "100%" }}
          label="Titre de l'Atelier"
          helperText={`${workshopTitle.length}/50`}
          inputProps={{ maxLength: 50 }}
          onChange={(e: ChangeEvent<HTMLInputElement>) => setWorkShopTitle(e.target.value)}
          value={workshopTitle}
        />
        <div className="w-full flex gap-4">
          <FormControl fullWidth>
            <InputLabel>Catégorie</InputLabel>
            <Select
              label="categorie"
              className="outline-none w-full"
              onChange={(e: SelectChangeEvent) => setWorkshopCategorie(e.target.value as string)}
              value={workshopCategorie}
            >
              {
                CategoriesData.map((item, index) => (
                  <MenuItem className="outline-none" key={index} value={item}>{item}</MenuItem>
                ))
              }
            </Select>
          </FormControl>
          <FormControl fullWidth>
            <InputLabel>Univers</InputLabel>
            <Select
              label="univers"
              className="outline-none w-full"
              onChange={(e: SelectChangeEvent) => setWorkshopUnivers(e.target.value as string)}
              value={workshopUnivers}
            >
              {
                universData.map((item, index) => (
                  <MenuItem className="outline-none" key={index} value={item}>{item}</MenuItem>
                ))
              }
            </Select>
          </FormControl>
        </div>
      </div>
    </div>
  );
};
