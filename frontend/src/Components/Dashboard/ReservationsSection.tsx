import { ChangeEvent, useState, useEffect } from 'react'
import { useRouter } from 'next/router'
import { DataGrid, GridColDef, GridValueGetterParams, GridFilterModel } from '@mui/x-data-grid';
import { BsSearch } from "react-icons/bs"

const ReservationsSection = () => {

    interface activeFilterAction {
        value: string
    }

    interface rowsData {
        id: number,
        clientName: string,
        phoneNumber: string,
        workshopTitle: string,
        seats: string
    }

    const [searchValue, setSearchValue] = useState<string>("")
    const [activeFilter, setActiveFilter] = useState<string>("ALL")
    const [data, setData] = useState<Array<rowsData>>([])

    const updateActiveFilter = (action: activeFilterAction): void => setActiveFilter(action.value)

    const filter = [{ value: "Toutes les réservations", filter: "ALL", number: "90" }, 
                    { value: "expiree", filter: "EXPIRED", number: "88" },
                    { value: "nouveaux", filter: "FUTURE", number: "5" },
                    { value: "annuler", filter: "CANCELED", number: "11"}]

                    
    const columns: GridColDef[] = [
        { field: 'id', headerName: 'ID', width: 90 },
        {
            field: 'clientName',
            headerName: 'Nom Client',
            width: 250,
            editable: true,
        },
        {
            field: 'workshopTitle',
            headerName: 'Titre Atelier',
            width: 250,
            filterable: true,
            editable: true,
        },
        {
            field: 'phoneNumber',
            headerName: 'Numero Du Telephone',
            width: 200,
            editable: true,
        },
        {
            field: 'seats',
            headerName: 'Nombre De Reservations',
            description: 'This column has a value getter and is not sortable.',
            sortable: false,
            width: 200,
        },
    ];
        
    const rows: Array<rowsData> = [
        { id: 1, clientName: 'David E. Easterling', workshopTitle: 'Atelier Poterie', phoneNumber: "+212 555-9999", seats: "privatiser" },
        { id: 2, clientName: 'Janis J. Ortiz', workshopTitle: 'Atelier ceramique', phoneNumber: "+212 555-9999", seats: "10" },
        { id: 3, clientName: 'Lillian T. Hill', workshopTitle: 'Atelier painture', phoneNumber: "+212 555-9999", seats: "1" },
        { id: 4, clientName: 'John T. King', workshopTitle: 'Atelier culineral', phoneNumber: "+212 555-9999", seats: "5" },
        { id: 5, clientName: 'Robert H. Cunningham', workshopTitle: 'Atelier Poterie', phoneNumber: "+212 555-9999", seats: "6" },
        { id: 6, clientName: 'Gordon V. Canales', workshopTitle: 'Atelier Poterie', phoneNumber: "+212 555-9999", seats: "77" },
        { id: 7, clientName: 'Wendell M. Slater', workshopTitle: 'Atelier Poterie', phoneNumber: "+212 555-9999", seats: "1" },
        { id: 8, clientName: 'Mark T. Bradley', workshopTitle: 'Atelier Poterie', phoneNumber: "+212 555-9999", seats: "5" },
        { id: 9, clientName: 'Alan P. Cody', workshopTitle: 'Atelier Poterie', phoneNumber: "+212 555-9999", seats: "2" },
    ];
    
    const filterRows = (e: ChangeEvent<HTMLInputElement>) => {
        setSearchValue(e.target.value)
        setData(rows.filter(dt => dt.workshopTitle.toLowerCase().includes(searchValue)))
    }

    useEffect(() => {
        setData(rows)
    },[])

    const router = useRouter()

    return (
        <div className="Main2Dash bg-white h-full pt-20 px-24  overflow-hidden overflow-y-scroll">

            <div className="w-full h-fit">
                <h1 className="font-gotham font-bold text-[40px] uppercase text-[#9C9C9C]">
                    Réservations
                </h1>
            </div>

            <div className="search-input flex items-center border w-fit relative mt-10">
                <BsSearch className="absolute left-1 text-gray-400" />
                <input type="text" placeholder='Titre atelier' onChange={filterRows} className='pl-6 px-4 py-2 outline-none' />
            </div>

            <div className="filters flex w-full gap-2 mt-4">

            {
                filter.map(({value, filter, number}, key) => (
                    <button key={key} onClick={() => updateActiveFilter({value: filter})}  className={`filter-all duration-300 bg-dashboard-default ${activeFilter != filter && "bg-opacity-20"} flex w-fit items-center px-4 py-2 gap-2`}>
                        <span className={`uppercase font-normal ${activeFilter === filter && "!text-white"} text-dashboard-default`}>{ value }</span>
                        <div className="numbers-of-orders w-8 h-8 max-w-8 max-h-8 text-dashboard-default rounded-full flex items-center justify-center bg-white">
                            { number }
                        </div>
                    </button>
                ))
            }

            </div>

            <div className="orders mt-5">
                <DataGrid
                    rows={data}
                    columns={columns}
                    initialState={{
                    pagination: {
                        paginationModel: {
                        pageSize: 5,
                        },
                    },
                    }}
                    pageSizeOptions={[5]}
                    checkboxSelection
                    disableRowSelectionOnClick
                />
            </div>

        </div>
    )
}

export default ReservationsSection