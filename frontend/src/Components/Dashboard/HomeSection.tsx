import Image from "next/image";
import { useEffect } from "react";

export const HomeSection = () => {


  useEffect(() => {

  }, [])

  const workshops = [{ date: "nov 16", image: "ArticlesImg.png", title: "titre atelier", details: "date, heure" },
                      { date: "nov 16", image: "ArticlesImg.png", title: "titre atelier", details: "date, heure" },
                      { date: "nov 16", image: "ArticlesImg.png", title: "titre atelier", details: "date, heure" },
                      { date: "nov 16", image: "ArticlesImg.png", title: "titre atelier", details: "date, heure" },
                      { date: "nov 16", image: "ArticlesImg.png", title: "titre atelier", details: "date, heure" }]
  
  const orders = [{id: "123", client: "Nom Client", image: "ArticlesImg.png", title: "titre Atelier", phone: "+2125533662288", number: "privatiser" },
                  {id: "123", client: "Nom Client", image: "ArticlesImg.png", title: "titre Atelier", phone: "+2125533662288", number: "5" },
                  {id: "123", client: "Nom Client", image: "ArticlesImg.png", title: "titre Atelier", phone: "+2125533662288", number: "77" },
                  {id: "123", client: "Nom Client", image: "ArticlesImg.png", title: "titre Atelier", phone: "+2125533662288", number: "1" },
                  {id: "123", client: "Nom Client", image: "ArticlesImg.png", title: "titre Atelier", phone: "+2125533662288", number: "9" }]

  return (
    <div className="Main2Dash bg-white h-full pt-20 px-24 overflow-scroll">
      <div className="w-full h-fit">
        <h1 className="font-gotham font-bold uppercase text-[40px] text-[#9C9C9C] mb-5">
          Bonjour Mohammed khairallah
        </h1>
        <div className="workshops">
          <h1 className="mb-5 text-xl font-bold">Vos ateliers</h1>
          <div className="workshops-container w-full flex flex-col gap-3 ">
            {
              workshops.map(({date, image, title, details}, key) => (
                <div key={key} className="workshop flex justify-between w-full">
                  <div className="workshop-details flex items-center gap-4 bg-gray-200 p-3 w-full">
                    <span className="text-center uppercase font-semibold">{date.split(" ")[0]} <br /> {date.split(" ")[1]}</span>
                    <div className="workshop-details-image-text flex gap-4 items-center w-full">
                      <Image src={`/${image}`} width={100} height={50} alt="workshop image"/>
                      <div className="wordshop-details-text flex flex-col">
                        <span className="capitalize">{title}</span>
                        <span className="text-gray-500 capitalize">{details}</span>
                      </div>
                    </div>
                    <button className="bg-[#9747FF] text-white px-6 py-2">Modifier</button>
                  </div>
                </div>
              ))
            }
          </div>
        </div>
        <div className="orders mt-10 mb-10">
          <h1 className="mb-5 text-xl font-bold">Dernières réservations</h1>
          <div className="workshops-container w-full flex flex-col  gap-3">
            {
              orders.map(({client, image, title, phone, number, id}, key) => (
                <div key={key} className="bg-gray-200 w-full flex justify-between items-center px-3 py-4">
                  <span className="capitalize">{client}</span>
                  <div className="workshop-details flex gap-3 items-center">
                    <Image src={`/${image}`} width={100} height={50} alt="workshop image" />
                    <span className="capitalize">{title}</span>
                  </div>
                  <span>{phone}</span>
                  <span className=" w-20 max-w-20">{number}</span>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    </div>
  );
};
