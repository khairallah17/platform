import { ChangeEvent, useState } from 'react'
import { TextField, Button, Autocomplete, SelectChangeEvent } from '@mui/material'
import { AiOutlineCloudUpload } from "react-icons/ai"

const SettingsComponent = () => {

    const [category, setCategory] = useState<string>("masson")

    const updateCategory = (e: SelectChangeEvent<string>): void => setCategory(e.target.value)

    const categories = ["Artistique","Manuel","Culinaire","Nature","MultiMedia"]

    return (
        <div className="Main2Dash bg-white h-full pt-20 px-24  overflow-hidden overflow-y-scroll relative">
            <h1 className="font-gotham uppercase font-bold text-[40px] text-[#9C9C9C]">
                Paramètres
            </h1>

            <div className="info-personnel mt-6">

                <h1 className='text-xl font-bold'>Information Personnel</h1>
                
                <div className="informations flex w-full justify-between gap-6 py-4">

                    <div className="image-upload w-fit">

                        <div className="image border border-[#9747FF] h-56 w-56 rounded-full flex items-center justify-center">
                            <Button
                                // variant="contained"
                                component="label"
                                className="h-full w-full flex rounded-full items-center justify-center gap-2 hover:bg-dashboard-default hover:bg-opacity-20"
                            >
                                <input hidden accept="image/*" multiple type="file" />
                                <AiOutlineCloudUpload size={46} color={"#9747FF"} />
                            </Button>
                        </div>

                    </div>

                    <div className='w-full flex flex-col gap-4 '>
                        <div className="fullanme flex w-full gap-4">
                            <TextField className='w-full rounded-none' id="outlined-basic" label="Nom" variant="outlined" />
                            <TextField className='w-full' id="outlined-basic" label="Prenom" variant="outlined" />
                        </div>

                        <div className="additional-info flex w-full gap-4">
                            <TextField className='w-full' id="outlined-basic" label="email" variant="outlined" />
                            <TextField className='w-full' id="outlined-basic" label="Numero du telephone" variant="outlined" />
                        </div>
                    </div>

                </div>


            </div>

            <div className="details-du-metier w-full flex flex-col gap-4 mt-6">
                <h1 className='text-xl font-bold'>Information savoir faire</h1>

                <Autocomplete
                    disablePortal
                    id="combo-box-demo"
                    options={categories}
                    renderInput={(params) => <TextField {...params} label="categorie savoir faire" />}
                />

            </div>

            <div className="save-changes fixed bottom-0 left-0 w-full flex px-24 py-5 justify-end ">
                <button className=' text-white duration-300 bg-dashboard-default border border-dashboard-default rounded-none px-4 py-3 hover:bg-opacity-20 hover:text-dashboard-default hover:border-dashboard-default hover:border'>
                    Mettre à jour
                </button>
            </div>

        </div>
    )   
}

export default SettingsComponent