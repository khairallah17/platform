import { useEffect, useReducer, useState } from "react"
import { BsCalendar2Date } from "react-icons/bs";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import dayjs from "dayjs";
import AnnoucementsHook from "@/Hooks/AnnoucementsHook";

export const TimeInfo = () => {

  const { workshopDateAndTime, setWorkshopDateAndTime,
          startTime, setStartTime,
          endTime, setEndTime,
          eventDate, setEventDate } = AnnoucementsHook()

  const numberOfDatesInitState = [1]

  const numberReducer = (state: typeof numberOfDatesInitState, action: ReducerActionNumber): number[] => {

      
      switch(action.type) {
          case REDUCER_ACTION_TYPES_NUMBER.INCREMENT:
              state.length == 3 ? state : state.push(1); break;
          case REDUCER_ACTION_TYPES_NUMBER.DECREMENT:
              state.length == 1 ? state : state.pop(); break;
      }

      console.log(state)

      return state;

  }

  const [numberOfDates, dispatchNumberOfDates] = useReducer(numberReducer, numberOfDatesInitState)

  const [reccuringEvent, setReccuringEvent] = useState<boolean>(false)


  const datesDispatcher = (value: string, action: REDUCER_ACTION_TYPES_NUMBER) => {

    console.log(value)

    dispatchNumberOfDates({type: action})

  }

  const enum REDUCER_ACTION_TYPES_DATE_TIME {
    START_TIME,
    END_TIME,
    DATE
  }

  const enum REDUCER_ACTION_TYPES_NUMBER {
      INCREMENT,
      DECREMENT
  }

  type ReducerActionDateTime = {
      type: REDUCER_ACTION_TYPES_DATE_TIME,
      value:  Date
  }

  type ReducerActionNumber = {
      type: REDUCER_ACTION_TYPES_NUMBER,
  }


  return (
    <div className="w-full h-fit flex items-start justify-start">
      <div className="w-fit h-full flex flex-col items-center justify-start pr-6">
        <BsCalendar2Date className="text-[55px] text-[#9747FF]" />
      </div>
      <div className="w-full h-fit flex flex-col items-start justify-start gap-3">
        <h1 className=" font-trebuchet font-bold text-[27px]">Date et heure</h1>
        <div className="w-full ">
          <p className="font-trebuchet font-normal text-[14px] text-[#B8B8B8] pb-2">
            Dites aux participants quand votre atelier commence et se termine
            afin qu&apos;ils puissent faire des plans pour y assister.
          </p>
        </div>
        <div className="w-full h-fit flex items-center justify-start gap-2">
          <button onClick={() => setReccuringEvent(false)} className={`px-7 duration-300 py-4 font-trebuchet font-regular text-[18px] ${!reccuringEvent ? "text-[#9747FF]  bg-[#9747FF0F] border-[#9747FF]" : "text-[#B8B8B8]   border-[#B8B8B8]"} border-[1.8px]`}>
            Atelier unique
          </button>
          <button onClick={() => setReccuringEvent(true)} className={`px-7 duration-300 py-4 font-trebuchet font-regular text-[18px] ${reccuringEvent ? "text-[#9747FF]  bg-[#9747FF0F] border-[#9747FF]" : "text-[#B8B8B8]   border-[#B8B8B8]"} border-[1.8px]`}>
            Atelier récurrent
          </button>
        </div>

        {
          numberOfDates.map((ats:number, index: number) => (
            <div key={index} className="w-full flex flex-col gap-6 mt-4">
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <DatePicker
                  views={["day", "month"]}
                  sx={{
                    width: "100%",
                  }}
                  label="Date de l'Atelier"
                  onChange={setEventDate}
                />
              </LocalizationProvider>
              <div className="w-full h-fit flex items-center justify-between">
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <TimePicker
                    views={["hours", "minutes"]}
                    ampm={false}
                    sx={{
                      width: "49%",
                    }}
                    label="Debut de l'Atelier"
                    onChange={setStartTime}
                  />
                </LocalizationProvider>
                <LocalizationProvider dateAdapter={AdapterDayjs}>
                  <TimePicker
                    views={["hours", "minutes"]}
                    ampm={false}
                    sx={{
                      width: "49%",
                    }}
                    label="Fin de l'Atelier"
                    onChange={setEndTime}
                  />
                </LocalizationProvider>
              </div>
            </div>
          ))
        }
        
        {
          numberOfDates.length > 1 ? 
          <div className="flex gap-4 w-full">
            <button onClick={!reccuringEvent ? () => {} : () => datesDispatcher("DECREMENTING", REDUCER_ACTION_TYPES_NUMBER.DECREMENT)} className="w-full py-3 text-red-400 border-[1.8px] border-red-400">
              Supprimer une date
            </button>
            <button onClick={!reccuringEvent ? () => {} : () => datesDispatcher("INCREMENTING", REDUCER_ACTION_TYPES_NUMBER.INCREMENT)} className="w-full py-3 text-[#9747FF] border-[1.8px] border-[#9747FF]">
              Ajouter une Date
            </button>
          </div> :
          <button onClick={() => datesDispatcher("INCREMENTING", REDUCER_ACTION_TYPES_NUMBER.INCREMENT)} className="w-full py-3 text-[#9747FF] border-[1.8px] border-[#9747FF]">
            Ajouter une Date
          </button>
        }
      </div>
    </div>
  );
};
