import { useState } from "react";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import { RiFileTextLine } from "react-icons/ri";
import dynamic from "next/dynamic";

const QuillNoSSRWrapper = dynamic(import('react-quill'), {	
	ssr: false,
	loading: () => <p>Loading ...</p>,
	})


export const DescriptionFill = () => {
  const [value, setValue] = useState("");
  return (
    <div className="w-full h-fit flex items-start justify-start pb-11">
      <div className="w-fit h-full flex flex-col items-center justify-start pr-6">
        <RiFileTextLine className="text-[55px] text-[#9747FF]" />
      </div>
      <div className="w-full h-fit flex flex-col items-start justify-start gap-3">
        <h1 className=" font-trebuchet font-bold text-[27px]">Description</h1>
        <div className="w-full ">
          <p className="font-trebuchet font-normal text-[14px] text-[#B8B8B8] pb-2">
            Ajoutez descriptif à votre atelier, comme votre emploi du temps, vos
            sponsors ou vos invités en vedette.
          </p>
        </div>
        <QuillNoSSRWrapper
          className="w-full h-[150px]"
          placeholder="Ecrie une description sur votre atelier"
          theme="snow"
          value={value}
          onChange={setValue}
        />
      </div>
    </div>
  );
};
