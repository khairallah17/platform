import { useState } from "react";
import { InfoArticle } from "./InfoArticle";
import { InfoLocation } from "./infoLocation";
import { UploadInfo } from "./UploadInfo";
import { TimeInfo } from "./TimeInfo";
import { DescriptionFill } from "./DescriptionFill";
import { Details } from "./Details";
import Swal from "sweetalert2";
import AnnoucementsHook from "@/Hooks/AnnoucementsHook";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const CreateSection = () => {
  
  const [showNext, setShowNext] = useState(false);
  const [errorState ,setErrorState] = useState<boolean>(false)
  const [errorMsg, setErrorMsg] = useState<string>("")

  const notificationError = (message: string) => {
    toast.error(message, {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      theme: "light",
    });
  }

  const { workshopTitle,
          workshopCategorie,
          workshopUnivers,
          workshopCity,
          workshopAddress,
          workshopDateAndTime } = AnnoucementsHook()

  const nextPage = () => {

    if(!showNext) {
      if (!workshopTitle)
        return notificationError("vieullez remplir le titre de l'atelier de votre atelier")
      else if (!workshopCategorie)
        return notificationError("veuillez choisir la categorie de votre atelier")
      else if (!workshopUnivers)
        return notificationError("veuillez choisir l'univers de votre atelier")
      else if (!workshopCity)
        return notificationError("veuillez choisir la ville de votre atelier")
      else if (!workshopAddress)
        return notificationError("veuillez saisir l'address de votre atelier")
      else if (workshopDateAndTime.length == 0)
        return notificationError("veuillez chosir la date, l'heure de debut et de fin")
    }

  }

  const showSuccess = () => {
    if (showNext) {
      Swal.fire({
        title: "Annonce est encours de traitement",
        showClass: {
          popup: "animate__animated animate__fadeInDown",
        },
        hideClass: {
          popup: "animate__animated animate__fadeOutUp",
        },
        color: "#000",
        icon: "success",
        iconColor: "#9747FF",
      });
    }
  };

  return (
    <div className="Main2Dash relative bg-white h-full pt-20 px-24  ">
      <ToastContainer className=" mt-20" />
      {!showNext && (
        <div className="w-full h-[85%] flex flex-col items-start justify-start gap-6 overflow-hidden overflow-y-scroll">
          <InfoArticle />
          <InfoLocation />
          <TimeInfo />
        </div>
      )}
      {showNext && (
        <div className="w-full h-[85%] flex flex-col items-start justify-start gap-6 overflow-hidden overflow-y-scroll">
          <UploadInfo />
          <DescriptionFill />
          <Details />
        </div>
      )}
      <div
        className="w-full h-[100px] absolute bottom-0 left-0 z-99 px-24 flex items-center"
        style={{
          justifyContent: showNext ? "space-between" : "flex-end",
        }}
      >
        <div
          style={{
            display: !showNext ? "none" : "flex",
          }}
          onClick={() => setShowNext(false)}
          className="w-[180px] h-[52px] bg-[#9747FF] rounded-[2px] flex items-center justify-evenly cursor-pointer"
        >
          <h1 className="font-gotham font-normal text-white text-[18px]">
            Précédent
          </h1>
        </div>
        <div
          onClick={nextPage}
          className="w-[180px] h-[52px] bg-[#9747FF] rounded-[2px] flex items-center justify-evenly cursor-pointer"
        >
          <h1 className="font-gotham font-normal text-white text-[18px]">
            {showNext ? "Terminer" : "Continuer"}
          </h1>
        </div>
      </div>
    </div>
  );
};
