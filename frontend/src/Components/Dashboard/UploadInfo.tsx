import { IoImageOutline } from "react-icons/io5";
import { Button } from "@mui/material";
export const UploadInfo = () => {
  return (
    <div className="w-full h-fit flex items-start justify-start">
      <div className="w-fit h-full flex flex-col items-center justify-start pr-6">
        <IoImageOutline className="text-[55px] text-[#9747FF]" />
      </div>
      <div className="w-full h-fit flex flex-col items-start justify-start gap-3">
        <h1 className=" font-trebuchet font-bold text-[27px]">Media Atelier</h1>
        <div className="w-full ">
          <p className="font-trebuchet font-normal text-[14px] text-[#B8B8B8] pb-2">
            Ajoutez des photos pour montrer le sujet de votre événement. Vous
            pouvez télécharger jusqu&apos;à 10 images.
          </p>
        </div>
        <div className="w-full h-[350px] flex items-center justify-center border-[2px] border-[#B8B8B8]">
          <Button
            // variant="contained"
            component="label"
            sx={{
              border: "2px solid #B8B8B8",
            }}
            className="px-4 py-2 border-[2px] border-[#B8B8B8] flex items-center justify-center gap-2 font-trebuchet font-light text-[16px] bg-white text-[#B8B8B8]"
          >
            <input hidden accept="image/*" multiple type="file" />
            <IoImageOutline className="text-[18px] text-[#B8B8B8]" />
            Ajouter une image
          </Button>
        </div>
      </div>
    </div>
  );
};
