import Swal from "sweetalert2";

export const FillAnnounce = () => {
  const showSuccess = () => {
    Swal.fire({
      title: "Annonce créée avec succès",
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
      color: "#000",
      icon: "success",
      iconColor: "#9747FF",
    });
  };
  return (
    <div className="Main2Dash bg-white h-full pt-20 px-24 ">
      <div className="w-full h-fit">
        <h1 className="font-gotham font-bold text-[40px] text-[#9C9C9C]">
          CREER ANNONCES
        </h1>
      </div>
      <div className="w-full h-[70px]  mt-10 flex items-center justify-between">
        <div className="w-[450px]  h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden relative flex items-center">
          <input
            type="text"
            className="w-full h-full outline-none font-gotham font-normal tracking-[1px] text-black pl-[12px]"
            placeholder="Nombre de places"
          />
        </div>
        <div className="w-[450px]  h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden relative flex items-center">
          <input
            type="text"
            className="w-full h-full outline-none font-gotham font-normal tracking-[1px] text-black pl-[12px]"
            placeholder="Durée"
          />
        </div>
      </div>
      <div className="w-full h-[100px]  mt-2 flex items-center justify-between">
        <div className="w-[450px]  h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden relative flex items-center">
          <input
            type="text"
            className="w-full h-full outline-none font-gotham font-normal tracking-[1px] text-black pl-[12px]"
            placeholder="Adresse"
          />
        </div>
        <div className="w-[450px]  h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden relative flex items-center pl-2">
          <select
            className="custom-select w-[100%] h-[100%] outline-none rounded-[5px] font-trebuchet font-medium "
            placeholder="Categories"
          >
            <option value="volvo">Ville</option>
            <option value="saab">Rabat</option>
            <option value="mercedes">Casablanca</option>
            <option value="audi">Tangier</option>
          </select>
        </div>
      </div>
      <div className="w-full h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden relative flex items-center">
        <input
          type="text"
          className="w-full h-full outline-none font-gotham font-normal tracking-[1px] text-black pl-[12px]"
          placeholder="Details"
        />
      </div>
      <div className="w-full h-[52px] flex items-center justify-start gap-2 mt-4">
        <div className="h-[75%] w-[15%] border border-black rounded-l-full rounded-r-full flex items-center justify-center">
          <h1 className="text-[18px] font-extralight">
            Facilite d&apos;access
          </h1>
        </div>
        <div className="h-[75%] w-[15%] border border-black rounded-l-full rounded-r-full flex items-center justify-center">
          <h1 className="text-[18px] font-extralight">
            Facilite d&apos;access
          </h1>
        </div>
        <div className="h-[75%] w-[15%] border border-black rounded-l-full rounded-r-full flex items-center justify-center">
          <h1 className="text-[18px] font-extralight">
            Facilite d&apos;access
          </h1>
        </div>
        <div className="h-[75%] w-[15%] border border-black rounded-l-full rounded-r-full flex items-center justify-center">
          <h1 className="text-[18px] font-extralight">
            Facilite d&apos;access
          </h1>
        </div>
      </div>
      <div className="w-full h-fit flex items-center justify-end mt-2">
        <button
          onClick={showSuccess}
          className="w-[180px] h-[52px] bg-[#9747FF] rounded-[8px] flex items-center justify-evenly cursor-pointer"
        >
          <h1 className="font-gotham font-normal text-white text-[18px]">
            Terminer
          </h1>
        </button>
      </div>
    </div>
  );
};
