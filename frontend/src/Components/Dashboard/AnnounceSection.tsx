import { BiSearch, BiPlus } from "react-icons/bi";
import { useState } from "react";
import "animate.css";
import { useRouter } from "../../../node_modules/next/router";
import { AnnounceCard } from "./AnnounceCard";

export const AnnounceSection = () => {
  const router = useRouter();
  const [showcreate, setShowCreate] = useState<boolean>(false);

  const handleShowCreate = () => {
    router.push("/announce/Create");
  };
  return (
    <div className="Main2Dash bg-white h-full pt-20 px-24  overflow-hidden overflow-y-scroll">
      <div className="w-full h-fit">
        <h1 className="font-gotham font-bold text-[40px] text-[#9C9C9C]">
          ANNONCES
        </h1>
      </div>
      <div className="w-full h-[100px]  mt-10 flex items-center justify-between">
        <div className="w-[350px]  h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden relative flex items-center">
          <BiSearch className="text-[#ADADAD] text-[20px] absolute left-[15px] " />
          <input
            type="text"
            className="w-full h-full outline-none font-gotham font-normal tracking-[1px] text-black pl-[48px]"
            placeholder="rechercher par titre annonce"
          />
        </div>
        <div
          onClick={handleShowCreate}
          className="w-[180px] h-[52px] bg-[#9747FF] rounded-[8px] flex items-center justify-evenly cursor-pointer overflow-hidden"
        >
          <BiPlus className="text-white text-[20px]" />
          <h1 className="font-gotham font-normal text-white text-[18px]">
            Cree Annonce
          </h1>
        </div>
      </div>
      <div className="w-full h-fit flex flex-col items-center justify-start gap-2 mb-10">
        <AnnounceCard />
        <AnnounceCard />
        <AnnounceCard />
        <AnnounceCard />
      </div>
    </div>
  );
};
