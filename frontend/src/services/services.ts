import axios from "axios";
import { UserPreferences } from "@/interaces";
import { useAxios } from "@/Hooks/useAxios";

interface UserDetails {
    email: string
    password: string
    firstName: string
    lastName: string
    phoneNumber: string
    role:   string
    userPreferences?: UserPreferences
}

interface jwtToken {
    id:     string
    email:  string
    roles:  string
    iat:    string
    exp:    string
}

export const jwtDecoder = (accessToken: string): jwtToken | null => {

    if (!accessToken)
        return null;

    const tokenPayload = accessToken.split('.')[1];
    if (tokenPayload)
        return JSON.parse(atob(tokenPayload));

    return null;
}

export const loginService = async (email: string, password: string) => {

    try {

        const response = await axios.post("http://localhost:5000/auth/login", {
            email: email,
            password: password
        }, {
            withCredentials: true
        })

        return response

    } catch (error: any) {
        console.log(error.message)
    }

}

export const signUpFormSubmit = async (userDetails: FormData) => {

    try {

        const response = await axios.post(`http://localhost:5000/auth/signup`, userDetails, {
            withCredentials: true
        })

        console.log(response.data)

        return response.data

    } catch (error: any) {
        console.log(error.message)
    }

}
