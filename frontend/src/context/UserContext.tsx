import { ChangeEvent, FormEvent, createContext, useContext, useState } from "react";
import axios, { AxiosError } from "axios"
import { useRouter } from "next/router";
import { useAxios } from "@/Hooks/useAxios";
import { loginService, signUpFormSubmit } from "@/services/services";
import { jwtDecoder } from "@/services/services";
import { UserPreferences } from "@/interaces";

const AppContext = createContext<any>(null);

interface UserDetails {
  email: string
  password: string
  firstName: string
  lastName: string
  phoneNumber: string
  role:   string
  userPreferences?: UserPreferences
}

export const AppWrapper = ({ children }: any) => {

  // LOGGED IN USER
  const [ userRole, setUserRole ] = useState<string>("")

  const [user, setUser] = useState<string>("");
  const [userStatus, setUserStatus] = useState<boolean>(false)
  const [token, setToken] = useState<string>("");
  const [inputMail, setInputMail] = useState<string>("");
  const [inputPassword, setInputPassword] = useState<string>("");
  const [inputRetypePassword, setInputRetypePassword] = useState<string>("")
  const [inputName, setInputName] = useState<string>("");
  const [inputRole, setInputRole] = useState<string>("")
  const [inputLastName, setInputLastName] = useState<string>("");
  const [inputPhone, setInputPhone] = useState<string>("");
  
  const [verifNumber, setVerifNumber] = useState<string>("");
  const [accessToken, setAccessToken] = useState<string | null>("")

  // CREATOR FORM
  const [ showUploadCertificate, setShowUploadCertificate ] = useState<boolean>(false)
  const [ showUploadWorkshop,  setShowUploadWorkshop ] = useState<boolean>(false)
  const [ certificateFiles, setCertificateFile ] = useState<File | null>(null)
  const [ certificatePreview, setCertificatePreview ] =useState<string | ArrayBuffer | null>(null)
  const [ workshopImages, setWorkshopImages ] = useState<File | null>(null)
  const [ workShopPreview, setWorkShopPreview ] =useState<string | ArrayBuffer | null>(null)
  const [ professionLevel, setProfessionLevel ] = useState<string>("")
  const [ professionCategory, setProfessionCategory ] = useState<string>("Artistique")

  // ERROR HANDLING
  const [ loginError, setLoginError ] = useState<boolean>(false)
  const [ errorMsg, setErrorMsg ] = useState<string>("")

  // LOADING STATE
  const [ loading, setLoading ] = useState<boolean>(false)

  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false)

  const handleCertificationUpload = (e: ChangeEvent<HTMLInputElement>) => {

    if (e.target.files && e.target.files[0]) {
        const file = new FileReader
    
        file.onload = () => {
    
            if ( typeof file.result !== 'string' ) return ;
    
            setCertificatePreview(file.result)
    
        }
    
        file.readAsDataURL(e.target.files[0])
        setCertificateFile(e.target.files[0])
    }


  }

  const handleWorkshopImagesupload = (e: React.ChangeEvent<HTMLInputElement>) => {

      if (e.target.files && e.target.files[0]) {
          const file = new FileReader
    
          file.onload = () => {
    
              if ( typeof file.result !== 'string' ) return ;
    
              setWorkShopPreview(file.result)
    
          }
    
          file.readAsDataURL(e?.target?.files[0])
          setWorkshopImages(e?.target?.files[0])
      }

  }

  const router = useRouter()

  const afterLoginredirect = () => {

    const token = localStorage.getItem("accessToken")

    const decodedToken = jwtDecoder(token as string)

    if (decodedToken?.roles == "CREATOR")
      return router.push("/Dashboard")
    else if (decodedToken?.roles == "ADMIN")
      return router.push("/admin/home")
    else
      return router.push("/noselections")


  }

  const login = async () => {

    try {

      const response = await loginService(inputMail, inputPassword)

      if (response !== undefined) {
        localStorage.setItem("accessToken", response?.data.accessToken)
        setAccessToken(response.data.accessToken)
        afterLoginredirect()
        return setIsAuthenticated(true)
      } else {
        setLoginError(true)
        setErrorMsg("mot de passe ou email incorrect")
        return setIsAuthenticated(false)
      }
      
    } catch (error: any) {
      console.log("ERROR MESSAGE ==> ", error.message)
    }

  }

  const logout = () => {

    localStorage.clear()
    router.push("/")
    setIsAuthenticated(false)
    
  }

  const authenticationChecker = () => {

    const token = localStorage.getItem("accessToken")
    
        if (token !== null)
        {
          
          setIsAuthenticated(true)

          const decoded = jwtDecoder(token)

          console.log(decoded)
    
          if (decoded?.roles == "CREATOR")
            return router.push("/Dashboard")
          else if (decoded?.roles == "ADMIN")
            return router.push("/admin/home")
          else
            return router.push("/noselections")
    
        }
        else
          return router.push("/Sign_in")

  }

  const globalAuthChecker = () => {

    // const token = localStorage.getItem("accessToken")
  
    // if (token !== null)
    // {
    //   setIsAuthenticated(true)
      
    //   const decoded = jwtDecoder(token)
      
    //   if (decoded?.roles == "CREATOR")
    //   {
    //     // setUserStatus(decoded?.)
    //     return setUserRole("CREATOR")
    //   }
    //   else if (decoded?.roles =="CLIENT")
    //     return setUserRole("CLIENT")
    //   else if (decoded?.roles == "ADMIN")
    //     return setUserRole("ADMIN")
    // } 
    // else
    //   return setIsAuthenticated(false)

  }

  const clientSignUp = async (e: FormEvent<HTMLFormElement>) => {

    e.preventDefault()

    const form = new FormData()

    if (certificateFiles && workshopImages) {
      form.append("certificationImage", certificateFiles)
      form.append("WorkshopImage", workshopImages)
      form.append("name", `${inputName} ${inputLastName}`)
      form.append("email", inputMail)
      form.append("password", inputPassword)
      form.append("phoneNumber", inputPhone)
      form.append("role", inputRole),
      form.append("category", professionCategory)
      form.append("professionLevel", professionLevel)
    }

    try {


      const response = await signUpFormSubmit(form)

      console.log(response)


    } catch (error: any) {
      console.log(error.message)
    }
  
  
  }

  const googleLogin = async () => {

    try {

      const { data } = await axios.get("http://localhost:5000/auth")

      console.log(data)

    } catch (error: any) {
      console.log(error.message)
    }

  }

  return (
    <AppContext.Provider
      value={{
        user, setUser,
        token, setToken,
        isAuthenticated, setIsAuthenticated,
        inputMail, setInputMail,
        inputPassword, setInputPassword,
        inputName, setInputName,
        inputLastName, setInputLastName,
        inputPhone, setInputPhone,
        inputRetypePassword, setInputRetypePassword,
        verifNumber, setVerifNumber,
        inputRole, setInputRole,
        showUploadCertificate, setShowUploadCertificate,
        showUploadWorkshop,  setShowUploadWorkshop,
        certificateFiles, setCertificateFile,
        certificatePreview, setCertificatePreview,
        workshopImages, setWorkshopImages,
        workShopPreview, setWorkShopPreview,
        professionLevel, setProfessionLevel,
        professionCategory, setProfessionCategory,
        handleCertificationUpload,
        handleWorkshopImagesupload,
        accessToken,
        login,
        logout,
        clientSignUp,
        authenticationChecker,
        globalAuthChecker,
        userRole,
        loginError, setLoginError,
        errorMsg, setErrorMsg,
        googleLogin
      }}
    >
      {children}
    </AppContext.Provider>
  );
};

export default function GetContext() {
  return useContext(AppContext);
}
