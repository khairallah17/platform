import { createContext, useState, useReducer } from 'react'
import { AnnouncementDateTime } from '@/interaces'
import dayjs from 'dayjs'

export const AnnoucementContext = createContext<any>({})

export const AnnoucementContextProvider = ({ children }: any) => {

    const dateTimeInitState = [{}]
    const numberOfDatesInitState = [1]

    const enum REDUCER_ACTION_TYPES_DATE_TIME {
        START_TIME,
        END_TIME,
        DATE
    }

    const enum REDUCER_ACTION_TYPES_NUMBER {
        INCREMENT,
        DECREMENT
    }

    type ReducerActionDateTime = {
        type: REDUCER_ACTION_TYPES_DATE_TIME,
        value:  Date
    }

    type ReducerActionNumber = {
        type: REDUCER_ACTION_TYPES_NUMBER,
    }

    const dateTimeReducer = (state: typeof dateTimeInitState, action: ReducerActionDateTime): typeof dateTimeInitState => {

        switch (action.type) {
            case REDUCER_ACTION_TYPES_DATE_TIME.START_TIME:
                state[0] = {startTime: dayjs(action.value).toDate().getTime()}; break;
            case REDUCER_ACTION_TYPES_DATE_TIME.END_TIME:
                state[0] = {endTime: dayjs(action.value).toDate().getTime()}; break;
            case REDUCER_ACTION_TYPES_DATE_TIME.DATE:
                state[0] = {eventDate: dayjs(action.value).toDate()}; break;
        }
        
        return [{}]
    }

    const numberReducer = (state: typeof numberOfDatesInitState, action: ReducerActionNumber): number[] => {

        
        switch(action.type) {
            case REDUCER_ACTION_TYPES_NUMBER.INCREMENT:
                state.length == 3 ? state : state.push(1); break;
            case REDUCER_ACTION_TYPES_NUMBER.DECREMENT:
                state.length == 1 ? state : state.pop(); break;
        }

        console.log(state)

        return state;

    }

    
    const [workshopTitle, setWorkShopTitle] = useState<string>("")
    const [workshopCategorie, setWorkshopCategorie] = useState<string>("")
    const [workshopUnivers, setWorkshopUnivers] = useState<string>("")
    const [workshopCity, setWorkshopCity] = useState<string>("")
    const [workshopAddress, setWorkshopAddress] = useState<string>("")
    const [workshopDateAndTime, setWorkshopDateAndTime] = useReducer(dateTimeReducer, dateTimeInitState)
    const [numberOfDates, dispatchNumberOfDates] = useReducer(numberReducer, numberOfDatesInitState)
    const [workshopImages, setWorkShopImage] = useState<FileList | null>(null)
    const [workshopDescription, setWorkshopDescription] = useState<string>("")
    const [workshopDetails, setWorkshopDetails] = useState<Array<JSON>>([])
    
    
    // console.log(numberOfDates)

    const [startTime, setStartTime] = useState<Date | null>(null)
    const [endTime, setEndTime] = useState<Date | null>(null)
    const [eventDate, setEventDate] = useState<Date | null>(null)

    const values = {
        workshopTitle, setWorkShopTitle,
        workshopCategorie, setWorkshopCategorie,
        workshopUnivers, setWorkshopUnivers,
        workshopCity, setWorkshopCity,
        workshopAddress, setWorkshopAddress,
        workshopDateAndTime, setWorkshopDateAndTime,
        workshopImages, setWorkShopImage,
        workshopDescription, setWorkshopDescription,
        workshopDetails, setWorkshopDetails,
        numberOfDates, dispatchNumberOfDates
    }

    return (
        <AnnoucementContext.Provider value={values}>
            { children }
        </AnnoucementContext.Provider>
    )

}

export default AnnoucementContext