import { Header } from "@/Components/LandingPage/Header";
import { Footer } from "@/Components/LandingPage/Footer";
import { Filter } from "@/Components/LandingPage/Filter";
import { WishListCard } from "@/Components/LandingPage/WishListCard";
export default function WishList() {
  return (
    <div className="w-full h-[100vh]  bg-[#FFFBFC]">
      <Header />
      <Filter />
      <div className="w-full h-fit mt-4 px-72 laptop:px-12 tablet:px-1 phone:px-1 flex flex-col items-center justify-start gap-4">
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
        <WishListCard />
      </div>
      <div className="w-full h-[20px]"></div>
      <Footer />
    </div>
  );
}
