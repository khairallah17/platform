import Dashboard from "../Dashboard";
import { FillAnnounce } from "@/Components/Dashboard/FillAnnounce";
import Swal from "sweetalert2";

export default function Fill() {
  const showSuccess = () => {
    Swal.fire({
      title: "Annonce créée avec succès",
      showClass: {
        popup: "animate__animated animate__fadeInDown",
      },
      hideClass: {
        popup: "animate__animated animate__fadeOutUp",
      },
      color: "#000",
      icon: "success",
      iconColor: "#9747FF",
    });
  };
  return (
    <Dashboard>
        <FillAnnounce/>
    </Dashboard>
  );
}
