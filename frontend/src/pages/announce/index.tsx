import Dashboard from "../Dashboard";
import { AnnounceSection } from "@/Components/Dashboard/AnnounceSection";

export default function Index() {
  return (
    <Dashboard>
      <AnnounceSection />
    </Dashboard>
  );
}
