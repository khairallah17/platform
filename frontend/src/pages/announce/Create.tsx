import Dashboard from "../Dashboard";
import { CreateSection } from "@/Components/Dashboard/CreateSection";
import { useState } from "react";
import Swal from "sweetalert2";
import { InfoArticle } from "@/Components/Dashboard/InfoArticle";
import { UploadInfo } from "@/Components/Dashboard/UploadInfo";
import { TimeInfo } from "@/Components/Dashboard/TimeInfo";
import { InfoLocation } from "@/Components/Dashboard/infoLocation";
// import { DescriptionFill } from "@/Components/Dashboard/DescriptionFill";
import { Details } from "@/Components/Dashboard/Details";

const Create = () => {
  const [showNext, setShowNext] = useState(false);
  const showSuccess = () => {
    if (showNext) {
      Swal.fire({
        title: "Annonce est encours de traitement",
        showClass: {
          popup: "animate__animated animate__fadeInDown",
        },
        hideClass: {
          popup: "animate__animated animate__fadeOutUp",
        },
        color: "#000",
        icon: "success",
        iconColor: "#9747FF",
      });
    }
  };
  return (
    <Dashboard>

      <CreateSection/>

    </Dashboard>
  );
}

export default Create
