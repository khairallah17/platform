import { NextResponse } from "next/server"

export async function POST(request: Request) {

    const body = await request.json()

    const { username, password } = body

    if ( username !== "admin" && password !== "admin" ) {
        return NextResponse.json(
            {
                message: "UnAuthorized"
            },
            {
                status: 401
            }
        )
    }
    return NextResponse.json(
        {
            message: "authorized"
        },
        {
            status: 200
        }
    )

}

export async function GET() {
    return new Response("hello")
}