import React from 'react'
import { Header } from '@/Components/LandingPage/Header'
import { Footer } from '@/Components/LandingPage/Footer'

const Index = () => {
  return (
    <div className='h-screen flex flex-col justify-between'>
        <Header/>
        <div>
            <h1 className="text-center text-4xl font-bold my-20">
            Page en construction, comming soon...
            </h1>
        </div>
        <Footer/>
    </div>
  )
}

export default Index