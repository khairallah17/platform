import Image from "next/image";
import { SignUpComponent } from "@/Components/Login/Signup";
import { FormEvent, FormEventHandler, useState } from "react";
import GetContext from "@/context/UserContext";
import { useRouter } from "next/router";

export default function Step1() {

  const [showPassword, setShowPassword] = useState(false);
  const [showPassword2, setShowPassword2] = useState(false);
  const [showCheck, setShowCheck] = useState(false);
  const [showCheck2, setShowCheck2] = useState(false);
  const [showAccepte, setShowAccepte] = useState(false);
  const [showProfession, setShowProfession] = useState(0);
  const [showUploadDiploma, setShowUploadDiploma] = useState(0);
  const [emptyFieldsError, setEmptyFieldsError] = useState<boolean>(false)
  const [errorMsg, setErrorMsg] = useState<string>("")

  const handleShowAccepte = () => setShowAccepte(!showAccepte);

  const handleShowCheck2 = () => {
    setShowCheck2(true);
    setShowCheck(false);
  };

  const handleShowCheck = () => {
    setShowCheck(true);
    setShowCheck2(false);
  };

  const {
    inputMail, setInputMail,
    inputPassword, setInputPassword,
    inputRetypePassword, setInputRetypePassword,
    inputName, setInputName,
    inputLastName, setInputLastName,
    inputPhone, setInputPhone,
  } = GetContext()

  const router = useRouter()

  const onFormSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault()

    if(!inputName) {
      setErrorMsg("veuillez saisir votre nom")
      return setEmptyFieldsError(true)
    } else if (!inputLastName) {
      setErrorMsg("veuillez saisir votre prenom")
      return setEmptyFieldsError(true)
    } else if (!inputMail) {
      setErrorMsg("veuillez saisir votre email")
      return setEmptyFieldsError(true)
    } else if (!inputPhone) {
      setErrorMsg("veuillez saisir votre numero de telephone")
      return setEmptyFieldsError(true)
    } else if (!inputPassword) {
      setErrorMsg("veuillez saisir votre Mot de Passe")
      return setEmptyFieldsError(true)
    } else if (!inputRetypePassword) {
      setErrorMsg("veuillez retapez votre mot de passe")
      return setEmptyFieldsError(true)
    } else if (inputPassword != inputRetypePassword) {
      setErrorMsg("les mots de passes ne sont pas identiques")
      return setEmptyFieldsError(true)
    }
    
    router.push("/Signup/creator/step2")
  }

  const handleShowPassword = () => setShowPassword(!showPassword);

  const handleShowPassword2 = () => setShowPassword2(!showPassword2);

  return (
    <div className="w-full h-[120vh] phone:h-[150vh] flex items-center justify-center">
      <div className="w-1/2 h-full flex items-start justify-center tablet:w-full phone:w-full Signbg laptop:w-2/3">
        <div className="h-fit w-[600px] phone:w-[370px] mt-10 phone:flex phone:flex-col phone:items-center phone:justify-start">
          <Image
            src="/LogoSign.png"
            alt="Logo"
            width={70}
            height={70}
            className="mb-4 w-[60px] h-[60px] self-center"
          />
          <h1 className=" font-gotham-bold text-[26px] capitalize phone:text-center tablet:text-center text-white mb-4 text-start">
            Rejoignez-nous autant que créateur
          </h1>
          <SignUpComponent
            showCheck={showCheck}
            showCheck2={showCheck2}
            handleShowCheck={handleShowCheck}
            handleShowCheck2={handleShowCheck2}
            showPassword={showPassword}
            showPassword2={showPassword2}
            handleShowPassword={handleShowPassword}
            handleShowPassword2={handleShowPassword2}
            showAccepte={showAccepte}
            handleShowAccepte={handleShowAccepte}
            formSubmit={onFormSubmit}
            error={emptyFieldsError}
            errorMsg={errorMsg}
          />
        </div>
      </div>
      <div className="w-1/2 h-full SignImg tablet:hidden phone:hidden laptop:w-1/3">
        <div className="w-full h-full bgOpa"></div>
      </div>
    </div>
  );
}
