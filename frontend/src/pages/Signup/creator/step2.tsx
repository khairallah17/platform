import Image from "next/image";
import { ChangeEvent } from "react";
import Link from "next/link";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import GetContext from "@/context/UserContext";

export default function Step2() {

    const { 
      showUploadCertificate, setShowUploadCertificate,
      showUploadWorkshop,  setShowUploadWorkshop,
      certificatePreview, setCertificatePreview,
      workshopImages, setWorkshopImages,
      workShopPreview, setWorkShopPreview,
      professionLevel, setProfessionLevel,
      professionCategory, setProfessionCategory,
      handleCertificationUpload,
      handleWorkshopImagesupload,
      clientSignUp
    } = GetContext()


    const certificationUpload = (state: boolean) => {
      if (state)
        setShowUploadCertificate(true)
      else {
        setShowUploadCertificate(false)
        setCertificatePreview(null)
      }
    }

    const workshopUpload = (state: boolean) => {
      if (state)
        setShowUploadWorkshop(true)
      else {
        setWorkShopPreview(null)
        setShowUploadWorkshop(false)
      }
    }

    const professionCategorySetter = (event: ChangeEvent<HTMLInputElement>) => {

      const value = event.target.value

      setProfessionCategory(value)

    }

  return (
    <div className="w-full h-screen flex items-center justify-center">
        <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
        />
      <div className="w-1/2 h-full flex items-start justify-center tablet:w-full phone:w-full Signbg laptop:w-2/3">
        <div className="h-fit w-[600px] phone:w-[370px] mt-10 phone:flex phone:flex-col phone:items-center phone:justify-start">
          <Image
            src="/LogoSign.png"
            alt="Logo"
            width={70}
            height={70}
            className="mb-4 w-[60px] h-[60px] self-center"
          />
          <h1 className=" font-gotham-bold text-[26px] text-white mb-4 text-start">
            Rejoignez-nous autant que créateur
          </h1>
          <form onSubmit={clientSignUp} className="w-full h-fit bg-white p-8 rounded-[20px] flex flex-col items-start justify-start gap-3">

            <h1 className="font-gotham font-normal text-[16px] phone:text-[13px]">
              Votre catégorie de savoir-faire
            </h1>
            <div className="w-[80%]  h-[52px] bg-white rounded-[8px] border-2 border-[#ADADAD] overflow-hidden pl-4 relative flex items-center">
              <select
                onChange={professionCategorySetter as any}
                className="custom-select w-[100%] h-[100%] outline-none rounded-[5px] font-trebuchet font-medium"
                placeholder="Categories"
              >
                <option value="artistique">Artistique</option>
                <option value="culinaire">Culinaire</option>
                <option value="manuel">Manuel</option>
                <option value="nature">Nature</option>
                <option value="multimedia">Multimedia</option>
              </select>
            </div>

            <span className="font-gotham font-normal text-[16px] mt-4 phone:text-[13px] capitalize">
              Exercez-vous ce savoir-faire comme :
            </span>
            <div className="w-full h-fit flex items-start justify-start gap-8">
              <div className="w-fit h-fit flex items-center justify-start gap-2">
                <input onClick={() => setProfessionLevel("professionel")} type="radio" name="profession" id="" />
                <span className=" font-normal text-[14px] text-[#858585]">
                  Professionel
                </span>
              </div>
              <div className="w-fit h-fit flex items-center justify-start gap-2">
                <input onClick={() => setProfessionLevel("amateur")} type="radio" name="profession" id="" />
                <span className=" font-normal text-[14px] text-[#858585]">
                  Amateur
                </span>
              </div>
            </div>

            <span className="font-gotham font-normal text-[16px] mt-4 phone:text-[13px] capitalize">
              Disposez-vous de diplômes ou certificats liés à ce savoir-faire :
            </span>
            <div className="w-full h-fit flex items-start justify-start gap-8">
              <div className="w-fit h-fit flex items-center justify-start gap-2">
                <input type="radio" onClick={() => certificationUpload(true)} name="certificate" id="" />
                <span className=" font-normal text-[14px] text-[#858585]">Oui</span>
              </div>
              <div className="w-fit h-fit flex items-center justify-start gap-2">
                <input type="radio" onClick={() => certificationUpload(false)}  name="certificate" id="" />
                <span className=" font-normal text-[14px] text-[#858585]">Non</span>
              </div>
            </div>

            {
                showUploadCertificate &&
                <>
                    <input type="file" multiple onChange={handleCertificationUpload} name="cetificate" className="mt-2" id="" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />
                    <Image src={certificatePreview as string || ""} alt="" height={50} width={50} className="border-none" />
                </>
            }

            <span className="font-gotham font-normal mt-4 text-[16px] phone:text-[13px] capitalize">
                Disposez-vous d’un local
            </span>
            <div className="w-full h-fit flex items-start justify-start gap-8">
              <div className="w-fit h-fit flex items-center justify-start gap-2">
                <input type="radio" onClick={() => workshopUpload(true)} name="atelier" id="" />
                <span className=" font-normal text-[14px] text-[#858585]">Oui</span>
              </div>
              <div className="w-fit h-fit flex items-center justify-start gap-2">
                <input type="radio" onClick={() => workshopUpload(false)} name="atelier" id="" />
                <span className=" font-normal text-[14px] text-[#858585]">Non</span>
              </div>
            </div>

            {
                showUploadWorkshop && 
                <>
                    <input type="file" multiple onChange={handleWorkshopImagesupload} name="atelier" className="mt-2" id="" accept="image/jpeg,image/gif,image/png,application/pdf,image/x-eps" />
                    <Image src={workShopPreview as string || ""} alt="" height={50} width={50} className="border-none" />
                </>
            }

            <div className="w-full flex justify-end items-end">
                <button type="submit" className="text-center w-fit px-20 p-3 laptop:w-[270px] h-[50%] bg-components-bold flex items-center rounded-[5px] justify-center text-white font-trebuchet font-bold text-[20px] laptop:text-[16px]">
                    M&apos;inscrire
                </button>
            </div>

          </form>
        </div>
      </div>
      <div className="w-1/2 h-full SignImg tablet:hidden phone:hidden laptop:w-1/3">
        <div className="w-full h-full bgOpa"></div>
      </div>
    </div>
  );
}
