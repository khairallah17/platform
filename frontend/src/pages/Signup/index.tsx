import Image from "next/image";
import { useState } from "react";
import Link from "next/link";
import GetContext from "@/context/UserContext";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { jwtDecoder } from "@/services/services";

export default function Question() {

  const { inputRole, setInputRole } = GetContext()

  const [show, setShow] = useState<number>(0);
  
  const router = useRouter()

  return (
    <div className="w-full h-[100vh] relative overflow-hidden flex items-center justify-center">
      <div className="w-[400px] h-[210px] laptop:w-[360px] phone:w-[300px]flex flex-col items-center justify-start py-3 gap-3 z-[99]">
        <h1 className="font-bold capitalize text-[25px] text-[#0E1A2A] text-center">
          Rejoignez-nous en tant que ?
        </h1>
        <div className="w-full h-fit flex items-center justify-between mt-8">
          <Link href="/Signup/creator/step1"
            onMouseEnter={() => {
              setShow(1);
            }}
            onClick={() => setInputRole("CREATOR")}
            className="bg-components-bold w-[160px] h-[48px] flex items-center justify-center text-white font-trebuchet font-bold text-[20px] rounded-[5px] phone:text-[17px]"
          >
            Créateur
          </Link>
          <Link href="/Signup/client"
            onMouseEnter={() => {
              setShow(2);
            }}
            onClick={() => setInputRole("CLIENT")}
            className="bg-components-bold w-[160px] h-[48px] flex items-center justify-center text-white font-trebuchet font-bold text-[20px] rounded-[5px] phone:text-[17px]"
          >
            Client
          </Link>
        </div>
        <h1 className=" font-medium text-[15px] text-[#929292] mt-8  text-center transition-all duration-300 ease-in-out">
          {show === 1
            ? "Rejoignez-nous en tant qu'artisan, artiste, créateur, créatif ?"
            : show === 2
            ? "Vous souhaitez participer à un atelier ?"
            : ""}
        </h1>
      </div>
      <Image
        width={100}
        height={100}
        src="/Signup/v2.svg"
        alt=""
        className="w-[46%] absolute left-[-3%] bottom-[-1.5%] z-[1]"
      ></Image>
      <Image
        width={100}
        height={100}
        src="/Signup/v1.svg"
        alt=""
        className="w-[46%] absolute left-[-2.6%] bottom-[-1%] z-[2]"
      ></Image>
      <Image
        width={100}
        height={100}
        src="/Signup/v3.svg"
        alt=""
        className="w-[48%] h-fit absolute right-0 top-0 z-[1]"
      ></Image>
      <Image
        width={100}
        height={100}
        src="/Signup/v4.svg"
        alt=""
        className="w-[49%] h-fit absolute right-0 top-0 z-[2]"
      ></Image>
    </div>
  );
}
