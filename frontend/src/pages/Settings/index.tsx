import React from 'react'
import Dashboard from '../Dashboard'
import SettingsComponent from '@/Components/Dashboard/SettingsComponent'

const index = () => {
  return (
    <Dashboard>
        <SettingsComponent/>
    </Dashboard>
  )
}

export default index