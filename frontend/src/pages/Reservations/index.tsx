import React from 'react'
import Dashboard from '../Dashboard'
import ReservationsSection from '@/Components/Dashboard/ReservationsSection'

const index = () => {
  return (
    <Dashboard>
      <ReservationsSection />
    </Dashboard>
  )
}

export default index