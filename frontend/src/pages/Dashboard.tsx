import { Header } from "@/Components/Dashboard/Header";
import { BiHomeAlt2, BiSolidBarChartAlt2 } from "react-icons/bi";
import { AiOutlineFileDone, AiOutlineSetting } from "react-icons/ai";
import { CiReceipt } from "react-icons/ci"
import { BsCreditCard } from "react-icons/bs";
import { HomeSection } from "@/Components/Dashboard/HomeSection";
import { useState } from "react";
import { useRouter } from "next/router";
import { MdLogout } from "react-icons/md"
import GetContext from "@/context/UserContext";

function Dashboard({ children }: any) {

  const { logout } = GetContext()

  const router = useRouter();
  const [showAnnounce, setShowAnnounce] = useState<boolean>(false);
  const [showHome, setShowHome] = useState<boolean>(true);
  const [showStat, setShowStat] = useState<boolean>(false);
  const [showPayment, setShowPayment] = useState<boolean>(false);
  const [showSetting, setShowSetting] = useState<boolean>(false);

  const handleShowAnnounce = () => {
    router.push("/announce");
  };

  const handleShowHome = () => {
    router.push("/Home");
  };

  const handleShowStat = () => {
    router.push("/Reservations")
  };

  const handleShowPayment = () => {
    if (!showPayment) setShowPayment(true);
    setShowAnnounce(false);
    setShowHome(false);
    setShowStat(false);
    setShowSetting(false);
  };

  const handleShowSetting = () => {
    router.push("/Settings")
  };

  return (
    <div className="w-full h-[100vh] ">
      <Header />
      <div className="w-full bg-orange-400 MainDash flex items-start justify-center">
        <div className="h-full w-[70px] bg-[#9747FF] pt-[30px] justify-between flex flex-col">
          <div className="top">
            <button
              onClick={handleShowHome}
              className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
            >
              <BiHomeAlt2 className=" text-[30px]" />
            </button>
            <button
              onClick={handleShowAnnounce}
              className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
            >
              <AiOutlineFileDone className=" text-[30px]" />
            </button>
            <button
              onClick={handleShowStat}
              className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
            >
              <CiReceipt className=" text-[30px]" />
            </button>
            <button
              onClick={handleShowPayment}
              className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
            >
              <BsCreditCard className=" text-[30px]" />
            </button>
            <button
              onClick={handleShowSetting}
              className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
            >
              <AiOutlineSetting className=" text-[30px]" />
            </button>
          </div>
          <button
            onClick={logout}
            className="w-full h-fit flex items-center justify-center py-5 hover:bg-white text-white hover:text-[#9747FF] transition-all ease-out duration-300"
          >
              <MdLogout className=" text-[30px]" />
          </button>
        </div>
        {children ? children : <HomeSection />}
      </div>
    </div>
  );
}

export default Dashboard