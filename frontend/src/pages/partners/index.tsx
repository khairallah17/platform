import React, { FormEvent, FormEventHandler } from 'react'
import Image from 'next/image'
import { useRouter } from 'next/navigation'

const Index = () => {

    const router = useRouter()

    const submitForm = (e: FormEvent<HTMLFormElement>) => {
        router.push("/")
    }

  return (
    <div className='h-screen flex items-center justify-center'>
            <Image
                width={100}
                height={100}
                src="/Signup/v2.svg"
                alt=""
                className="w-[46%] absolute left-[-3%] bottom-[-1.5%] z-[1]"
            ></Image>
            <Image
                width={100}
                height={100}
                src="/Signup/v1.svg"
                alt=""
                className="w-[46%] absolute left-[-2.6%] bottom-[-1%] z-[2]"
            ></Image>
            <Image
                width={100}
                height={100}
                src="/Signup/v3.svg"
                alt=""
                className="w-[48%] h-fit absolute right-0 top-0 z-[1]"
            ></Image>
            <Image
                width={100}
                height={100}
                src="/Signup/v4.svg"
                alt=""
                className="w-[49%] h-fit absolute right-0 top-0 z-[2]"
            ></Image>

        <form className='z-[99] relative w-[550px] flex flex-col gap-3 items-center justify-center h-full' onSubmit={submitForm}>

            <h1 className='text-2xl capitalize font-gotham font-bold mb-4 text-components-bold'>Rejoinez-nous en tant que partenaire</h1>

            <div className='flex gap-3 w-full'>
                <input className='outline-none w-full border-components-bold bg-slate-200 border-2 px-4 py-2 rounded' type="text" placeholder='nom' name="fristName" id="" />
                <input className='outline-none w-full border-components-bold bg-slate-200 border-2 px-4 py-2 rounded' type="text" placeholder='prenom' name="lastName" id="" />
            </div>

            <input className='outline-none w-full border-components-bold bg-slate-200 border-2 px-4 py-2 rounded' type="email" placeholder='address mail professionel' name="email" id="" />
            <input className='outline-none w-full border-components-bold bg-slate-200 border-2 px-4 py-2 rounded' type="text" placeholder="nom de l'entreprise" name='company' />
            <input className='outline-none w-full border-components-bold bg-slate-200 border-2 px-4 py-2 rounded' type="text" placeholder='objet' name='objet' />

            <textarea placeholder='ecrivez-nous une demande' className='outline-none w-full border-components-bold bg-slate-200 border-2 px-4 py-2 rounded' name="content" id="" cols={30} rows={10}>

            </textarea>

            <button type='submit' className='outline-none w-full border-components-bold border-2 bg-components-bold text-white py-3 rounded text-xl hover:bg-white hover:text-components-bold duration-200'>
                Envoyez !!
            </button>

        </form>

    </div>
  )
}

export default Index