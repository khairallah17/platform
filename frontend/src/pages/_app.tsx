import "@/styles/globals.css";
import type { AppProps } from "next/app";
import { AppWrapper } from "@/context/UserContext";
import { AnnoucementContextProvider } from "@/context/AnnoucementContext";

export default function App({ Component, pageProps }: AppProps) {
  
  return (
    <AnnoucementContextProvider>
      <AppWrapper>
        <Component {...pageProps} />
      </AppWrapper>
    </AnnoucementContextProvider>
  );
}
