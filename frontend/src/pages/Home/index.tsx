import Dashboard from "../Dashboard";
import { HomeSection } from "@/Components/Dashboard/HomeSection";
export default function Home() {
  return (
    <Dashboard>
      <HomeSection />
    </Dashboard>
  );
}
