import { useState } from "react";
import { Header } from "@/Components/LandingPage/Header";
import Image from "next/image";
import { GiDuration } from "react-icons/gi";
import Link from "next/link";
import { AiFillStar } from "react-icons/ai";
import { Footer } from "@/Components/LandingPage/Footer";
import { AiOutlineCheckCircle } from "react-icons/ai";
import { useSearchParams } from "next/navigation"
import { Swiper, SwiperSlide } from 'swiper/react';
import { Swiper as swiper } from "swiper/types";

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';

// import required modules
import { Navigation, FreeMode, Thumbs } from 'swiper/modules';

export default function Article() {

  const arrTest = [
    {
      id: 1,
    },
    {
      id: 2,
    },
    {
      id: 3,
    },
    {
      id: 4,
    },
  ];

  const images = ["backArticle.png", "ArticlesImg.png","backArticle.png", "ArticlesImg.png","backArticle.png", "ArticlesImg.png","backArticle.png", "ArticlesImg.png",]


  const searchParams = useSearchParams()

  const id = searchParams?.get("id")

  const [thumbsSwiper, setThumbsSwiper] = useState<swiper | null>(null);

  return (
    <div className="w-full h-[100vh]  bg-[#FFFBFC]">
      <Header />
      <div className="container mx-auto my-20">
          <div className="flex flex-col gap-12">

            <div className="flex flex-col gap-6">
                <h1 className="font-trebuchet capitalize font-normal text-[48px] tablet:text-[40px] phone:text-[35px] text-[#410053] mt-4">
                    Atelier poterie
                </h1>
                <div className="grid grid-cols-2 tablet:grid-cols-1 phone:grid-cols-1 gap-12 h-full">

                    <div className="flex flex-col w-full gap-2">

                        <div className="flex flex-col gap-2">
                            <Swiper
                                style={{
                                '--swiper-navigation-color': '#fff',
                                '--swiper-pagination-color': '#fff',
                                }}
                                loop={true}
                                spaceBetween={10}
                                navigation={true}
                                thumbs={{ swiper: thumbsSwiper }}
                                modules={[FreeMode, Navigation, Thumbs]}
                                className="mySwiper2 h-3/4"
                            >
                                {
                                    images?.map((item, index) => (
                                        <SwiperSlide key={index} className="rounded-lg">
                                            <Image alt="" src={`/${item}`} height={0} width={0} sizes="100vw" className="rounded-lg" />
                                        </SwiperSlide>
                                    ))
                                }
                            </Swiper>
                            <Swiper
                                onSwiper={setThumbsSwiper}
                                loop={true}
                                spaceBetween={10}
                                slidesPerView={4}
                                freeMode={true}
                                watchSlidesProgress={true}
                                modules={[FreeMode, Navigation, Thumbs]}
                                className="mySwiper h-1/4"
                            >
                                {
                                    images?.map((items, index) => (
                                        <SwiperSlide key={index} className="rounded-lg">
                                            <Image src={`/${items}`} width={0} height={0} alt="" sizes="100vw" className="rounded-lg" />
                                        </SwiperSlide>
                                    ))
                                }
                            </Swiper>
                        </div>

                        <div className="mt-2 phone:mt-0  flex items-center justify-end gap-4">
                            <h1 className=" font-trebuchet font-bold text-[16px] text-[#4E326550] line-through phone:text-[13px]">
                                140000.00 dh
                            </h1>
                            <h1 className=" font-trebuchet font-bold text-[25px] text-[#4E3265] phone:text-[20px]">
                                140000.00 dh
                            </h1>
                        </div>

                        <div className="hidden h-fit phone:flex tablet:flex items-center justify-end">
                            <button className="px-28 py-2 bg-components-bold rounded-[5px] flex items-center justify-center cursor-pointer font-trebuchet font-bold text-[15px] text-white ">
                                Reserver
                            </button>
                        </div>
                    </div>

                    <div className="h-full pt-0 p-20 self-center flex items-center justify-center phone:hidden tablet:hidden">
                        <div className="sti w-full h-full bg-gray-200 p-24 flex flex-col items-center justify-center gap-4">
                            <h1 className="text-white font-trebuchet font-bold tracking-[0.8px] text-[20px]">
                                RESERVE VOTRE PLACE
                            </h1>
                            <input type="text" className="w-full py-2  outline-none font-trebuchet font-light  text-[14px] pl-4 text-components-bold rounded-[4px] border-[1.8px] border-components-bold" placeholder="Nom et Prenom" />
                            <input type="text" className="w-full py-2  outline-none font-trebuchet font-light text-[14px] pl-4 text-components-bold rounded-[4px] border-[1.8px] border-components-bold" placeholder="Numero de Telephone" />
                            <input type="text" className="w-full py-2  outline-none font-trebuchet font-light text-[14px] pl-4 text-components-bold rounded-[4px] border-[1.8px] border-components-bold" placeholder="Date" />
                            <input type="text" className="w-full py-2  outline-none font-trebuchet font-light text-[14px] pl-4 text-components-bold rounded-[4px] border-[1.8px] border-components-bold" placeholder="Nombre de places" />
                            <div className="flex items-center justify-between w-full gap-4">
                                <button className="py-2 hover:text-components-bold hover:bg-white duration-200 bg-components-bold w-full  outline-none cursor-pointer font-trebuchet font-bold text-[14px] text-white rounded-[4px] border-[1.8px] border-[#4E3265]">
                                    Reserver Maintenant
                                    </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

              <div className="container mx-auto">
                  <div className="w-full items-start justify-between gap-12">
                      <div className="w-full flex flex-col gap-4 mb-6">
                        <div>
                            <h1 className="font-trebuchet font-normal text-[42px] tablet:text-[34px] phone:text-[27px] text-[#410053]">
                                Description
                            </h1>
                            <div className="w-full h-[1.3px] bg-[#410053]"></div>
                            </div>
                            <div className="w-full">
                                <p className="font-trebuchet font-normal text-[18px] phone:text-[15px] leading-normal">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Integer feugiat scelerisque varius morbi enim nunc. Erat imperdiet sed euismod nisi. Arcu dictum varius duis at consectetur lorem donec. Elementum pulvinar etiam non quam lacus suspendisse faucibus interdum. Etiam tempor orci eu lobortis elementum nibh tellus molestie. Posuere sollicitudin aliquam ultrices sagittis orci a. Suspendisse ultrices gravida dictum fusce ut Placerat. Eget nullam non nisi est sit amet facilisis magna. Pellentesque diam volutpat commodo sed. Lectus proin nibh nisl condimentum id venenatis a. Suscipit tellus mauris a diam maecenas. Vulputate eu scelerisque felis imperdiet proin. Eu facilisis sed odio morbi. Tortor pretium viverra suspendisse potenti nullam ac. Nisl vel pretium lectus quam id leo. Blandit cursus risus at ultrices mi tempus imperdiet. Maecenas volutpat blandit aliquam etiam erat velit scelerisque. Tristique senectus et netus et. Tortor condimentum lacinia quis vel eros donec. Tincidunt praesent semper feugiat nibh sed pulvinar proin gravida
                                </p>
                            </div>
                        <div className="w-full h-fit pt-4  flex items-center justify-start flex-wrap gap-4 gap-y-6">
                            <div className="w-fit h-fit flex items-start justify-start gap-3">
                                <GiDuration className="text-[37px] laptop:text-[30px] tablet:text-[26px] phone:text-[22px]  text-[#410053]" />
                                <h1 className="text-[22px] laptop:text-[17px] tablet:text-[15px] phone:text-[10px] text-[#410053]">
                                    Durée de l&apos;atelier
                                </h1>
                            </div>
                            <div className="w-fit h-fit flex items-start justify-start gap-3">
                                <GiDuration className="text-[37px] laptop:text-[30px] tablet:text-[26px] phone:text-[22px]  text-[#410053]" />
                                <h1 className="text-[22px] laptop:text-[17px] tablet:text-[15px] phone:text-[10px] text-[#410053]">
                                    Durée de l&apos;atelier
                                </h1>
                            </div>
                            <div className="w-fit h-fit flex items-start justify-start gap-3">
                                <GiDuration className="text-[37px] laptop:text-[30px] tablet:text-[26px] phone:text-[22px]  text-[#410053]" />
                                <h1 className="text-[22px] laptop:text-[17px] tablet:text-[15px] phone:text-[10px] text-[#410053]">
                                    Durée de l&apos;atelier
                                </h1>
                            </div>
                        </div>
                      </div>
                      <div className="w-full mb-6">
                          <h1 className="font-trebuchet font-normal text-[42px] tablet:text-[34px] phone:text-[27px] text-[#410053]">
                              Details d&apos;atelier
                          </h1>
                          <div className="w-full h-[1.3px] bg-[#410053] mb-6"></div>
                          <div className="w-full h-fit flex items-center justify-start gap-8 flex-wrap">
                              <div className="w-fit h-fit flex items-center justify-center gap-2">
                                  <AiOutlineCheckCircle className="text-[25px] text-[#410053]" />
                                  <h1 className="text-[20px] text-[#410053]">Parking</h1>
                              </div>
                              <div className="w-fit h-fit flex items-center justify-center gap-2">
                                  <AiOutlineCheckCircle className="text-[25px] text-[#410053]" />
                                  <h1 className="text-[20px] text-[#410053]">En extérieur</h1>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="flex flex-col items-start gap-3">
                      <h1 className="font-trebuchet font-normal text-[42px] tablet:text-[34px] phone:text-[27px] text-[#410053] mt-4">
                          Commentaires
                      </h1>
                      <div className="w-full h-[1.3px] bg-[#410053]"></div>
                      <div className="w-fit h-fit flex items-start justify-start gap-2">
                          <AiFillStar className=" text-[40px] text-[#410053]" />
                          <h1 className=" font-trebuchet font-medium text-[30px]">
                              4.1 - 100 commentaires
                          </h1>
                      </div>
                      <div className="grid lg:grid-cols-2 items-start justify-start gap-8">
                          {arrTest.map((el, index) => (
                          <div key={index} className="w-full h-fit">
                              <div className="flex items-center justify-start gap-2">
                                  <Image src={ "/iconCom.png"} alt={ ""} width={50} height={50} className="w-[50px] h-[50px] rounded-full" />
                                  <div className="flex flex-col gap-2">
                                      <h1 className="font-trebuchet font-regular text-[20px] text-[#000]">
                                          Ted Yorick
                                      </h1>
                                      <h1 className="font-trebuchet font-regular text-[15px] text-[#00000095]">
                                          Octobre 2023
                                      </h1>
                                  </div>
                              </div>
                              <p className=" font-trebuchet text-[18px] font-normal text-[#000]">
                                  Lorem ipsum dolor sit amet consectetur. Cursus sit enim orci lorem lorem nisi dui tempus. Mus id ut velit sed blandit interdum ornare venenatis interdum. Eu vel sed lacus nam eget cras dolor urna tortor. Id ac ipsum eget sit in egestas mattis nulla libero. Mus id ut velit sed
                              </p>
                          </div>
                          ))}
                      </div>
                  </div>
              </div>

          </div>
      </div>
      <Footer />
    </div>
  );
}
