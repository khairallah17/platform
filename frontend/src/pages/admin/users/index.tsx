import React, { useEffect, useState } from 'react'
import DashboardLayout from '@/Components/Dashboard/admin/DashboardLayout'
import UsersLayout from '@/Components/Dashboard/admin/UsersLayout'
import { User, UserList } from '@/interaces'
import { useAxios } from '@/Hooks/useAxios'
import axios, { AxiosError, AxiosResponse } from 'axios'
import GetContext from '@/context/UserContext'
import { useLayoutEffect } from 'react'
import useSWR from "swr"

const Index = () => {

  const [users, setUsers] = useState<any>()
  const [response, setResponse] = useState<User[]>([])
  const [loading, setLoading] = useState<boolean>()

  useEffect(() => {

    const fetch = async () => {

      try {

        const response = await axios.get("http://localhost:5000/user",{
          withCredentials: true,
          headers: {
            Authorization: localStorage.getItem("accessToken")
          }
        })

        console.log(response.data)

        setResponse(response?.data)

      } catch (error: any) {
        console.log(error.message)
      }

    }

    fetch()

  }, [])


  const convertDate = (date: string): string => (new Intl.DateTimeFormat('fr-FR').format(new Date(date)))


  return (
    <DashboardLayout>
        <UsersLayout users={response} />
    </DashboardLayout>
  )
}

export default Index