import React from 'react'
import DashboardLayout from '@/Components/Dashboard/admin/DashboardLayout'
import HomeLayout from '@/Components/Dashboard/admin/HomeLayout'
import { useEffect } from 'react'
import GetContext from '@/context/UserContext'
import { useRouter } from 'next/router'
import AuthWrapper from '@/Components/authentication/authWrapper'

const Index = () => {
  
  return (
    <DashboardLayout>
        <HomeLayout/>
    </DashboardLayout>
  )
}

export default Index