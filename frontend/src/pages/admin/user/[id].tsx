import React from 'react'
import DashboardLayout from '@/Components/Dashboard/admin/DashboardLayout'
import { useSearchParams } from 'next/navigation'

const Index = () => {

    const searchParams = useSearchParams()
    let id = null

    if (searchParams)
        id = searchParams.get("id")


    return (
        <DashboardLayout>
            <div className='w-full'>{id}</div>
        </DashboardLayout>
    )
}

export default Index