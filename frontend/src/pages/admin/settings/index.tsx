import React from 'react'
import DashboardLayout from '@/Components/Dashboard/admin/DashboardLayout'

const index = () => {
  return (
    <DashboardLayout>
    
        <div className="w-full h-full p-10">

            <div className="w-full h-fit">
                <h1 className="font-gotham font-bold text-[40px] uppercase text-[#9C9C9C]">
                    paramètre
                </h1>
            </div>

        </div>
 
    </DashboardLayout>
  )
}

export default index