import React from 'react'
import DashboardLayout from '@/Components/Dashboard/admin/DashboardLayout'

const index = () => {
  return (
    <DashboardLayout>
        <div className="w-full h-full p-10">
            <h1 className="font-gotham font-bold text-[40px] uppercase text-[#9C9C9C]">
                List des Reservations
            </h1>
        </div>
    </DashboardLayout>
  )
}

export default index