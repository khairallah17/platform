import Image from "next/image";
import { BsFacebook, BsGoogle } from "react-icons/bs";
import GetContext from "@/context/UserContext";
import Link from "next/link";
import { FormEvent, useEffect } from "react";
import { useAxios } from "@/Hooks/useAxios";
import { MdErrorOutline } from "react-icons/md";
import { useState } from "react";
import { PulseLoader } from "react-spinners"

export default function Sign_in() {

  const [ emptyFieldsError, setEmptyFieldsError ] = useState(false)
  const [ loading ,setLoading ] = useState<boolean>(false)
  
  let { inputMail, inputPassword, setInputMail, setInputPassword, login, googleLogin, authenticationChecker, loginError, errorMsg, setErrorMsg, setLoginError } =
    GetContext();

    const loginSubmit = (e: FormEvent<HTMLFormElement>) => {

      e.preventDefault()
      setLoginError(false)
      login()

    }

    useEffect(() => {

      authenticationChecker()
  
    }, [])

  return (
    <div className="w-full h-[100vh] flex items-center justify-center">
      <div className="w-1/2 h-full flex items-center justify-center tablet:w-full phone:w-full Signbg laptop:w-2/3">
        <form onSubmit={loginSubmit} className="h-fit w-[600px] phone:w-[400px]">
          <Image
            src="/LogoSign.png"
            alt="Logo"
            width={70}
            height={70}
            className="mb-4 w-[60px] h-[60px]"
          />
          <h1 className=" font-gotham-bold text-[26px] text-white mb-4">
            Me connecter
          </h1>
          <div className="w-full h-fit bg-white px-8 rounded-[20px] flex flex-col items-center justify-center py-8">

            {
              loginError &&
              <div className="error-message bg-red-300 flex p-4 items-center gap-4 self-start w-full rounded-xl">
                <span>
                  <MdErrorOutline className=" text-red-500" size={32}/>
                </span>
                <span>
                  {errorMsg}
                </span>
              </div>
            }

            <div className="w-full h-fit mt-5 phone:mt-3">
              <h1 className="font-gotham font-normal text-[16px]">Email</h1>
              <input
                type="email"
                value={inputMail}
                onChange={(e) => setInputMail(e.target.value)}
                className="validation_input pl-3 w-full h-[45px] phone:h-[40px] mt-3 phone:mt-3 bg-[#F5F5F5] border-2 rounded-[10px] px-2 outline-none"
                placeholder="Email Adress"
              />
              <h1 className="font-gotham font-normal text-[16px] mt-4 phone:mt-3">
                Mot De Passe
              </h1>
              <input
                type="password"
                value={inputPassword}
                onChange={(e) => setInputPassword(e.target.value)}
                className="validation_input pl-4 w-full h-[45px] phone:h-[40px] phone:mt-2 mt-3 bg-[#F5F5F5] border-2 rounded-[10px] px-2 outline-none"
                placeholder="Saisissez votre mot de passe"
              />
              <div className="w-full h-fit flex items-center justify-end mt-2">
                <Link
                  href="/"
                  className="font-gotham font-semibold text-[#4E3265] text-[12px] tracking-normal"
                >
                  J&apos;ai oublié mon mot de passe ?
                </Link>
              </div>
              <div className="w-full h-fit flex items-center justify-center mt-6 phone:mt-4">
                <button
                  type="submit"
                  className="bg-components-bold w-[220px] h-[48px] text-white font-trebuchet font-bold text-[20px] phone:text-[17px] rounded-[5px] cursor-pointer"
                >
                  {
                    loading ?  <PulseLoader color="#fff" /> : "Me connecter"
                  }
                </button>
              </div>
              <div className="w-full h-fit flex items-center justify-center mt-6 phone:mt-5">
                <div className="w-[80px] h-[3px] opacity-25 bg-black"></div>
              </div>
              <div className="w-full h-fit flex items-center justify-center mt-3 gap-1">
                <h1 className="font-gotham font-medium text-[#000] text-[15.2px] tablet:text-[14px] phone:text-[10px] tracking-normal">
                  Vous Disposer pas d&apos;un compte ?
                </h1>
                <Link
                  href="/Signup"
                  className="font-gotham font-medium text-[#4E3265] text-[15.2px] tablet:text-[14px] phone:text-[10px] tracking-normal"
                >
                  Créer un compte
                </Link>
              </div>
              <div className="w-full h-fit flex items-center justify-center mt-3">
                <div className=" cursor-pointer w-[80%] h-[60px] tablet:w-[63%] tablet:h-[55px] phone:h-[50px] phone:w-[85%] bg-white rounded-[5px] border-[2px] border-[#4E3265] flex items-center justify-center relative  gap-12">
                  <BsFacebook
                    className=" absolute left-2 text-[#4E3265] ml-2 text-[32px] mr-12 phone:text-[26px]"
                    size={32}
                  />
                  <h1 className="text-[26px]  font-bold text-[#4E3265] phone:text-[22px]">
                    Facebook
                  </h1>
                </div>
              </div>
              <button className="w-full h-fit flex items-center justify-center mt-3" onClick={googleLogin}>
                <div className=" cursor-pointer w-[80%] h-[60px] tablet:w-[63%] tablet:h-[55px] phone:h-[50px] phone:w-[85%] bg-white rounded-[5px] border-[2px] border-[#4E3265] flex items-center justify-center relative  gap-12 mb-3 phone:mb-1">
                  <BsGoogle
                    className=" absolute left-2 text-[#4E3265] text-[32px]  phone:text-[26px]"
                    size={32}
                  />
                  <h1 className="text-[26px] font-bold text-[#4E3265] phone:text-[22px]">
                    Gmail
                  </h1>
                </div>
              </button>
            </div>
          </div>
        </form>
      </div>
      <div className="w-1/2 h-full SignImg tablet:hidden phone:hidden laptop:w-1/3">
        <div className="w-full h-full bgOpa"></div>
      </div>
    </div>
  );
}
