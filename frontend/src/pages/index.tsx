import { Header } from "@/Components/LandingPage/Header";
import { useState } from "react";
import CategoriesSection from "@/Components/LandingPage/CategoriesSection";
import NSection from "@/Components/LandingPage/NouveauSection";
import PSection from "@/Components/LandingPage/PopulaireSection";
import { LoginSection } from "@/Components/LandingPage/LoginSection";
import { text } from "stream/consumers";
import { WhoWeAre } from "@/Components/LandingPage/WhoWeAre";
import { BlogArea } from "@/Components/LandingPage/BlogArea";
import { Partner } from "@/Components/LandingPage/Partner";
import { Footer } from "@/Components/LandingPage/Footer";
import MainSection from "@/Components/LandingPage/MainSection";

export default function Home() {

  const [datainput, setDatainput] = useState("");

  return (
    <div className="w-full h-[100vh]  bg-[#FFFBFC]">
      <Header />
      <MainSection value={datainput} setValue={setDatainput} />
      <CategoriesSection />
      <NSection />
      <PSection />
      <LoginSection />
      <WhoWeAre />
      <BlogArea />
      <Partner />
      <Footer />
    </div>
  );
}
