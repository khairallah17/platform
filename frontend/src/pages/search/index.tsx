import { Header } from "@/Components/LandingPage/Header";
import { Footer } from "@/Components/LandingPage/Footer";
import { Filter } from "@/Components/LandingPage/Filter";
import Card from "@/Components/LandingPage/Card";

export default function Index() {

  return (
    <div className="w-full h-[100vh]  bg-[#FFFBFC]">
      <Header />
      <Filter />
      <div className="w-full h-fit px-20 laptop:px-11  tablet:px-8 phone:px-8  pt-10">
        <h1 className="font-gotham font-normal text-[19px] text-[#898989]">
          52 resultats
        </h1>
        <div className="w-full h-fit mt-5 flex flex-wrap justify-center mb-10 gap-6">
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
          <Card />
        </div>
      </div>
      <Footer />
    </div>
  );
}
