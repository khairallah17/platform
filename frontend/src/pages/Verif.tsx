import Image from "next/image";
import { Verif } from "@/Components/Signup/Verif";
import { VerifSucc } from "@/Components/Signup/VerifSuccess";
import { useState } from "react";
export default function Verification() {
  const [val, setVal] = useState<boolean>(false);
  return (
    <div className="w-full h-[100vh] relative overflow-hidden flex items-center justify-center">
      {!val ? <Verif val={val} setVal={setVal} /> : <VerifSucc />}
      <Image
        width={100}
        height={100}
        src="/Signup/v2.svg"
        alt=""
        className="w-[46%] absolute left-[-3%] bottom-[-1.5%] z-[1]"
      ></Image>
      <Image
        width={100}
        height={100}
        src="/Signup/v1.svg"
        alt=""
        className="w-[46%] absolute left-[-2.6%] bottom-[-1%] z-[2]"
      ></Image>
      <Image
        width={100}
        height={100}
        src="/Signup/v3.svg"
        alt=""
        className="w-[48%] h-fit absolute right-0 top-0 z-[1]"
      ></Image>
      <Image
        width={100}
        height={100}
        src="/Signup/v4.svg"
        alt=""
        className="w-[49%] h-fit absolute right-0 top-0 z-[2]"
      ></Image>
    </div>
  );
}
