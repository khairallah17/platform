export interface User {
    id:                     string,
    email:                  string,
    name:                   string,
    phoneNumber:            string,
    birthDate:              string,
    createdAt:              string,
    profileImage?:          string | null
    verified:               boolean
    type:                   string
    status:                 boolean
    professionCategory?:    Category
    professionType?:        string
    certification?:         boolean
    cartificationImage?:    string | null
    workshop?:              boolean
    workshopImage?:         string | null
    userPreferences:        UserPreferences
}

export interface UserPreferences {
    id:                     string
    type:                   string
    status:                 boolean
    user?:                  User
    professionCategory?:    Category
    professiontype?:        string
    certification?:         boolean
    certificationImage?:    string
    workshop?:              boolean
    workShopImage?:         string
}

export interface Announcement {
    id:             string
    annoucedBy:     User
    category:       Category
    comments:       Comment[]
    duration:       string
    city:           string
    address:        string
    details:        JSON
    description:    string
    reservations:   Reservation
    seats:          number
    filled:         boolean
    approved:       boolean
    title:          string
    createdAt:      string
    images:         JSON
}

export interface WishList {
    user:           User
    announcement:    Announcement
}

export interface Reservation {
    id:             string
    reservedBy?:    User
    fullName:       string
    phoneNumber:    string
    announcement:   Announcement
    dateAndTime:    string
    seats:          number
}

export interface Category {
    name:   string
}

export interface Comment {
    user:           User
    announcement:    Announcement
    content:        string
    rating:         number
    createdAt:      string
}

export interface Article {
    id:             string
    title:          string
    content:        string
    image:          string
    createdAt:      string
}

export interface UserList {
    users: User[]
}

export interface AnnoucementList {
    annoucements:  Announcement[]
}

export interface ArticleList {
    articles:      Article[]
}

export interface AnnouncementDateTime {
    startDate:     Date,
    endDate:       Date,
    eventDate:     Date
}
