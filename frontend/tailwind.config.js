/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/Components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        'trebuchet': ['Trebuchet MS', 'sans'],
      },
      fontFace: {
        'trebuchet': {
          'fontFamily': 'Trebuchet MS',
          'fontStyle': 'regular',
          'fontDisplay': 'swap',
          'src': 'url("/fonts/TrebuchetMS.woff") format("woff")',
        },
        'gotham' : {
          'fontFamily': 'Gotham',
          'fontStyle': 'regular',
          'fontDisplay': 'swap',
          'src': 'url("/fonts/Gotham-Black.woff") format("woff")',
        },
      }, 
      screens: {
        'laptop': {'raw': ' (min-width: 1024px) and (max-width: 1280px)'},
        'tablet': {'raw': '(min-width: 768px) and (max-width: 1023px)'},
        'phone': {'raw': '(max-width: 767px)'},
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      colors: {
        dashboard: {
          default: "#9747FF"
        },
        components: {
          DEFAULT: "#EFDCF9",
          bold: "#4E3265"
        }
      }
    },
  },
  plugins: [],
}
